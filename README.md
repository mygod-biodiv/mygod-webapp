# MyGOD

MyGOD is a web-based tool to visualize and explore data collected by genome observatories. It provides tools to import the data as well as to build interactive *dashboards* aggregating various graphical representations. The parameters of these representations can be adjusted to focus on a specific subset of the initial representation. For registered users, MyGOD allows to save dashboards for later reuse. Saved dashboards can be shared with all the registered users of a MyGOD instance, as well as promoted as gallery entries available in the Gallery section of MyGOD.

# Installing & Running MyGOD using Docker

The easiest way to run MyGOD is to use the provided Docker image and `docker-compose.yml` file, as documented in this [README.md](docker/README.md)


# Installing & Running MyGOD from Source.

MyGOD is also available in source form. It has been run successfully on Ubuntu 20.x as well as Windows 10.

## Requirements

The following packages will be needed to install MyGOD from source :

1. NPM (The source tree contains a `package.json` with all the libraries).
1. PostgreSQL 10 or above (with the PostGis 2.x extension)
1. Python 3.8 or above (The source tree contains a `requirements.txt` file with all the Python dependencies.)
1. R 4.x or above

## Installation procedure

TODO



# Data Import

MyGOD currently supports two types of datasets: metabarcodes and environmental (physico-chemical or PC) measurements. Each type has its dedicated import command. 
Metabarcode data has to be imported before PC data. All data are stored in tabular files and the MyGOD import tools allow these to be very flexible as their column layout is defined in a table configuration file that is specified as an argument of the import command.

Full documentation about file formats can be found [here](doc/file_formats.md)

All data import is done using the Django `manage.py`command. This implies that the `DJANGO_SETTINGS_MODULE` has been properly initialized, and that the overall Django environment is functional. 


## Importing metabarcode data

MyGOD relies on four files to import metabarode data:

1. An event table describing when and where the samples were collected.
2. A sample table describing the actual ASVs, taxonomic assignments, and number of reads for each sample.
3. A JSON file with metadata about the events and samples.
4. A JSON file describing the column layout in the sample table as well as how to match event identifiers between the event and the sample table.

The basic command to import matabarcode data is as follows:

```
python manage.py loadmetabar --event-table "/PATH/TO/event_table.csv" --sample-table "/PATH/TO/sample_table.csv" --metadata "/PATH/TO/metadata.json" --config "/PATH/TO/table_config.json" 
```


## Importing physico-chemical data

Environmental (physico-chemical or PC) data import in MyGOD relies on two files:

1. A table with the sampling event information supplemented with the actual PC data.
2. A JSON file describing the layout of the columns in the previous table.

The basic command to import PC data is as follows:

```
python manage.py loadpc --csv "/PATH/TO/event_pc_table.csv" --config "/PATH/TO/table_config.json" 

```


## Data Owner User


MyGoD application got two types of users 

-- Owner Users :
Each time a new dataset is loaded in database, a new group of users is created, 
this new group has permissions to view and change metadata associated to dataset. 
In the same time a set of Users, described in owner field of json file Metadata is created. 
Thoses users are associated to previous explained Group. 

-- Simple Users : 
They can only visualize data from dataset on the dashboard and can't modify any data or metadata related to dataset



