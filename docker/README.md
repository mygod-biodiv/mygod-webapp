# Runnning MyGOD with Docker

## Requirements

This README covers the process of running an instance of MyGOD inside a Docker container.

In order to run MyGOD in a Docker container, you need a working installation of Docker, as well as the `docker compose` command (V2 or higher).

## Pulling the MyGOD Docker Image

The latest release of the MyGOD Docker image can be pulled using the following command:

```
docker pull quay.io/mhoebeke/mygod:latest
```

This will be done automatically when using `docker compose` to run one of the services defined in the `docker-compose.yml` file.

## Setting Up the MyGOD Instance.

The MyGOD docker image relies on set of environment variables that are initialized in a file. By default, the `docker-compose.yml`file uses the `mygod.env` file of the current directory.

A `mygod_template.env` is available and can be copied and adapted to configure the actual `mygod.env`file. Comments inside the template explain the meaning of the different variables.

## Initializing the Application

This step needs only to be performed once. It takes care of creating the database structure and adding an application administrator.

The initialization is performed using the following command:
```
docker compose run initdb
```

## Inserting Datasets

In order to be processed by the data loaders, dataset files need to be organized in a specific layout, inside subdirectories of the main data directory.

The actual location of the data directory is defined in the `docker-compose.yml` file, in the `volumes` section of the `loaddata` service :

```
  loaddata:
  (...)
    volumes:
        - ./data:/data
  (...)
```
In this default configuration, the main data directory is the `data` directory located in the same directory as the `docker-compose.yml` file.

### Metabarcode Datasets

Metabarcode datasets are stored in the `metabar` subdirectory of the main data directory. Each metabarcode dataset has its own subdirectory inside the ̀`metabar`directory. In this dataset subdirectory, four files must be present:

  1. `metadata.json` : the JSON file with the dataset metadata
  2. `samples.csv`: a CSV file with occurrence data (reads/ASVs)
  3. `events.csv`: a CSV file with information about the sampling events.
  4. `table_config.json`: the JSON file describing how the CSV files are organized.

For a dataset called `ASTAN_16S`, the directory layout would be as follows :

```
data
├── metabar
│   └── ASTAN_16S
│       ├── events.csv
│       ├── metadata.json
│       ├── samples.csv
│       └── table_config.json

```

### Physico-chemical Datasets

Physico-chemical datasets are stored in the `pc` subdirectory of the main data directory. Each physico-chemical dataset has its own subdirectory inside the ̀`pc`directory. In this dataset subdirectory, two files must be present:

  1. `data.csv`: a CSV file with physico-chemical measurements
  2. `table_config.json`: the JSON file describing how the CSV files are organized.

For a dataset called `ASTAN_SOMLIT`, the directory layout would be as follows :

```
data
└── pc
    └── ASTAN_SOMLIT
        ├── data.csv
        └── table_config.json
```

### Performing Data Insertion

Running the data loading routines is done with the following command:

```
docker compose run loaddata
```

The command displays information about the different steps of each dataset that is imported.


## Running the Web Application


The MyGOD Web Application is run with the following command:

```
docker compose up -d
```

Applications logs are then available by running:
```
docker compose logs -f
```

By default, the Web Application will be bound to port 9090 on the local host. The MyGOD instance will then be available at address:

`http://localhost:9009/mygod`

(If the default `APPLICATION_CONTEXT` has not been changed in the environment file.)


The actual port number can be tuned in the `docker-compose.yml` file:

```
  webapp:
    env_file:
    (...)
    ports:
        - "9090:8080"
    (...)
```

