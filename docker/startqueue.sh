#!/bin/bash

source /app/venv/bin/activate

cd /app
# Create Django migrations to ensure database
# is in sync with application models. 
echo "***Starting the Queue Cluster."
python3 manage.py qcluster


