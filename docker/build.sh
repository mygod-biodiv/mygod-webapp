# This script creates an 'app' directory with the contents of the MyGOD application
# needed to build the Docker image.
# It works from inside a MyGOD Git repo cloned directory.
# 1. Build the Vue.js packaged components.
package_js() {
	pushd ../mygod/mygodvue
	npm install
	npx --yes browserslist@latest --update-db
	npm run build
	popd
}

# 2. Copy the needed files & directories from the Git workspace to the app directory mounted in the container.
fill_app() {
	rm -rf app
	mkdir -p app
	cp -r  ../mygod/{manage.py,mygod,mygodapp} app/
	mkdir app/mygodvue
	cp -r ../mygod/mygodvue/{static,vue} app/mygodvue
	mkdir app/rscripts
	cp -r ../mygod/rscripts/*.r app/rscripts
}

package_js
fill_app

