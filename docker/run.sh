#!/bin/bash

source /app/venv/bin/activate

echo "Running MyGOD Application as $(id)"

cd /app
# Create Django migrations to ensure database
# is in sync with application models. 
echo "***Making the migrations."
python3 manage.py makemigrations mygodapp

# If needed update database schema.
echo "***Applying the migrations to the database."
python3 manage.py migrate


# Create the superuser.
# This can be run multiple times. The user will
# only be created if no existing user with the same
# email address already exists.
if [ "x${DJANGO_SUPERUSER_USERNAME}" == "x" ]
then
        export DJANGO_SUPERUSER_USERNAME=mygodadmin
fi
if [ "x${DJANGO_SUPERUSER_PASSWORD}" == "x" ]
then
        export DJANGO_SUPERUSER_PASSWORD=mygodpassword
fi
if [ "x${DJANGO_SUPERUSER_EMAIL}" == "x" ]
then
        export DJANGO_SUPERUSER_EMAIL=mygodadmin@domain.com
fi
echo "***Creating the superuser: ${DJANGO_SUPERUSER_USERNAME} / ****password**** / ${DJANGO_SUPERUSER_EMAIL}"
python3 manage.py createsuperuser --noinput
echo "***Activating the superuser."
python3 manage.py shell <<EOSHELL
from django.contrib.auth.models import Group,User
publishers,created = Group.objects.get_or_create(name='publisher')
importers,created = Group.objects.get_or_create(name='importers')
users = User.objects.all()
for user in users:
        if user.is_superuser:
                user.is_active = True
                user.groups.add(publishers)
                user.groups.add(importers)
                user.save()
EOSHELL


# Collect all static content in the dedicated directory.
echo "***Collecting static content..."
rm -rf /static/*
python3 manage.py collectstatic --noinput --clear

# Create the root directory where imported files will be stores
mkdir -p "${IMPORT_ROOT}"

# Create an empty file signaling all pre-uWSGI operations are finished
touch /tmp/done

# Finally start the web application.
echo "***Starting uWSGI..."
uwsgi --master \
      --chdir=/app \
      --module=mygod.wsgi:application \
      --env DJANGO_SETTINGS_MODULE=mygod.settings \
      --env R_HOME=/usr/lib/R \
      --static-map ${APPLICATION_CONTEXT}/static=/static \
      --static-map ${APPLICATION_CONTEXT}/media=/app/media \
      --processes=16 \
      --http :8080 \
      --buffer-size=65535 \
      --harakiri=600 \
      --http-timeout=600
