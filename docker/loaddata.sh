#!/bin/bash

source /app/venv/bin/activate

if [ -d "/data/metabar" ]
then
   echo "Scanning for metabarcode datasets."
   for dataset in $(find /data/metabar/ -mindepth 1 -maxdepth 1 -type d)
   do
      echo "Processing metabarcode dataset in ${dataset}"
      if [ -r "${dataset}/import.log" ]
      then
      	echo "Dataset in ${dataset} already imported. Skipping."
      	continue
      fi
      ok=1
      if [ ! -r "${dataset}/metadata.json" ]
      then
         echo "Missing metadata file: ${dataset}/metadata.json"
         ok=0
      fi
      if [ ! -r "${dataset}/samples.csv" ]
      then
         echo "${dataset}/samples.csv"
         ok=0
      fi
      if [ ! -r "${dataset}/events.csv" ]
      then
         echo "Missing events file: ${dataset}/events.csv"
         ok=0
      fi
      if [ ! -r "${dataset}/table_config.json" ]
      then
         echo "Missing table configuration file: ${dataset}/table_config.json"
         ok=0
      fi
      if [ "x${ok}" == "x1" ]
      then
         pushd /app
         python3 manage.py loadmetabar --metadata ${dataset}/metadata.json --sample_table ${dataset}/samples.csv --event_table ${dataset}/events.csv --config ${dataset}/table_config.json |& tee ${dataset}/import.log
         popd
      fi
      echo "Done processing metabarcode dataset in ${dataset}"
   done
else
   echo "No metabarcode datasets found."
fi

if [ -d "/data/pc" ]
then
   echo "Scanning for physico-chemical datasets."
   for dataset in $(find /data/pc/ -mindepth 1 -maxdepth 1 -type d)
   do
      echo "Processing physico-chemical dataset in ${dataset}"
      if [ -r "${dataset}/import.log" ]
      then
      	echo "Dataset in ${dataset} already imported. Skipping."
      	continue
      fi
      ok=1
      if [ ! -r "${dataset}/data.csv" ]
      then
         echo "Missing data file: ${dataset}/data.csv"
         ok=0
      fi
      if [ ! -r "${dataset}/table_config.json" ]
      then
         echo "Missing table configuration file: ${dataset}/table_config.json"
         ok=0
      fi
      if [ "x${ok}" == "x1" ]
      then
         pushd /app
         python3 manage.py loadpc --csv ${dataset}/data.csv --config ${dataset}/table_config.json |& tee ${dataset}/import.log 
         popd
      fi
      echo "Done processing physico-chemical dataset in ${dataset}"
   done
else
   echo "No physico-chemical datasets found."
fi

