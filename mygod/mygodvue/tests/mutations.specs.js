import Vuex from "vuex";
import * as mutations from '../src/store/mutations';
import {expect} from "chai";
import {createLocalVue} from "@vue/test-utils";


/* eslint-disable */
const localVue = createLocalVue();
localVue.use(Vuex);

describe('Testing TaxonSearchPanel related Mutations', () => {
    let state = {
        dashboardConfig: [], selectedPanel: null,
        panelIndex: 1, panels: {
            "59711c43-c909-e948": {
                backgroundColor: "#047D72",
                dataset: "Astan 18S - 2009-2016",
                dateEnd: "2013-12-22",
                dateStart: "2009-01-07",
                haswidgets: true,
                index: 0,
                name: "Astan 18S - 2009-2016  / 11|Bathycoccaceae",
                panelId: "59711c43-c909-e948",
                rank: "Any",
                restore: false,
                taxon: "11|Bathycoccaceae",
                type: " Temporal"
            }
        }
    }

    it('Add panel in Search Panel (addTaxonSelectorPanel)"', () => {
        let payload = {
            panelId: "36711c72-x225-e642",
            backgroundColor: "#F9F871",
            name: 'Unnamed Panel',
            restore: false
        }
        mutations.addTaxonSelectorPanel(state, payload);
        expect(state.panelIndex).to.equal(2)
    });

    it('Change store.panels info when search panel is updated (taxonSearchPanelUpdated)"', () => {

        let payload = {
            backgroundColor: "#047D72",
            dataset: "Astan 18S - 2009-2016",
            dateEnd: "2016-12-22",
            dateStart: "20011-01-07",
            index: 0,
            panelId: "59711c43-c909-e948",
            rank: "Genus",
            taxon: "36|Ostreoccocus",
            type: "Temporal"
        }

        mutations.taxonSearchPanelUpdated(state, payload);

        expect(state.panels["59711c43-c909-e948"].rank).to.equal("Genus")
        expect(state.panels["59711c43-c909-e948"].taxon).to.equal("36|Ostreoccocus")
    });

    it('Change Search Panel selected  (searchPanelSelectionChanged)"', () => {
        let payload = {panelId: "36711c72-x225-e642"}
        mutations.searchPanelSelectionChanged(state, payload);
        expect(state.selectedPanel.panelId).to.equal("36711c72-x225-e642")
    });

    it('Change store.panels info when search panel is removed (removeTaxonSelectorPanelWithDependencies)"', () => {
        let payload = "36711c72-x225-e642"
        mutations.removeTaxonSelectorPanelWithDependencies(state, payload);
        expect(state.panelIndex).to.equal(1)
        expect(state.panels).to.not.have.keys("36711c72-x225-e642")
    });


});


describe('Testing Dashboard related Mutations', () => {
    let state = {
        dashboardConfig: [{
            x: 0,
            y: 0,
            w: 12,
            h: 10,
            i: "griditem-scatterplot-6d7363c8-ce14-63c7",
            type: "scatterplot",
            c: "scatterplot",
            backgroundColor: "#047D72",
            panelId: "59711c43-c909-e948",
            triggerValue: 0,
            settings: {"restore": true}
        },], selectedPanel: {panelId: "59711c43-c909-e948"},
        panelIndex: 1, panels: {
            "59711c43-c909-e948": {
                backgroundColor: "#047D72",
                dataset: "Astan 18S - 2009-2016",
                dateEnd: "2013-12-22",
                dateStart: "2009-01-07",
                haswidgets: true,
                index: 0,
                name: "Astan 18S - 2009-2016  / 11|Bathycoccaceae",
                panelId: "59711c43-c909-e948",
                rank: "Any",
                restore: false,
                taxon: "11|Bathycoccaceae",
                type: " Temporal"
            }
        }
    }

    it('Update widget in dashboard config (updateDashboardWidgetSettings)"', () => {
        let payload = {
            componentId: "griditem-scatterplot-6d7363c8-ce14-63c7",
            settings: {
                dataset: "Astan 18S - 2009-2016",
                taxon: "11|Bathycoccaceae",
                asv: "asv",
                representation: "relative_abundance",
                fraction: "02",
                parameter: "temperature",
                dateStart: "2019-04-26",
                dateEnd: "2014-12-22",
                depth: "SRF"
            }
        }
        mutations.updateDashboardWidgetSettings(state, payload);
        expect(state.dashboardConfig[0].settings.restore).to.equal(false)
    });

    it('Clone widget in dashboard config (cloneWidget)"', () => {
        let payload = {componentId: "griditem-scatterplot-6d7363c8-ce14-63c7"}
        mutations.cloneWidget(state, payload);
        expect(state.dashboardConfig).to.have.lengthOf(2)
    });

    it('Add widget in dashboard config (addDashboardWidget)"', () => {
        let payload = {type: 'summary'}
        mutations.addDashboardWidget(state, payload);
        expect(state.dashboardConfig).to.have.lengthOf(3)
    });

    it('Remove widget in dashboard config (removeDashboardWidget)"', () => {
        let payload = "griditem-scatterplot-6d7363c8-ce14-63c7"
        mutations.removeDashboardWidget(state, payload);
        expect(state.dashboardConfig).to.have.lengthOf(2)
    });

    it('Clear dashboard (clearDashboard)"', () => {
        mutations.clearDashboard(state);
        expect(state.dashboardConfig).to.have.lengthOf(0)
    });

});

describe('Testing Save/Load related Mutations', () => {
    let state = {
        dashboardConfig: [], selectedPanel: null,
        panelIndex: 0, panels: {}
    }

    it('Load settings in Dashboard (loadSettings)"', () => {
        let payload = {
            panelId: "36711c72-x225-e642",
            backgroundColor: "#F9F871",
            name: 'Unnamed Panel',
            restore: false
        }
        mutations.addTaxonSelectorPanel(state, payload);
        expect(state.panelIndex).to.equal(1)
        expect(state.panels).to.have.keys("36711c72-x225-e642")
    });

});
