/**
 * Deployment parameters
 */
// The 'context' path of the deployed app (i.e. root path component
// of the full URL of the application.
const ROOT_PATH = '/mygod/'
// The path component (just adter ROOT_PATH) to all the API calls provided
// by the Django backend.
const API_ROOT_PATH = ROOT_PATH + 'api/'

/**
 * The default height (in rows) of a dashboard widget.
 * @type {number}
 */
const DEFAULT_WIDGET_HEIGHT = 10
const DEFAULT_WIDGET_WIDTH = 3

export default {
    apiRootPath: API_ROOT_PATH,

    defaultWidgetHeight: DEFAULT_WIDGET_HEIGHT,
    defaultWidgetWidth: DEFAULT_WIDGET_WIDTH,

    /**
     List storing the configurations of each dashboard component.
     The list is used by vue-grid-layout to render the components. Each list entry is a dictionary containing
     at least the following keys :
     - x : the left column of the component (starting at 0)
     - w : the with of the component (expressed in columns)
     - y : the top row of the component (starting at 0)
     - h : the height of the component (expressed in rows)
     - i : a unique identifier for the component
     - background_color : the color to use for the components title bar
     - panel_id : the id of the taxon search panel the components relates to
     - type : the type of the component (ex. 'summary', 'scatterplot', 'dategraph', 'seasonplot' ...)
     - settings : a dictionary with storing the settings specific to each widget, needed for dashboard
     configuration saving/reloading
     **/
    dashboardConfig: [],

    /**
     Dictionary with taxon search panel information.
     Keys are the panel_ids.
     **/
    panels: {},
    /**
     *  An integer value used to keep the correct ordering of the panels.
     */
    panelIndex: 0,


    /**
     The panel currently selected in the drop down list after the Search Criteria button in the Dashboard
     control area.
     **/
    selectedPanel: null,

    /**
     *  List of available graph types (displayed in the Graph Types selector in the dashboard control panel
     **/
    availableGraphtypes:
        {
            "Summary": [{text: "Dataset Information", value: 'summary'},
                {text: "Hits Taxonomy (Sunburst)", value: 'sunburst'}, {text: "Hits Taxonomy (Treemap)", value: 'treemap'}],
            "Temporal/Spatial pattern": [{text: "Discrete", value: 'temporaldiscretegraph'}, {
                text: "Continuous",
                value: 'dategraph'
            }],
            "Environment": [{
                text: "Organisms vs Environment",
                value: 'scatterplot'
            }, {text: "Parameters Distribution", value: 'pc_repartition'}],
            "Geographical Distribution": [{text: "Sample Locations", value: 'map'}],
            "Taxonomic Composition": [{text: "Barcharts", value: "barchart"}],
            "Alpha diversity": [{text: "Diversity Index", value: "alpha_div"}],
            "Beta diversity": [{text: "NMDS (Bray-Curtis/Jaccard)", value: "scatterbetanmds"}]
        }


}
