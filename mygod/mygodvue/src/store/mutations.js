import _ from "lodash"
import * as lib from './lib'
import axios from "axios"
import defaultTemporalDashboard from '../../static/defaulttemporaldashboard.json'
import defaultGeographicalDashboard from '../../static/defaultgeographicaldashboard.json'
import widgetSize from '../../static/widget_size.json'

/**
 * Called to add a set of widgets to a panel.
 * Used either on startup to generate the placeholder widget,
 * or when the user clicks the Generate Dashboard button in a Taxon Search Panel
 *
 * @param state the Vuex store
 * @param data a data structure describing the widgets to add.
 */
export const setDashboardConfig = (state, data) => {
    data.forEach((widget) => {
        widget.i = lib.guid()
        widget.panelId = state.selectedPanel.panelId
    })

    state.dashboardConfig = data || []
}


/**
 * Called whenever the user clicks the 'Generate Dashboard' (or 'Update Dashboard')
 * button of the TaxonSearchPanel.
 *
 * @param state the Vuex store
 * @param payload a dictionary with the settings of the TaxonSearchPanel :
 *                  panelId, dataset, date_start, date_end, rank, taxon, backgroundColor
 */
export const taxonSearchPanelUpdated = (state, payload) => {

    const panelId = payload.panelId


    let dashboardConfigClone = _.cloneDeep(state.dashboardConfig)
    let panelsClone = _.cloneDeep(state.panels)
    let panel = panelsClone[panelId]

    /*
     Update the name of the panel in the dashboard Search Criteria selector.
     */
    panel.name = payload.dataset + "  / " + payload.taxon
    if (!panel.haswidgets) {
        if (dashboardConfigClone.length === 1 && dashboardConfigClone[0].type === "placeholder") {
            dashboardConfigClone = []
        }

        panel.haswidgets = true
        panel["backgroundColor"] = payload.backgroundColor
        let defaultDashboard = null
        if (payload.type === "Temporal") {
            defaultDashboard = defaultTemporalDashboard
        } else {
            defaultDashboard = defaultGeographicalDashboard
        }
        let defaultDashboardClone = _.cloneDeep(defaultDashboard)
        let maxY = -1
        let heightMaxY = 0
        for (let index in dashboardConfigClone) {
            let item = dashboardConfigClone[index]
            if (maxY < item.y) {
                maxY = item.y
                heightMaxY = item.h
            }
        }
        for (let index in defaultDashboardClone) {
            let component = defaultDashboardClone[index]
            component.backgroundColor = payload.backgroundColor
            component.panelId = panelId
            component.i = "griditem-" + component.type + "-" + lib.guid()
            component.y = maxY + heightMaxY + 1
            component.settings = {'restore': false}
        }
        Array.prototype.push.apply(dashboardConfigClone, defaultDashboardClone)

    }
    panel['dataset'] = payload.dataset
    panel['rank'] = payload.rank
    panel['taxon'] = payload.taxon
    panel['dateStart'] = payload.dateStart
    panel['dateEnd'] = payload.dateEnd

    state.panels = panelsClone
    state.selectedPanel = panel

    for (let index in dashboardConfigClone) {
        if (dashboardConfigClone[index].panelId === payload.panelId) {
            dashboardConfigClone[index].triggerValue++
        }
    }

    state.dashboardConfig = dashboardConfigClone


}

/**
 * Called whenever the search panel selector (labeled Search Criteria) in the
 * dashboard control area changes.
 *
 * @param state the Vuex store
 * @param payload a dictionary with a panelId entry (id of the selected panel).
 */
export const searchPanelSelectionChanged = (state, payload) => {

    state.selectedPanel = state.panels[payload.panelId]

}

/**
 * Called whenever the user uses the Remove Panel in a Taxon Search Panel.
 *
 * @param state the Vuex store
 * @param panelId the id of the panel to be removed with all the dependent dashboard widgets.
 */
export const removeTaxonSelectorPanelWithDependencies = (state, panelId) => {


    if (state.panels[panelId]) {

        let clonedPanel = _.cloneDeep(state.panels)

        /*
            Remove all widgets associated to the panel being removed.
        */
        let dashboardConfig = state.dashboardConfig
        for (let index = 0; index < dashboardConfig.length; index++) {
            if (dashboardConfig[index].panelId === panelId) {
                dashboardConfig.splice(index, 1)
                index--
            }
        }

        /*
            Actually remove panel.
         */
        delete clonedPanel[panelId]
        state.panels = clonedPanel

        /*
            If the removed panel was the currently selected one, reset the selection
            to the first panel.
         */
        if (state.selectedPanel.panelId === panelId) {
            let selectedPanelId = Object.keys(state.panels)[0]
            state.selectedPanel = state.panels[selectedPanelId]
        }
        state.panelIndex--
    } else {
        console.warn("No panel matching " + panelId + " found in state.")
    }

}

/**
 * Called whenever the user load settings, the board is cleared before loading widgets
 * @param state the Vuex store
 */

export const clearDashboard = (state) => {

    state.dashboardConfig = []

}


/**
 * Called whenever the user clicks on the 'Reset Layout' button.
 * Useful after widget removal (either directly or through removal
 * of taxon search panel), to reposition all widgets in the dashboard.
 *
 * FIXME: fix & use per widget type default height & width.
 *
 * @param state the Vuex store
 */
export const resetDashboardLayout = (state) => {

    let DEFAULT_WIDGET_WIDTH = 3;
    let MAX_COLUMNS = 15;
    let DEFAULT_WIDGET_HEIGHT = 12;

    let configClone = _.cloneDeep(state.dashboardConfig)
    state.dashboardConfig = []

    let column = 0;
    let line = 0;

    configClone.forEach(function (widget) {
        let widgetParameters = widgetSize.find(element => {
            return element.type === widget.type;

        });
        widget.x = column
        widget.y = line
        if (widgetParameters) {
            widget.h = widgetParameters.h
            widget.w = widgetParameters.w
        } else {
            widget.h = DEFAULT_WIDGET_HEIGHT
            widget.w = DEFAULT_WIDGET_WIDTH
        }
        column = column + DEFAULT_WIDGET_WIDTH;
        if (column + DEFAULT_WIDGET_WIDTH >= MAX_COLUMNS) {
            column = 0
            line = line + widget.h
        }
    })
    state.dashboardConfig = configClone

}

/**
 * Called whenever a widget is removed by the user through a click on the
 * remove icon.
 *
 * @param state the Vuex store
 * @param widgetId the id of the widget to be removed.
 */
export const removeDashboardWidget = (state, widgetId) => {
    let widgetIndex = state.dashboardConfig.findIndex(object => {
        return object.i === widgetId;
    })
    if (widgetIndex >= 0) {
        state.dashboardConfig.splice(widgetIndex, 1)
    } else {
        console.warn("Widget not found")
    }
}

/**
 * Called when the user clicks the Add Graph button.
 * @param state the Vuex store
 * @param payload a dictionary with the following key :
 *  - type (one of the predefined widget types, cf. the Dashboard options data attribute).
 */
export const addDashboardWidget = (state, payload) => {

    let panelId = state.selectedPanel.panelId
    if (!panelId)
        return

    /**
     *  Start by determining new widget position.
     */
    let h = state.defaultWidgetHeight
    let w = state.defaultWidgetWidth

    let widgetParameters = widgetSize.find(element => {
        return element.type === payload.type;

    });
    if (widgetParameters) {
        h = widgetParameters.h
        w = widgetParameters.w
    }

    let x = 0
    let y = 0
    for (let index = 0; index < state.dashboardConfig.length; index++) {
        let widget = state.dashboardConfig[index]
        if (y <= widget.y) {
            y = widget.y
            x = 0
            if (x < (widget.x + widget.w)) {
                x = widget.x + widget.w
            }
        }
    }

    if ((x + w) > 12) {
        y = y + h
        x = 0
    }

    /**
     * Then create new widget.
     */
    let newWidget = {
        'x': x,
        'y': y,
        'w': w,
        'h': h,
        'i': 'griditem-' + payload.type + '-' + lib.guid(),
        'type': payload.type,
        "c": "",
        "triggerValue": 0,
        "backgroundColor": state.panels[panelId]['backgroundColor'],
        "panelId": panelId,
        "settings": {'restore': false},
    }

    /**
     * Finally add widget to dashboard.
     */
    state.dashboardConfig.push(newWidget)
}

/**
 * Called whenever the user clicks the 'Save' button in the dashboard control panel
 * @param state
 * @param payload a dictionary with the following keys :
 *          - name : the human readable name of the settings
 *          - info : (optional) the settings information for a previously saved settings
 */

export const saveSettings = (state, payload) => {
    // Needed to signal the menu with available (saved) datasets to wait to refresh
    // its contents.

    state.saving = true
    let data = {'name': payload.name}

    if (payload.info) {
        data['id'] = payload.info.id
        data['creationdate'] = payload.info.creationdate
    }

    data['settings'] = {
        'dashboardConfig': state.dashboardConfig,
        'panels': state.panels
    }


    axios
        .post(encodeURI(state.apiRootPath + 'save_settings/'), 'data=' + JSON.stringify(data))
        .then(response => {
            console.debug("Settings ID returned from API call: " + response.data.id)
            state.saving = false
        })
}

/**
 * Called whenever the user clicks the 'Save' button in the dashboard control panel
 * @param state
 * @param settingsId
 *          - state : the Vuex store
 *          - settings_id : the settings id (string) of the settings to be loaded.
 */
export const loadSettings = (state, settingsId) => {

    state.panelIndex = 0
    axios
        .get(encodeURI(state.apiRootPath + 'load_settings/' + settingsId))
        .then(response => {
            let data = response.data
            /*
                Let each panel know it will have to restore its settings.
             */
            let panels = data.settings.panels
            state.selectedPanel = Object.values(panels)[0]
            for (let panelId in panels) {
                let panel = panels[panelId]
                panel.restore = true
                if (state.panelIndex < panel.index) {
                    state.panelIndex = panel.index
                }
            }
            state.panelIndex++
            state.panels = panels
            /*
                Let each widget know it will need to restore its settings.
             */
            let dashboardConfig = data.settings.dashboardConfig
            for (let index = 0; index < dashboardConfig.length; index++) {
                let component = dashboardConfig[index]
                component.settings['restore'] = true
            }
            state.dashboardConfig = dashboardConfig
        })
}

/**
 * Called whenever the user clicks on the Add Taxon Search Panel button
 * @param state the Vuex store
 * @param payload a dictionary with the following keys :
 *          - state : the Vuex store
 *          - payload : a dictionary with the following keys :
 *              - panelId : the unique id of the panel
 *
 */
export const addTaxonSelectorPanel = (state, payload) => {

    const panelId = payload.panelId
    let clonedPanels = _.cloneDeep(state.panels)
    clonedPanels[panelId] = _.cloneDeep(payload)
    clonedPanels[panelId].index = state.panel_index
    clonedPanels[panelId].haswidgets = false

    state.panels = clonedPanels
    state.panelIndex++

    state.selectedPanel = state.panels[panelId]

}

/**
 * Called whenever there is state change in the settings of a dashboard widget.
 * The matching dashboardConfig entry must keep track of changes to enable
 * them to be saved properly.
 * @param state the Vuex store
 * @param payload a dictionary with the following entries :
 *          - component_id : the id of the component for which the settings will be updated
 *          - settings : a dictionary whose contents will be copied entry by entry in the ccomponents settings.
 */
export const updateDashboardWidgetSettings = (state, payload) => {

    let dashboardConfig = state.dashboardConfig
    let componentId = payload.componentId
    let settings = payload.settings


    for (let index = 0; index < dashboardConfig.length; index++) {
        let component = dashboardConfig[index]
        if (component.i === componentId) {
            for (const key in settings) {
                component.settings[key] = _.cloneDeep(settings[key])
                /**
                 * Set the restore flag to false to prevent restoring the widget's state from
                 * saved values, now that the user made some changes.
                 */
                component.settings['restore'] = false
            }
        }
    }
}


export const updateDashboardWidgetLegend = (state, payload) => {

    let dashboardConfig = state.dashboardConfig
    let componentId = payload.componentId
    let legend = payload.legend

    for (let index = 0; index < dashboardConfig.length; index++) {
        let component = dashboardConfig[index]
        if (component.i === componentId) {
            component.settings['legend'] = _.cloneDeep(legend)

        }
    }
}

/**
 * Called when the 'Duplicate' icon is clicked on a dashboard widget.
 * @param state The Vuex store
 * @param payload a dictionary with the following entries :
 *              - component_id : the ID of the component to clone.
 */

export const cloneWidget = (state, payload) => {

    let dashboardConfig = state.dashboardConfig
    let componentId = payload.componentId


    /**
     * Then clone new widget.
     */

    for (let index = 0; index < dashboardConfig.length; index++) {
        let component = dashboardConfig[index]
        if (component.i === componentId) {
            let newComponent = _.cloneDeep(component)
            newComponent.i = 'griditem-' + newComponent.type + '-' + lib.guid()
            newComponent.settings['restore'] = true
            /**
             * Set the restore flag to true to restore parameter from original component.
             */
            newComponent.restore = true
            state.dashboardConfig.push(newComponent)


        }
    }
}

/**
 * Called when the application context is initialized on startup.
 * @param state The Vuex store
 * @param applicationContext a string with the application context :
 */

export const setApplicationContext = (state, applicationContext) => {

    state.apiRootPath = applicationContext+'/api/'

}

