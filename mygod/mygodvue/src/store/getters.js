export const getDashboardConfig = (state) => {
    console.debug("Getting Resources")
    return state.dashboardConfig
}

export const getPanelList = (state) => {
    console.debug("Refreshing Taxon Selector Panels")
    let panels=[]
    for (let panelId in state.panels) {
      panels.push(state.panels[panelId])
    }
    return panels
}

export const getTaxonInfoFromPanel = (state) => (idPanel) => {
    let taxonInfo = {'taxon' : '', 'rank' : '', 'dataset' : ''}
    if (idPanel in state.panels) {
        let panelItem = state.panels[idPanel]
        if ('taxon' in panelItem) {
            taxonInfo['taxon'] = panelItem['taxon']
        }
        if ('rank' in panelItem) {
            taxonInfo['rank'] = panelItem['rank']
        }
        if ('dataset' in panelItem) {
            taxonInfo['dataset'] = panelItem['dataset']
        }
    } else {
        console.warn('No panel found with id '+idPanel+'. Number of panels = '+Object.keys(state.panels).length)
        for (const panelId in Object.keys(state.panels)) {
            console.debug("ID: "+panelId);
        }
    }
    return taxonInfo
}

export const getAvailablePanels = (state) => {
    return state.panels
}

export const getSelectedPanel = (state) => {
    return state.selectedPanel
}

export const getAvailableGraphTypes = (state) => {
    return state.availableGraphtypes
}


