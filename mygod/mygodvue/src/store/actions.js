import init from '../../static/init.json'

export const initDashboard = ({commit}) => {
    commit('setDashboardConfig', init)
}


export const addTaxonSelectorPanel = ({commit}, payload) => {
    commit('addTaxonSelectorPanel', payload)
}

export const taxonSearchPanelUpdated = ({commit}, payload) => {
    commit('taxonSearchPanelUpdated', payload)

}

export const searchPanelSelectionChanged = ({commit}, payload) => {
    commit('searchPanelSelectionChanged', payload)
}

export const removeTaxonSelectorPanelWithDependencies = ({commit}, panelId) => {
    commit('removeTaxonSelectorPanelWithDependencies', panelId)
}

export const resetDashboardLayout = ({commit}) => {
    commit('resetDashboardLayout')
}

export const addDashboardWidget = ({commit}, payload) => {
    commit('addDashboardWidget',payload)
}

export const removeDashboardWidget = ({commit}, widgetId) => {
    commit('removeDashboardWidget', widgetId)
}

export const clearDashboard = ({commit}) => {
    commit('clearDashboard')
}

export const updateDashboardWidgetSettings = ({commit}, payload) => {
    commit('updateDashboardWidgetSettings', payload)
}

export const updateDashboardWidgetLegend = ({commit}, payload) => {
    commit('updateDashboardWidgetLegend', payload)
}

export const saveSettings = ({commit}, payload) => {
    commit('saveSettings', payload)
}

export const loadSettings = ({commit}, settingsId) => {
    commit('loadSettings', settingsId)
}

export const removeWidgetQuery = ({commit}, payload) => {
    commit('removeWidgetToList', payload)
}

export const cloneWidget = ({commit}, payload) => {
    commit('cloneWidget', payload)
}

export const setApplicationContext = ({commit}, application_context) => {
    commit('setApplicationContext',application_context)
}