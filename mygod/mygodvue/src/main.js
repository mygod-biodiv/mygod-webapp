import Vue from 'vue'
import { BootstrapVue, IconsPlugin} from "bootstrap-vue";
import App from './App.vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faQuestionCircle, faSave, faArrowsAlt} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { LMap, LTileLayer, LMarker } from 'vue2-leaflet';
import 'leaflet/dist/leaflet.css';
import { Icon } from 'leaflet';
import store from './store';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'jquery/src/jquery.js';
import 'bootstrap-icons/font/bootstrap-icons.css';
import "leaflet.markercluster/dist/MarkerCluster.css";
import "leaflet.markercluster/dist/MarkerCluster.Default.css";


delete Icon.Default.prototype._getIconUrl;
Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});

library.add(faQuestionCircle, faSave, faArrowsAlt)

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.component('l-map', LMap);
Vue.component('l-tile-layer', LTileLayer);
Vue.component('l-marker', LMarker);
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)


Vue.config.productionTip = false
new Vue({
  render: h => h(App),
  store
}).$mount('#app')

