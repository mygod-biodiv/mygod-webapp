/* eslint-disable */

import axios from "axios";
import * as svg from "save-svg-as-png";

export const ApiCall = {
    methods: {
        initializeFractionSizes() {
            this.fractions = []
            this.fraction = null
            if (this.dataset) {
                let me = this
                me.apiCallsInProgress++
                axios
                    .get(this.sanitizeApiCall(this.$store.state.apiRootPath,['fraction_sizes',
                    this.dataset]))
                    .then(function (response) {
                        if (response.data.length > 0) {
                            me.fractions = response.data
                            if (me.settings['restore']) {
                                me.fraction = me.settings['fraction']
                            } else {
                                me.fraction = me.fractions[0].value
                            }
                            me.apiCallsInProgress--
                        }
                    })
            }
        },
        initializeDepthCode() {
            this.depthCodes = []
            if (this.dataset) {
                let me = this
                me.apiCallsInProgress++
                axios
                    .get(this.sanitizeApiCall(this.$store.state.apiRootPath,['depth_codes',
                    this.dataset]))
                    .then(function (response) {
                        if (response.data.length > 0) {
                            me.depthCodes = response.data
                            if (me.settings['restore']) {
                                me.depth = me.settings['depth']
                            } else {
                                me.depth = response.data[0]['depth']
                            }
                            me.apiCallsInProgress--
                        }

                    })
            }
        },

        initializeTaxons() {
            this.taxons = {}
            if (this.dataset && this.rank && this.searchPanelTaxon) {
                let me = this
                me.apiCallsInProgress++
                axios
                    .get(this.sanitizeApiCall(this.$store.state.apiRootPath,['taxon_subtree',
                    this.rank, this.searchPanelTaxon]))
                    .then(function (response) {
                        if (response.data) {
                            me.taxons = response.data
                            if (me.settings['restore']) {
                                me.taxon = me.settings['taxon']
                            } else {
                                let key = Object.keys(response.data)[0]
                                me.taxon = response.data[key][0]['value']
                            }
                        }
                        me.apiCallsInProgress--
                    })
            } else {
                console.warn("No taxon initialization performed.")
            }
        },
        initializeASVs() {
            this.asvs = {}
            if (this.dataset && this.rank && this.searchPanelTaxon) {
                let me = this
                me.apiCallsInProgress++
                axios
                    .get(this.sanitizeApiCall(this.$store.state.apiRootPath,['asv_subtree',
                    this.dataset, this.rank, this.searchPanelTaxon]))
                    .then(function (response) {
                        if (response.data) {
                            me.asvs = response.data
                            if (me.settings['restore']) {
                                me.asv = me.settings['asv']
                            } else {
                                let key = Object.keys(response.data)[0]
                                me.asv = response.data[key][0]['value']
                            }
                        }
                        me.apiCallsInProgress--
                    })
            } else {
                console.warn("No taxon initialization performed.")
            }
        },
        initializeRepresentation() {
            this.datatype = ""
            let me = this
            if (this.dataset) {
                me.apiCallsInProgress++
                axios
                    .get(this.sanitizeApiCall(this.$store.state.apiRootPath,['datatype',
                    this.dataset]))
                    .then(function (response) {
                        if (response.data) {
                            me.datatype = response.data
                            me.representationOptions = []
                            if (me.datatype === "metabarcode") {
                                me.representationOptions.push({text: 'Taxon', value: 'taxon'},
                                    {text: 'ASV', value: 'asv'},)
                                me.initializeASVs()
                            }
                            if (me.datatype === "morphological") {
                                me.representationOptions.push({text: 'Relative Abundance', value: 'taxon'},
                                    {text: 'Count', value: 'count'},)
                            }
                            if (me.settings['restore']) {
                                me.representation = me.settings['representation']
                            } else {
                                me.representation = me.representationOptions[0].value
                            }
                        }
                        me.apiCallsInProgress--
                    })
            } else {
                console.warn("No datatype of dataset retrieved.")
            }
            this.initializeTaxons()

        },
        cloneCurrentWidget() {
            this.cloneWidget({'componentId': this.componentId})
        },

        getAxisLabel() {
            /**
             * On initialization, the list of parameters may not be initialized.
             * Avoid invalid API call.
             */
            if (this.parameter.value < 0)
                return

            let me = this
            me.apiCallsInProgress++
            axios
                .get(this.sanitizeApiCall(this.$store.state.apiRootPath,['axis_label',
                    this.dataset, this.parameter['value']]))
                .then(function (response) {
                    me.axisLabel = response.data['label']
                    me.getPlotData(me.parameter.value, me.resolution, me.taxon)
                })
            me.apiCallsInProgress--
        },
        initializeParameters() {
            this.parameters = []
            this.parameter = {"text": "No Available Parameters", "value": -1}
            if (this.dataset) {
                let me = this
                me.apiCallsInProgress++
                axios
                    .get(this.sanitizeApiCall(this.$store.state.apiRootPath,['parameters',
                    this.dataset]))
                    .then(function (response) {
                        if (response.data.length > 0) {
                            me.parameters = response.data
                            if (me.settings['restore']) {
                                me.parameter = me.settings['parameter']
                            } else {
                                me.parameter = response.data[0]
                            }
                            this.drawPlot()
                        }
                    })
                me.apiCallsInProgress--
            }
        },
        initializeGroupBy() {
            let me = this
            if (me.settings['restore']) {
                me.groupBy = me.settings['groupBy']
            } else {

                me.groupBy = this.groupByOptions[0].value

            }
        },
        initializeSplitBy() {
            let me = this
            if (me.settings['restore']) {
                me.splitBy = me.settings['splitBy']
                me.advanced = me.settings['advanced']
            } else {

                me.splitBy = null

            }
        },
        initializeLegend() {
            let me = this
            if (me.settings['restore']) {

                me.legend = me.settings['legend']
                if (me.legend !== null){
                    me.visible=true
                }
            } else {

                me.legend = null

            }
        },

        reformatData() {
            this.formattedData = []
            for (let pool of this.plotData.data) {
                let group_name = pool['group']
                for (let values of pool['values']) {
                    this.formattedData.push(values)
                }
            }
        },

        sanitizeApiCall(baseUri,pathComponents) {
            let sanitizedUri = encodeURI(baseUri)
            if (pathComponents.length > 0) {
                if ( !sanitizedUri.endsWith("/")) {
                    sanitizedUri += '/'
                }
                let sanitizedPathComponents = []
                pathComponents.forEach(component => sanitizedPathComponents.push(encodeURIComponent(component)))
                sanitizedUri += sanitizedPathComponents.join("/")
            }
            return sanitizedUri
        },

    }
}