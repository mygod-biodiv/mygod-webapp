/* eslint-disable */
import * as d3 from "d3";

export const loadingCircle = {
    methods: {
        spinningCircle(margin,width, height) {
            let svg = d3.select(this.divElement)
                .append("svg")
                .append('g')
                .attr("viewBox", `0 0 ${width + margin.left + margin.right} ${height + margin.top + margin.bottom}`)
                .attr("transform",
                    `translate(${margin.left}, ${margin.top})`);

// main circle
            const CX = width / 2;
            const CY = height / 2;
            const R = 20;
// little circles
            const r = 2;

// degree - radian conversion
            function radian(degree) {
                return degree * Math.PI / 180;
            }
// parametric equation of a circle is : x=cx+r*cos(t) and y=cy+r*sin(t)
            function x(radian) {
                return CX + R * Math.cos(radian);
            }
            function y(radian) {
                return CY + R * Math.sin(radian);
            }

// 10 equal sectors for 10 points on the main circle
            const SECTORS = 10; // number of sectors
            const SECTOR = 360 / SECTORS; // sector in degree: each equal
            let anglesList = [];

            for (let index = 0; index < SECTORS; index++) {
                anglesList.push(radian(index * SECTOR));
            }
            const MAX = anglesList[SECTORS - 1];

// opacity
            function getOpacity(datum) {
                return Number((datum / MAX).toFixed(1));
            }

// little circle radius
            function getRadius(datum) {
                return Number(r + r * (datum / MAX).toFixed(1));
            }

            function update(dataset) {
                let littleCircle = svg.selectAll("#little").data(dataset);

                // UPDATE
                // Update old elements as needed.
                littleCircle
                    .attr("r", function(d) {
                        return getRadius(d);
                    })
                    .style("opacity", function(d) {
                        return getOpacity(d);
                    });

                // ENTER
                // Create new elements as needed.
                littleCircle
                    .enter()
                    .append("circle")
                    .attr("id", "little")
                    .attr("fill", "#439DB7")
                    .attr("cx", function(d) {
                        return x(d);
                    })

                    .attr("cy", function(d) {
                        return y(d);
                    })
                    .attr("r", function(d) {
                        return getRadius(d);
                    })
                    .style("opacity", function(d) {
                        return getOpacity(d);
                    });

                // EXIT
                // Remove old elements as needed.
                // littleCircle.exit().remove();
            }

            update(anglesList);

            function arrayRotate(arr, count) {
                count -= arr.length * Math.floor(count / arr.length);
                arr.push.apply(arr, arr.splice(0, count));
                return arr;
            }

// cyclic circular permutation
            d3.interval(function() {
                anglesList = arrayRotate(anglesList.slice(), -1);
                update(anglesList);
            }, 150);

        },

        notEnoughData(margin,width, height) {
            let svg = d3.select(this.divElement)
                .append("svg")
                .append('g')
                .attr("viewBox", `0 0 ${width + margin.left + margin.right} ${height + margin.top + margin.bottom}`)
                .attr("transform",
                    `translate(${margin.left}, ${margin.top})`);

            svg.append("text")
                .attr("text-anchor", "end")
                .attr("x", width/2 + 110)
                .attr("y", height /2 )
                .style("font-size", "100%")
                .style("font-family", "Avenir, Helvetica, Arial, sans-serif")
                .text("Not enough data to display");

        }
    }
}