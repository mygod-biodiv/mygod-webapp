module.exports = {
    filenameHashing: false,
    outputDir : 'vue',
    devServer: {
        host: '127.0.0.1',
        proxy: {
            '^/mygod/api' : {
                target : 'http://127.0.0.1:8000/'
            }
        }
    }
}
