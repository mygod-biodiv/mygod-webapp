# Input
beta_diversity_distance_matrix = function(asv_table,samples,fraction, dissimilarity_type) {
# samples to keep

if (dissimilarity_type == "bray_curtis"){
    matrix_method = "total"
    par_dist_method = "bray"
}

if (dissimilarity_type == "jaccard"){
    matrix_method = "pa"
    par_dist_method = "binary"
}
setDT(asv_table)
setDT(samples)
samples_to_keep <- samples[min_frac_size %in% fraction,sample_id]

asv_table_matrix <- asv_table[, .SD, .SDcols = samples_to_keep]

asv_table_matrix <- t(as.matrix(asv_table_matrix))

# asv_table_matrix_stdz <- apply(asv_table_matrix,2,\(X) X/sum(X)) |> t()
asv_table_matrix_total <- vegan::decostand(asv_table_matrix, method = matrix_method)

asv_dist <- parallelDist::parDist(asv_table_matrix_total,
                      method=par_dist_method,
                      threads=2)

asv_dist_nmds <- vegan::metaMDS(asv_dist,
                                   k = 2,
                                   try = 5,
                                   trymax = 10,
                                   engine = "monoMDS")



asv_dist_nmds_scores <- vegan::scores(asv_dist_nmds,display="sites") |>
  data.table(keep.rownames = TRUE)

asv_dist_nmds_scores <- merge(samples,
                                 asv_dist_nmds_scores,
                                 by.x = "sample_id",
                                 by.y = "rn")




asv_dist_nmds_scores <- data.table(asv_dist_nmds_scores)
stress <- asv_dist_nmds$stress
as.data.frame(asv_dist_nmds_scores)
return(list(val1=asv_dist_nmds_scores, val2=stress))
}
