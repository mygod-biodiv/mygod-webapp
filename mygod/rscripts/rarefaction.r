rarefaction = function(data_input) {
    seq_depth <- min(apply(data_input, 2, sum))
    nthreads <- 4
    raref_res <- rtk::rtk(input = data_input,
                          depth = seq_depth,
                          repeats = 1,
                          threads = nthreads,
                          ReturnMatrix = 1,
                          seed = 1)

    raref_res <- data.frame(raref_res$raremat)
    x <- rowSums(raref_res) > 0
    raref_res[x, ]
    return(raref_res)
}
