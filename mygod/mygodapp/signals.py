from django.contrib.auth.models import User
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save
from django.conf import settings
from django.core.mail import send_mail

import logging

logger = logging.getLogger(__name__)


@receiver(pre_save, sender=User, dispatch_uid="SetInactiveUser")
def set_new_user_inactive(sender, instance, **kwargs):
    user = instance
    if user._state.adding is True:
        user.is_active = False


# signal used for is_active=False to is_active=True
@receiver(pre_save, sender=User, dispatch_uid='active')
def active(sender, instance, **kwargs):
    user = instance
    if user.is_active and User.objects.filter(pk=user.pk, is_active=False).exists():
        subject = 'MyGOD Account Activation'
        message = '%s your account is now active %s' % (user.username, settings.SITE_DOMAIN + '/' + settings.APPLICATION_CONTEXT+'/login/')
        if hasattr(settings, 'DISABLE_MAIL_SENDING') is False or settings.DISABLE_MAIL_SENDING is False:
            send_mail(subject, message, settings.FROM_EMAIL, [user.email], fail_silently=False)
        else:
            logger.debug(f"Mail not sent : {subject}, {message}, {settings.DEFAULT_FROM_EMAIL}, {user.email}")


# signal to send an email to the admin when a user creates a new account
@receiver(post_save, sender=User, dispatch_uid='register')
def register(sender, instance, **kwargs):
    user = instance
    logger.info("Registering new user account")
    if kwargs.get('created', False):
        subject = f"MyGOD Registration Request for {user.first_name} {user.last_name}"
        message = f"The user {user.first_name} {user.last_name} (email: {user.email}) has requested a MyGOD account.\n"\
                  f"Please check the request using:  {settings.SITE_DOMAIN}/{settings.APPLICATION_CONTEXT}/bo/admin/"
        admins = User.objects.filter(is_superuser=True, is_active=True)
        admin_emails = [admin.email for admin in admins]
        logger.info(f"Admin emails: {admin_emails}")
        if hasattr(settings, 'DISABLE_MAIL_SENDING') is False or settings.DISABLE_MAIL_SENDING is False:
            send_mail(subject, message, settings.FROM_EMAIL, admin_emails, fail_silently=False)
        else:
            logger.debug(f"Mail not sent : {subject}, {message}, {','.join(admin_emails)}, {user.email}")

