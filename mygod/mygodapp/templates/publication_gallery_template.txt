{% autoescape off %}
Hello, {{ publisher_name }}


{{ from_user }} submitted a new gallery entry for approval.

Title : {{ title }}
Summary : {{ summary }}


Your role as publisher is to check the text, images and any links added to the dashboard before publication in the gallery.

{{ site_root_url }}{% url 'user_page' %}

Sincerely,
The Website Team

{% endautoescape %}
