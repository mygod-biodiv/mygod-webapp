import uuid as uuid
from django.db import models
from django.contrib.auth.models import User
from datetime import date, datetime
from django.conf import settings


class Taxonomy(models.Model):
    name = models.CharField(max_length=50, unique=False)
    version = models.CharField(max_length=100, unique=False)
    rank_description = models.JSONField()

    def __str__(self):
        return str(
            (self.name, self.version, self.rank_description)
        )


class Taxon(models.Model):
    parent = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True, related_name='children')
    rank = models.CharField(max_length=200, unique=False, default="NA")
    name = models.CharField(max_length=200, unique=False, default="NA")
    compiled = models.CharField(max_length=1000, unique=False, default="NA")
    taxonomy_linked = models.ForeignKey(Taxonomy, null=True, blank=True, on_delete=models.CASCADE)
    children_list = models.JSONField(default=list)
    taxon_id = models.CharField(max_length=200, default="NA")

    def __str__(self):
        return str((self.rank,
                    self.name, self.compiled, self.taxonomy_linked, self.children_list))


class Observation(models.Model):
    taxon = models.ForeignKey(Taxon, null=True, blank=True, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class Sequence(Observation):
    amplicon = models.CharField(max_length=1000, unique=False)
    sequence = models.CharField(max_length=1000, unique=False)
    score = models.FloatField()

    def __str__(self):
        return str((self.amplicon, self.sequence, self.score, self.taxon))


class MorphObs(Observation):
    observator = models.CharField(max_length=1000, unique=False)
    descriptor = models.CharField(max_length=1000, unique=False)

    def __str__(self):
        return str((self.observator, self.descriptor, self.taxon))


class DataSet(models.Model):
    class TypeChoices(models.TextChoices):
        TEMPORAL = 'Temporal'
        SPATIAL = "Spatial"

    class StatusChoices(models.TextChoices):
        DRYRUN = "dry_run"
        VALID = "valid"
        IN_PROGRESS = "in_progress"

    name = models.CharField(max_length=100, unique=False)
    type = models.CharField(max_length=100, choices=TypeChoices.choices, default=TypeChoices.TEMPORAL)
    datatype = models.CharField(max_length=100, unique=False, null=True)
    campaign = models.CharField(max_length=100, unique=False)
    project = models.CharField(max_length=100, unique=False, null=True, blank=True)
    doi = models.CharField(max_length=100, unique=False, null=True, blank=True)
    description = models.CharField(max_length=100, unique=False, null=True, blank=True)
    first_sample_date = models.DateField(default=datetime(2099, 1, 1).date())
    last_sample_date = models.DateField(default=datetime(1901, 1, 1).date())
    site = models.CharField(max_length=100, unique=False)
    taxonomy = models.ForeignKey(Taxonomy, null=True, blank=True, on_delete=models.CASCADE)
    pc_parameter_list = models.JSONField(default=list)
    fraction = models.JSONField(default=list)
    related_publications = models.JSONField(default=list)
    is_part_of = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True, related_name="parent")
    includes = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True, related_name="children")
    text = models.TextField(blank=True)
    status = models.CharField(max_length=100, choices=TypeChoices.choices, default=StatusChoices.IN_PROGRESS)
    base_layers = models.JSONField(default=list)


    class Meta:
        permissions = (
            ('view_dataset_init', 'View dataset upload'),
        )


class MetabarDataSet(DataSet):
    marker = models.CharField(max_length=100, unique=False, blank=True)
    minimal_identity = models.CharField(max_length=100, unique=False, null=True, blank=True)

    class Meta:
        permissions = (
            ('view_dataset_init', 'View dataset upload'),
        )

    def __str__(self):
        return str(
            (self.name, self.campaign, self.project, self.description, self.description, self.first_sample_date,
             self.last_sample_date,
             self.is_part_of, self.includes, self.fraction, self.marker))

    def get_fields(self):
        return [self.name, self.project, self.doi, self.minimal_identity, self.site, self.pc_parameter_list,
                self.description,
                self.first_sample_date, self.last_sample_date, self.fraction, self.marker]


class MorphoDataSet(DataSet):
    class Meta:
        permissions = (
            ('view_dataset_init', 'View dataset upload'),
        )

    def __str__(self):
        return str(
            (self.name, self.campaign, self.project, self.description, self.description, self.first_sample_date,
             self.last_sample_date,
             self.is_part_of, self.includes, self.fraction))

    def get_fields(self):
        return [self.name, self.project, self.doi, self.site, self.pc_parameter_list,
                self.description,
                self.first_sample_date, self.last_sample_date, self.fraction]


class Event(models.Model):
    event_id = models.CharField(max_length=200, unique=False)
    type = models.CharField(max_length=200, unique=False)
    date = models.DateField()
    depth_code = models.CharField(max_length=200, unique=False, null=True, blank=True)
    station_code = models.CharField(max_length=200, unique=False, null=True, blank=True)
    min_depth = models.FloatField()
    max_depth = models.FloatField(null=True, blank=True)
    latitude = models.FloatField(null=True, blank=True)
    longitude = models.FloatField(null=True, blank=True)
    bind_dataset = models.ManyToManyField(DataSet)
    cluster = models.CharField(max_length=100, unique=False, blank=True, null=True, default="No cluster")

    def __str__(self):
        return str((self.event_id, self.type, self.date, self.min_depth, self.max_depth, self.latitude, self.longitude,
                    self.station_code, self.depth_code))


class Occurrence(models.Model):
    occurrence_id = models.CharField(max_length=1000, unique=False)
    event = models.ForeignKey(Event, null=True, blank=True, on_delete=models.CASCADE)
    from_taxon = models.ForeignKey(Taxon, null=True, blank=True, on_delete=models.CASCADE)
    from_dataset = models.ForeignKey(DataSet, on_delete=models.CASCADE)

    def __str__(self):
        return str((
            self.occurrence_id, self.event,
            self.from_taxon, self.from_dataset))


class CumulatedMeasurement(models.Model):
    from_occurrence = models.ForeignKey(Occurrence, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, unique=False)
    value = models.FloatField(default=0.0)
    fraction_size_cumulated = models.CharField(max_length=50)

    def __str__(self):
        return str(
            (self.from_occurrence
             , self.name, self.value, self.fraction_size_cumulated)
        )


class Measure(models.Model):
    from_occurrence = models.ForeignKey(Occurrence, null=True, blank=True, on_delete=models.CASCADE)
    link_sequence = models.ForeignKey(Sequence, null=True, blank=True, on_delete=models.CASCADE)
    link_morpho = models.ForeignKey(MorphObs, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=50, unique=False)
    type = models.CharField(max_length=100, unique=False)
    methode = models.CharField(max_length=200, unique=False)
    value = models.FloatField(null=True)
    fraction_size = models.CharField(max_length=50)

    def __str__(self):
        return str(
            (self.from_occurrence, self.type, self.link_sequence, self.methode, self.value, self.name,
             self.fraction_size))


class PcReleve(models.Model):
    site = models.CharField(max_length=100, unique=False)
    date = models.DateField()
    profondeur = models.CharField(max_length=10, unique=False, null=True, blank=True)
    coef_maree = models.FloatField(verbose_name="Tidal Coefficient", null=True, blank=True)
    temperature = models.FloatField(verbose_name="Temperature", null=True, blank=True)
    temperature_unit = models.CharField(max_length=10, unique=False, null=True, blank=True)
    s = models.FloatField(verbose_name="Salinity", null=True, blank=True)
    s_unit = models.CharField(max_length=10, unique=False, null=True, blank=True)
    ph = models.FloatField(verbose_name="pH", null=True, blank=True)
    o = models.FloatField(verbose_name="Dissolved Oxygen", null=True, blank=True)
    o_unit = models.CharField(max_length=10, unique=False, null=True, blank=True)
    nh4 = models.FloatField(verbose_name="Ammonium", null=True, blank=True)
    nh4_unit = models.CharField(max_length=10, unique=False, null=True, blank=True)
    no3 = models.FloatField(verbose_name="Nitrate", null=True, blank=True)
    no3_unit = models.CharField(max_length=10, unique=False, null=True, blank=True)
    no2 = models.FloatField(verbose_name="Nitrite", null=True, blank=True)
    no2_unit = models.CharField(max_length=10, unique=False, null=True, blank=True)
    po4 = models.FloatField(verbose_name="Phosphate", null=True, blank=True)
    po4_unit = models.CharField(max_length=10, unique=False, null=True, blank=True)
    chla = models.FloatField(verbose_name="Chlorophylle A", null=True, blank=True)
    chla_unit = models.CharField(max_length=10, unique=False, null=True, blank=True)
    event = models.ForeignKey(Event, null=True, blank=True, on_delete=models.CASCADE)
    custom_parameters = models.JSONField(default=dict)


class DashboardSettings(models.Model):
    class StatusChoices(models.TextChoices):
        PRIVATE = 'private'
        SHARED = "shared"

    user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    shared = models.BooleanField(null=False, default=False)
    uuid = models.CharField(null=False, max_length=256)
    name = models.CharField(max_length=100, unique=False)
    settings = models.JSONField(null=True)
    created = models.DateTimeField(null=False)
    last_saved = models.DateTimeField(null=False)
    status = models.CharField(max_length=64, choices=StatusChoices.choices, default=StatusChoices.PRIVATE)
    promoted = models.BooleanField(default=False)

    def __str__(self):
        return str(
            (self.user, self.name, str(self.created), str(self.last_saved)))


class Request(models.Model):
    from_user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    dataset = models.CharField(max_length=100, unique=False)
    taxon = models.CharField(max_length=100, unique=False)
    rank = models.CharField(max_length=100, unique=False)

    def __str__(self):
        return str(
            (self.from_user, self.dataset, self.taxon, self.rank))


class GalleryEntry(models.Model):
    class StatusChoices(models.TextChoices):
        PRIVATE = 'private'
        SHARED = "shared"
        PUBLISHED = "published"

    bind_dashboard = models.ForeignKey(DashboardSettings, null=True, blank=True, on_delete=models.CASCADE)
    title = models.CharField(max_length=150, null=True)
    image = models.ImageField(upload_to=settings.GALLERY_ROOT, null=True)
    summary = models.TextField(max_length=288, blank=True, null=True)
    text = models.TextField(blank=True)
    last_saved = models.DateTimeField(null=True)
    status = models.CharField(max_length=64, choices=StatusChoices.choices, default=StatusChoices.PRIVATE)
    awaiting_publication = models.BooleanField(default=False)
    rejected = models.BooleanField(default=False)

    def __str__(self):
        return str(
            (self.bind_dashboard, self.image, self.summary, self.status, self.awaiting_publication))


class ImportDataRequest(models.Model):
    class StatusChoices(models.TextChoices):
        WAITING = "waiting"
        PROGRESS = 'in_progress'
        ERROR = "error"
        COMPLETED = "complete"

    from_user = models.ForeignKey(User, null=True, blank=True, on_delete=models.CASCADE)
    dataset_name = models.CharField(max_length=350)
    state = models.CharField(max_length=100, choices=StatusChoices.choices, default=StatusChoices.WAITING)
    date = models.DateTimeField(null=True)
    uuid = models.CharField(max_length=500, null=True)
    type = models.CharField(max_length=200, blank=True, null=True)
    dry_run = models.BooleanField(default=False)
    files = models.BooleanField(default=False)
