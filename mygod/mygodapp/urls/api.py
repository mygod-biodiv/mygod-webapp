from django.urls import path
from mygodapp.views import api as api_views  # import views so we can use them in urls.

app_name = "mygodapp_api"
urlpatterns = [

    path('settings/', api_views.list_settings, name='list_settings'),
    path('save_settings/', api_views.save_settings, name='save_settings'),
    path('load_settings/<str:settings_id>', api_views.load_settings, name='load_settings'),

    path('dataset_type/<path:dataset_name>', api_views.get_dataset_type, name="type_from_dataset"),
    path('gallery_entry/<str:uuid>', api_views.retrieve_gallery_entry, name="retrieve_gallery_entry"),
    path('dataset_names/', api_views.get_dataset_names, name='get_dataset_names'),
    path('dates/<path:dataset_name>', api_views.get_dataset_date_interval, name='get_dataset_date_interval'),
    path('taxonomy_ranks/<path:dataset_name>', api_views.get_taxonomy_ranks, name='get_taxonomy_ranks'),
    path('taxonomy_subranks/<path:dataset_name>/<str:rank>/<str:taxon>', api_views.get_taxonomy_subranks,
         name='get_taxonomy_subranks'),
    path('taxon_proposals/<str:rank>/<str:taxon_header>/<path:dataset_name>', api_views.get_taxon_proposals,
         name="get_taxon_proposals"),
    path('taxonomic_tree/<path:dataset_name>', api_views.get_taxonomic_tree_for_dataset,
         name="get_taxonomic_tree_for_dataset"),
    path('matches_for_summary/<path:dataset_name>/<str:rank>/<str:taxon>/<str:date_start>/<str:date_end>',
         api_views.get_matches_for_summary,
         name="get_matches_for_summary"),
    path('parameters/<path:dataset_name>', api_views.get_physico_chemical_parameters_for_dataset, name="get_parameters"),
    path('fraction_sizes/<path:dataset_name>', api_views.get_fraction_sizes_for_dataset, name='get_fraction_sizes'),
    path('datatype/<path:dataset_name>', api_views.get_dataset_datatype, name='get_dataset_datatype'),
    path('depth_codes/<path:dataset_name>', api_views.get_depth_codes_for_dataset, name='get_depth_codes'),
    path('taxon_subtree/<str:rank>/<str:taxon_name>', api_views.get_taxon_subtree,
         name="get_taxon_subtree"),
    path('asv_subtree/<path:dataset_name>/<str:rank>/<str:taxon_name>', api_views.get_asv_subtree,
         name="get_asv_subtree"),
    path('date_list/<path:dataset_name>/<str:date_start>/<str:date_end>', api_views.get_date_list_for_dataset,
         name="get_date_list_for_dataset"),
    path('metadata/<path:dataset_name>', api_views.get_dataset_metadata,
         name="get_dataset_metadata"),
    path('sequences_as_fasta/<path:dataset_name>/<str:taxon_name>', api_views.get_fasta_sequences_for_taxon_in_dataset,
         name="get_sequences_as_fasta"),
    path(
        'relative_abundances_taxon/<path:dataset_name>/<str:rank>/<str:taxon_name>/<str:fraction>/<str:parameter>/<str:date_start>/<str:date_end>/<str:depth>/<str:representation>/<str:split_by>',
        api_views.get_relative_abundances_for_taxon, name="get_relative_abundances_taxon"),
    path(
        'relative_abundances_asv/<path:dataset_name>/<str:asv_list>/<str:fraction>/<str:parameter>/<str:date_start>/<str:date_end>/<str:depth>/<str:split_by>',
        api_views.get_relative_abundances_for_asv, name="get_relative_abundances_asv"),
    path(
        'discrete_distribution_taxon/<path:dataset_name>/<str:rank>/<str:taxon_name>/<str:fraction>/<str:date_start>/<str:date_end>/<str:depth>/<str:group_by>/<str:representation>/<str:split_by>',
        api_views.get_discrete_temporal_distribution_for_taxon, name="get_seasonal_distribution_taxon"),
    path(
        'discrete_distribution_asv/<path:dataset_name>/<str:asv_list>/<str:fraction>/<str:date_start>/<str:date_end>/<str:depth>/<str:group_by>/<str:split_by>',
        api_views.get_discrete_temporal_distribution_for_asv, name="get_seasonal_distribution_asv"),

    path('axis_label/<path:dataset_name>/<str:parameter>', api_views.get_axis_label, name='get_axis_label'),

    path(
        'occurrences_by_date_taxon/<path:dataset_name>/<str:rank>/<str:taxon_name>/<str:fraction>/<str:date_start>/<str:date_end>/<str:depth>/<str:representation>/<str:split_by>',
        api_views.get_occurrences_by_date_for_taxon, name="get_occurrences_by_date_taxon"),
    path(
        'occurrences_by_date_asv/<path:dataset_name>/<str:asv_list>/<str:fraction>/<str:date_start>/<str:date_end>/<str:depth>/<str:split_by>',
        api_views.get_occurrences_by_date_for_asv, name="get_occurrences_by_date_asv"),
    path(
        'nested_abundances_taxon/<path:dataset_name>/<str:rank>/<str:taxon_name>/<str:fraction>/<str:date_start>/<str:date_end>/<str:depth>',
        api_views.get_nested_abundance_for_taxon, name="get_nested_abundance_for_taxon"),
    path(
        'grouped_abundances/<path:dataset_name>/<str:rank>/<str:taxon_name>/<str:fraction>/<str:date_start>/<str:date_end>/<str:group_by>/<str:sublevel>/<str:data_type>/<str:number_displayed>/<str:split_by>',
        api_views.get_grouped_abundances_api, name="get_grouped_abundances_api"),

    path(
        'abundances_with_coordinates/<path:dataset_name>/<str:rank>/<str:taxon_name>/<str:fraction>/<str:date_start>/<str:date_end>/<str:depth>',
        api_views.get_abundances_with_coordinates, name="get_abundances_with_coordinates"),
    path('station_reference_coordinates/<path:dataset_name>', api_views.get_station_reference_coordinates,
         name="get_station_reference_coordinates"),
    path(
        'physico_chemical_repartition/<path:dataset_name>/<str:date_start>/<str:date_end>/<str:pc_parameter>/<str:group_by>/<str:depth>/<str:split_by>',
        api_views.get_physico_chemical_repartition, name="get_physico_chemical_repartition"),

    path(
        'beta_diversity_dissimilarity/<path:dataset_name>/<str:rank>/<str:taxon_name>/<str:fraction>/<str:date_start>/<str:date_end>/<str:dissimilarity_type>',
        api_views.get_beta_diversity_dissimilarity, name="get_beta_diversity_dissimilarity"),

    path(
        'physico_chemical_beta_diversity/<path:dataset_name>/<str:date_start>/<str:date_end>/<str:parameter>',
        api_views.get_physico_chemical_data_beta_diversity, name="get_physico_chemical_data_beta_diversity"),

    path(
        'alpha_diversity/<path:dataset_name>/<str:rank>/<str:taxon_name>/<str:fraction>/<str:date_start>/<str:date_end>/<str:group_by>/<str:split_by>',
        api_views.get_alpha_diversity, name="get_alpha_diversity"),
    path(
        'base_layers/<path:dataset_name>',
        api_views.get_base_layers, name="get_base_layers"),

]
