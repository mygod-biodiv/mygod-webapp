from django.urls import path
from mygodapp.views import gallery as gallery_views  # import dashboard_views so we can use them in urls.

app_name = "mygodapp_gallery"
urlpatterns = [
    path('edit/<str:status>', gallery_views.edit_gallery_entry,
         name="edit_gallery_entry"),
    path('edit/<str:status>/<str:uuid>', gallery_views.edit_gallery_entry,
         name="edit_gallery_entry"),
    path('edit/<str:status>/<str:uuid>/<str:publication_statut>',
         gallery_views.edit_gallery_entry,
         name="edit_gallery_entry"),
    path('remove/<str:uuid>', gallery_views.remove_gallery_entry,
         name="remove_gallery_entry"),
    path('reject/', gallery_views.reject_gallery_entry,
         name="reject_gallery_entry"),
    path('reject/<str:uuid>', gallery_views.reject_gallery_entry),
    path('change_status/<str:status>/<str:uuid>', gallery_views.change_gallery_entry_status,
         name="change_gallery_status")

]
