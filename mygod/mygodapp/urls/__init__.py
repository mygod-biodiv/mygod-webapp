from mygodapp.urls.api import urlpatterns as api_urls
from mygodapp.urls.frontend import urlpatterns as frontend_urls
from mygodapp.urls.gallery import urlpatterns as gallery_urls
from django.urls import path, include

app_name = 'mygodapp'

urlpatterns = [
    path('api/', include(api_urls)),
    path('gallery/', include(gallery_urls)),
]

urlpatterns += frontend_urls
