from django.views.generic.base import TemplateView
from django.urls import path
from mygodapp.views import frontend as frontend_views  # import frontend_views so we can use them in urls.

app_name = "mygodapp_frontend"
urlpatterns = [
    path('', frontend_views.index, name="index"),
    path('search/', frontend_views.search, name="search"),
    path('search/<str:uuid>/<str:type>', frontend_views.search, name="search"),
    path('gallery/', frontend_views.gallery, name="gallery"),
    path('dataset/', frontend_views.dataset_description, name="description"),
    path('doc/', TemplateView.as_view(template_name="documentation.html"), name="documentation"),
    path('changelog/', TemplateView.as_view(template_name="changelog.html"), name="changelog"),

    path('dashboard/remove/<str:uuid>', frontend_views.remove_dashboard,
         name="remove_dashboard"),
    path('dashboard/share/<str:uuid>/<str:status>', frontend_views.share_dashboard,
         name="share_dashboard"),
    path('dataset/edit_metadata/', frontend_views.modify_metadata_dataset,
         name="modify_metadata_dataset"),
    path('dataset/edit_metadata/<path:dataset_name>', frontend_views.modify_metadata_dataset,
         name="modify_metadata_dataset"),
    path('import_metabar_data/', frontend_views.import_metabar_data, name="import_metabar_data"),
    path('import_dataset_in_database/<str:pid>/<str:data_type>',
         frontend_views.import_dataset_in_database, name="import_dataset_in_database"),
    path('check_process/<str:pid>/<str:data_type>/<str:dry_run>', frontend_views.check_process, name="check_process"),
    path('import_physico_chemical_data/', frontend_views.import_physico_chemical_data,
         name="import_physico_chemical_data"),
    path('display_logs/<str:pid>', frontend_views.display_logs, name='display_logs'),
    path('remove_files/<str:pid>', frontend_views.remove_files, name='remove_files'),
    path('remove_metabar_dataset/<path:dataset_name>', frontend_views.remove_metabar_dataset,
         name='remove_metabar_dataset'),
    path('dataset/edit_cluster/', frontend_views.modify_cluster_dataset,
         name="modify_cluster_dataset"),
    path('dataset/edit_cluster/<path:dataset_name>', frontend_views.modify_cluster_dataset,
         name="modify_cluster_dataset"),

    ## Auth frontend_views
    path('user/', frontend_views.user_page, name="user_page"),
    path('login/', frontend_views.login_request, name="sign_in"),
    path('logout/', frontend_views.logout, name="logout"),
    path('register/', frontend_views.register_request, name='sign_up'),
    path("password_reset", frontend_views.password_reset_request, name="password_reset"),

]
