import csv
import json
import logging
import re
from datetime import datetime
import pandas as pd
import rpy2.robjects as ro
from django.contrib.auth.models import Group, User
from guardian.shortcuts import assign_perm
from rpy2.robjects import pandas2ri
from rpy2.robjects.conversion import localconverter
from rpy2.robjects.packages import STAP
import requests
from mygodapp.models import Event, Taxon, Occurrence, Measure, Sequence, PcReleve, DataSet, CumulatedMeasurement, \
    Taxonomy, MorphoDataSet, MorphObs
from mygodapp.tools.commons import DataWithConfigLoader, MetaDataLoader, fill_taxon_children_list


class MorphoMetaDataLoader(MetaDataLoader):
    LOGGER = logging.getLogger(__name__)

    def process_dataset_creation(self, metadata):
        (self.dataset, created) = MorphoDataSet.objects.get_or_create(name=metadata["name"],
                                                                      description=metadata["description"],
                                                                      datatype="morphological",
                                                                      fraction=metadata["fraction"],
                                                                      taxonomy=self.taxonomy,
                                                                      campaign=metadata['campaign'],
                                                                      site=metadata['site'],
                                                                      pc_parameter_list=metadata["pc_parameter"],
                                                                      type=metadata["datatype"],
                                                                      related_publications=metadata[
                                                                          'related_publications'])


class MorphologyDataLoader(DataWithConfigLoader):

    def __init__(self, dataset, taxonomy):
        super().__init__()

        self.dataset = dataset
        self.taxonomy = taxonomy
        self.events = None

    def process_events(self, row):
        error_rows = []
        cluster = None
        try:
            date_string = row[self.config["DATE_TIME_INDEX"]]
            date_format = self.config["DATE_FORMAT"]
            datetime_object = datetime.strptime(date_string, date_format)
            date_object = datetime_object.date()
            if (self.dataset.last_sample_date is None) or (date_object > self.dataset.last_sample_date):
                self.dataset.last_sample_date = date_object
            if (self.dataset.first_sample_date is None) or (date_object < self.dataset.first_sample_date):
                self.dataset.first_sample_date = date_object
        except ValueError:
            message = f"Row {row}: date field ({date_string}) could not be parsed according to format {date_format}."
            self.LOGGER.error(message)
            error_rows.append(message)

        if cluster:
            cluster_parameter = row[self.config["CLUSTER_INDEX"]]
        else:
            cluster_parameter = "No cluster"

        event_id = None
        event_id_string = row[self.config["SAMPLE_ID_INDEX"]]
        depth_min = row[self.config['DEPTH_MIN_INDEX']]
        if isinstance(depth_min, str):
            depth_min = self.config['DEFAULT_DEPTH_MIN']
        parameter = self.config["FRACTION_SIZE"]

        (event, created) = Event.objects.get_or_create(event_id=row[self.config["SAMPLE_ID_INDEX"]],
                                                       date=date_object,
                                                       type="morpho",
                                                       station_code=row[self.config["STATION_CODE_INDEX"]],
                                                       depth_code=row[self.config['DEPTH_CODE_INDEX']],
                                                       min_depth=depth_min,
                                                       latitude=row[self.config["LATITUDE_INDEX"]],
                                                       longitude=row[self.config["LONGITUDE_INDEX"]],
                                                       cluster=cluster_parameter
                                                       )
        event.bind_dataset.add(self.dataset)
        event.save()
        self.events[event_id_string] = {'event': event, 'parameter': parameter}

        if len(self.events) == 0:
            self.LOGGER.warning("Dataset has no valid events. No data will be associated.")

        if self.dataset.first_sample_date is None:
            self.LOGGER.warning("Dataset has no valid first sample date. Assigning 1901-01-01")
            self.dataset.first_sample_date = datetime(1901, 1, 1).date()
        if self.dataset.last_sample_date is None:
            self.LOGGER.warning("Dataset has no valid last sample date. Assigning 2999-12-31")
            self.dataset.last_sample_date = datetime(2999, 12, 31).date()

        self.dataset.save()
        for error_row in error_rows:
            self.LOGGER.debug(error_row)

        return event

    def get_taxons_from_nested_classification(self, full_taxonomy, compiled_taxonomy, parent):
        compiled_taxonomy += full_taxonomy['scientificname']
        (taxon, created) = Taxon.objects.get_or_create(name=full_taxonomy['scientificname'], rank=full_taxonomy['rank'],
                                                       parent=parent,
                                                       compiled=compiled_taxonomy,
                                                       taxonomy_linked=self.taxonomy,
                                                       children_list=[],
                                                       taxon_id=full_taxonomy['AphiaID'])
        if full_taxonomy["child"] is not None:
            compiled_taxonomy += '|'
            parent = taxon
            return self.get_taxons_from_nested_classification(full_taxonomy['child'], compiled_taxonomy, parent)
        else:
            dict_taxon = {'taxon': taxon, "created": created}
            return dict_taxon

    def process_taxon(self, aphia_id):
        try:
            taxon = Taxon.objects.get(taxon_id__iexact=aphia_id, taxonomy_linked=self.taxonomy)
            current_taxon = taxon
        except Taxon.DoesNotExist:
            self.VERBOSE_LOGGER.debug(f"*** Processing taxon description {aphia_id}")
            response = requests.get(f'https://www.marinespecies.org/rest/AphiaClassificationByAphiaID/{aphia_id}')
            full_taxonomy = response.json()
            parent = None
            compiled_taxonomy = ""
            dict_taxon = self.get_taxons_from_nested_classification(full_taxonomy, compiled_taxonomy, parent)
            self.VERBOSE_LOGGER.debug(f"*** Done Processing taxon description {aphia_id}")
            current_taxon = dict_taxon['taxon']
        return current_taxon

    def process_morphologic_observation(self, descriptor, observator, taxon):
        morphologic_observation = MorphObs.objects.create(observator=observator,
                                                          descriptor=descriptor,
                                                          taxon=taxon)
        return morphologic_observation

    def process_occurrences(self, row, taxon, morphologic_observation, event):
        self.VERBOSE_LOGGER.debug(f"*** Processing Occurrences")
        fraction_size = self.config["FRACTION_SIZE"]
        occurrence_id = event.event_id + "_" + taxon.name
        (occurrence, created) = Occurrence.objects.get_or_create(occurrence_id=occurrence_id, from_taxon=taxon,
                                                                 event=event, from_dataset=self.dataset)

        if int(row[self.config["COUNT_CELL_INDEX"]]) > 0:
            Measure.objects.create(from_occurrence=occurrence,
                                   name="count",
                                   type="miscropic",
                                   methode="observation",
                                   value=row[self.config["COUNT_CELL_INDEX"]],
                                   link_morpho=morphologic_observation,
                                   fraction_size=fraction_size)
        self.VERBOSE_LOGGER.debug(f"*** Done Processing Occurrences{occurrence, morphologic_observation}")

    def process_cumulative_measurements(self):
        self.LOGGER.debug(f"*** Processing Cumulative Measurements")
        for event_info in self.events.values():
            taxon_dict = {}
            total_count = 0
            event = event_info['event']
            fraction_size = event_info['parameter']
            measures = Measure.objects.filter(fraction_size=fraction_size).select_related("from_occurrence").filter(
                from_occurrence__event=event).filter(
                from_occurrence__from_dataset__description=self.dataset.description)

            for measure in measures:
                total_count += measure.value
                taxon = measure.from_occurrence.from_taxon
                if taxon not in taxon_dict:
                    taxon_dict[taxon] = {'count_total': 0.0, 'relative_abundance_total': 0.0,
                                         "occurrence": measure.from_occurrence}
                taxon_dict[taxon]['count_total'] += measure.value

            for measure in measures:
                measure_relative_abundance = Measure.objects.create(from_occurrence=measure.from_occurrence,
                                                                    name="relative_abundance",
                                                                    type="read",
                                                                    methode="metabarcode",
                                                                    value=(measure.value * 100) / total_count,
                                                                    link_morpho=measure.link_morpho,
                                                                    fraction_size=fraction_size)
                taxon = measure_relative_abundance.from_occurrence.from_taxon
                taxon_dict[taxon]['relative_abundance_total'] += measure_relative_abundance.value

            for taxon, value in taxon_dict.items():
                CumulatedMeasurement.objects.create(from_occurrence=value["occurrence"],
                                                    name="cumulative_count",
                                                    value=value['count_total'],
                                                    fraction_size_cumulated=fraction_size)
                CumulatedMeasurement.objects.create(from_occurrence=value["occurrence"],
                                                    name="cumulative_relative_abundance",
                                                    value=value['relative_abundance_total'],
                                                    fraction_size_cumulated=fraction_size)

        self.LOGGER.debug(f"*** Done Processing Cumulative Measurements")

    def process_taxon_children_list_creation(self):
        self.LOGGER.debug(f"*** Processing Children List Creation for each Taxon in the Taxonomy ")
        taxon_list = Taxon.objects.filter(taxonomy_linked=self.taxonomy)
        for taxon in taxon_list:
            # Only process taxons that have not yet been processed by previous recursive calls to fill_taxon_children()
            if taxon.children_list is None or len(taxon.children_list) == 0:
                children_list = []
                index = 0
                children_list = fill_taxon_children_list(taxon, children_list, index)
                self.LOGGER.debug(children_list)
                taxon.children_list = children_list
            taxon.save()
        self.LOGGER.debug(f"*** Done Processing Children List Creation")

    def load_data(self, biodiversity_file):

        biodiversity_table = csv.reader(biodiversity_file, delimiter=self.config["DELIMITER"], quotechar='"')
        next(biodiversity_table)
        row_index = 2
        self.events = {}
        for row in biodiversity_table:
            self.VERBOSE_LOGGER.debug(f"Processing row {row_index}")
            if row_index % 100 == 0:
                self.LOGGER.debug(f"Processing row {row_index}")
            row_index = row_index + 1
            aphia_id = row[self.config["APHIAID_INDEX"]]
            event = self.process_events(row)
            taxon = self.process_taxon(aphia_id)
            morphologic_observation = self.process_morphologic_observation(
                descriptor=row[self.config["DESCRIPTOR_INDEX"]],
                observator=taxon.name,
                taxon=taxon,
            )
            self.process_occurrences(row, taxon, morphologic_observation, event)

            self.VERBOSE_LOGGER.debug(f"Done Processing row {row_index}")
            if row_index % 100 == 0:
                self.LOGGER.debug(f"Done Processing row {row_index}")
        #
        # # The actual measurements and occurrences can only be created once all rows have been processed :
        # # they store the relative abundances for each occurrence/event/parameter and not the raw values
        self.process_cumulative_measurements()
        if self.config['CHUNK'] == "no":
            self.process_taxon_children_list_creation()
