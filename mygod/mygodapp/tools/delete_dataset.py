import logging
from mygodapp.models import Event, DataSet, Taxonomy


class RemoveDatasetException(Exception):
    pass


class RemoveDataset:
    LOGGER = logging.getLogger(__name__)

    def __init__(self):
        super().__init__()

    def remove_metabar_data(self, dataset_name):
        self.LOGGER.info(f"Dataset selected {dataset_name}")
        events = Event.objects.filter(bind_dataset__name__iexact=dataset_name)
        self.LOGGER.info(f"Removing associated Taxonomy instances, taxons and sequences")
        Taxonomy.objects.get(dataset__name__iexact=dataset_name).delete()
        self.LOGGER.info(f"Removing associated Event instances")
        events.delete()
        self.LOGGER.info(f"Removing associated Dataset, Occurrence, Measure and CumulatedMeasurement instances")

