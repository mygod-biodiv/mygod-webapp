import datetime
import json
import logging
from django.contrib.auth.models import Group, User
from guardian.shortcuts import assign_perm
from mygodapp.models import Taxonomy, DataSet
from mygodapp.tools.delete_dataset import RemoveDataset


def fill_taxon_children_list(taxon, children_list, index):
    if not any(rank['rank_name'] == taxon.rank for rank in children_list):
        children_list.insert(index, {'rank_name': taxon.rank, "taxon_id": [taxon.pk]})
    else:
        index = 0
        for rank in children_list:
            index += 1
            if rank['rank_name'] == taxon.rank:
                rank['taxon_id'].append(taxon.pk)
    if taxon.children:
        for child in taxon.children.all():
            index += 1
            fill_taxon_children_list(child, children_list, index)
        return children_list
    else:
        return children_list


class MetadataException(Exception):
    pass


class DatasetImportException(Exception):
    pass


class MultiLogger:
    def __init__(self, name, file_logger=None):
        self.default_logger = logging.getLogger(name)
        self.file_logger = file_logger

    def set_file_logger(self, file_logger):
        self.file_logger = file_logger

    def log(self, level, message, json_message=None):
        self.default_logger.log(level, message)
        if self.file_logger:
            if json_message:
                json_decoded_message = json.loads(json_message)
                json_decoded_message['timestamp'] = datetime.datetime.now().strftime("[%Y-%m-%d %H:%M:%S]")
                json_reencoded_message = json.dumps(json_decoded_message)
                self.file_logger.log(level, f"###JSON:{json_reencoded_message}")
            else:
                self.file_logger.log(level, message)


class DataWithConfigLoader:
    VERBOSE_LOGGER = logging.getLogger('verbose.' + __name__)
    MANDATORY_FIELDS = ["TAXONOMY_INDEX", "EVENT_START_INDEX", "TAXONOMY_NAME", "SAMPLE_ID_INDEX", "STATION_CODE_INDEX",
                        "DEPTH_CODE_INDEX", "DEPTH_MIN_INDEX", "DEPTH_MAX_INDEX", "FRACTION_SIZE_INDEX",
                        "DATE_TIME_INDEX", "LATITUDE_INDEX", "LONGITUDE_INDEX", "EVENT_ID_REGEX", "DATE_FORMAT",
                        "COUNT_DELIMITER", "EVENT_DELIMITER"]

    def __init__(self):
        self.config = None
        self.multilogger = MultiLogger(__name__)

    def set_logger_file(self, file_logger):
        self.multilogger.set_file_logger(file_logger)

    def _check_config(self, config):
        missing_fields = []
        for field in DataWithConfigLoader.MANDATORY_FIELDS:
            if field not in config:
                missing_fields.append(field)
        if len(missing_fields) > 0:
            all_missing_fields = ' '.join(missing_fields)
            self.multilogger.log(logging.ERROR, f"missing fields {missing_fields}",
                                 "{ \"level\" : \"error\",\"type\" : \"event\", "
                                 "\"message\" : \"Missing fields in configuration file : " + all_missing_fields + "\"}")
            raise MetadataException()

    def load_config(self, config_file):
        self.config = json.load(config_file)
        self._check_config(self.config)


class MetaDataLoader:
    MANDATORY_FIELDS = ['name', "project", "description", 'campaign', 'taxonomy', "owners", "marker", "site",
                        "pc_parameter", "rank_description", "datatype", "related_publications"]

    def __init__(self):
        self.dataset = None
        self.taxonomy = None
        self.multilogger = MultiLogger(__name__)

    def _check_metadata(self, metadata):
        missing_fields = []
        for field in MetaDataLoader.MANDATORY_FIELDS:
            if field not in metadata:
                missing_fields.append(field)
        if len(missing_fields) > 0:
            self.multilogger.log(logging.ERROR, f"missing fields {missing_fields}",
                                 "{ \"level\" : \"error\",\"type\" : \"metadata\", \"message\" : "
                                 "\"Metadata files missing fields " + str(missing_fields) + "\"}")
            raise MetadataException()

    def set_logger_file(self, file_logger):
        self.multilogger.set_file_logger(file_logger)

    def get_dataset(self):
        return self.dataset

    def get_taxonomy(self):
        return self.taxonomy

    def process_taxonomy_creation(self, metadata):
        self.multilogger.log(logging.DEBUG, f"Processing metadata.",
                             "{ \"level\" : \"info\",\"type\" : \"metadata\", \"message\" : "
                             "\"Processing metadata \"}")
        self.taxonomy = Taxonomy.objects.create(name=metadata["taxonomy"],
                                                version=metadata["version_taxonomy"],
                                                rank_description=metadata['rank_description'])

    def process_dataset_creation(self, metadata):
        self.multilogger.log(logging.WARN, f"Unexpected called to MetadataLoader.process_dataset_creation()",
                             "{ \"level\" : \"error\",\"type\" : \"metadata\", \"message\" : "
                             "\"Unexpected called to MetadataLoader.process_dataset_creation() \"}")
        raise MetadataException

    def process_group_creation(self, metadata):

        try:
            self.multilogger.log(logging.DEBUG, f"Trying to retrieve group {self.dataset.description}")
            group = Group.objects.get(name=self.dataset.description)
            self.multilogger.log(logging.DEBUG, f"Group {self.dataset.description} exists.")
        except Group.DoesNotExist:
            self.multilogger.log(logging.DEBUG, f"Group {self.dataset.description} does not exist. Creating")
            group = Group.objects.create(name=self.dataset.description)
        assign_perm('view_dataset_init', group, self.dataset)

        try:
            self.multilogger.log(logging.DEBUG, f"Trying to retrieve publisher group")
            publication_group = Group.objects.get(name="publisher")
            self.multilogger.log(logging.DEBUG, f"Publisher Group exists.")
        except Group.DoesNotExist:
            self.multilogger.log(logging.DEBUG, f"Publisher Group does not exist. Creating")
            publication_group = Group.objects.create(name="publisher")
        self.multilogger.log(logging.DEBUG, f"Processing dataset ownership")
        for owner in metadata['owners']:
            username = (owner['first_name'] + owner['last_name']).lower()
            try:
                self.multilogger.log(logging.DEBUG,
                                     f"Trying to retrieve user {username} ({owner['first_name']} {owner['last_name']}).")
                user = User.objects.get(username=username, email=owner['contact_email'])
                self.multilogger.log(logging.DEBUG,
                                     f"User {username} ({owner['first_name']} {owner['last_name']} exists).")
            except User.DoesNotExist:
                self.multilogger.log(logging.DEBUG,
                                     f"User {username} ({owner['first_name']} {owner['last_name']}) does not exist. Creating")
                user = User(username=username, first_name=owner['first_name'], last_name=owner['last_name'],
                            password="temp" + owner['first_name'] + owner['last_name'],
                            email=owner['contact_email'])
                user.save()

            user.groups.add(group)
            user.groups.add(publication_group)
        self.multilogger.log(logging.DEBUG, f"Done processing metadata.",
                             "{ \"level\" : \"info\",\"type\" : \"metadata\", \"message\" : "
                             "\"Done processing metadata \"}")

    def load(self, file):
        metadata = json.load(file)
        try:
            self._check_metadata(metadata)
        except MetadataException:
            self.multilogger.log(logging.ERROR, "Import aborted",
                                 json.dumps({'level': 'error',
                                             'type' : 'metadata',
                                             'message' : f"Import of dataset {self.dataset.name} aborted."}))
            raise DatasetImportException()
        self.process_taxonomy_creation(metadata)
        self.process_dataset_creation(metadata)
        self.process_group_creation(metadata)
