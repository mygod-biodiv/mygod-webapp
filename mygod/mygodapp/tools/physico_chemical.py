import csv
import json
import logging
from datetime import datetime

from mygodapp.models import Event, PcReleve, DataSet
from mygodapp.tools.commons import DataWithConfigLoader, DatasetImportException, MetadataException
from mygodapp.tools.delete_dataset import RemoveDataset


class PhysicoChemicalCsvLoader(DataWithConfigLoader):
    ALL_PARAMETER_LIST = ["DEPTH_CODE_INDEX", "DEPTH_INDEX",
                          "NH4_INDEX", "NH4_UNIT", "NO2_INDEX",
                          "NO2_UNIT", "NO3_INDEX", "NO3_UNIT", "OXYGEN_INDEX",
                          "OXYGEN_UNIT", "PH_INDEX", "PO4_INDEX", "PO4_UNIT",
                          "SALINITY_INDEX", "SALINITY_UNIT", "SITENAME_INDEX", "TEMPERATURE_INDEX",
                          "TEMPERATURE_UNIT", "TIDE_COEFF_INDEX", "TIME_INDEX", "CHLA_INDEX", "CHLA_UNIT"]

    MANDATORY_FIELDS = ALL_PARAMETER_LIST + ["CAMPAIGN", "DEFAULT_MISSING_VALUES", "DELIMITER"]

    def __init__(self):
        super().__init__()
        self.remove_command = RemoveDataset()
        self.csv_file = None

    def _check_config(self, config):
        missing_fields = []
        for field in PhysicoChemicalCsvLoader.MANDATORY_FIELDS:
            if field not in PhysicoChemicalCsvLoader.MANDATORY_FIELDS:
                missing_fields.append(field)
        if len(missing_fields) > 0:
            self.multilogger.log(logging.ERROR, f"missing fields {missing_fields}")
            raise MetadataException()

    def __process_sample(self, row, dry_run):

        dict_parameter = {}
        custom_field_dict = {}
        missing_value = self.config["DEFAULT_MISSING_VALUES"]
        for parameter in PhysicoChemicalCsvLoader.ALL_PARAMETER_LIST:
            index_value = None
            dict_parameter[parameter] = {}
            related_missing_value = parameter + "_MISSING"
            if related_missing_value in self.config:
                missing_value = self.config[related_missing_value]
            dict_parameter[parameter]['missing_value'] = str(missing_value)
            if parameter in self.config:
                index_value = self.config[parameter]
                if isinstance(index_value, int):
                    index_value = row[self.config[parameter]]
            dict_parameter[parameter]['index_value'] = index_value
        sampling_date = None
        sampling_date_format = self.config["DATE_FORMAT"]
        sampling_date_str = row[self.config["DATE_INDEX"]]
        try:
            sampling_date = datetime.strptime(sampling_date_str, sampling_date_format)
        except ValueError:
            self.multilogger.log(logging.ERROR,
                                 f"Date string ({sampling_date_str}) could not be parsed according to format {sampling_date_format}. Skipping sample.",
                                 "{ \"level\" : \"error\",\"type\" : \"physico_chemical_sampling\", \"message\" : \" Date string (" + sampling_date_str + ") could not be parsed according to format " + sampling_date_format + ". Skipping sample.\"}")
            return False
        for parameter, value in dict_parameter.items():
            if value['index_value'] == value['missing_value']:
                value['index_value'] = None
        if 'CUSTOM_FIELDS' in self.config:
            for field in self.config["CUSTOM_FIELDS"]:
                if 'missing_values' in field:
                    missing_values = field['missing_values']
                else:
                    missing_values = self.config["DEFAULT_MISSING_VALUES"]
                if row[field['index']] == missing_values:
                    pc_value = None
                else:
                    try:
                        pc_value = row[field['index']]
                        float(pc_value)
                    except ValueError:
                        self.multilogger.log(logging.ERROR,
                                             f"Value of {field} for {sampling_date_str}) is not a numerical value.")
                        raise DatasetImportException

                custom_field_dict[field['name']] = {'value': pc_value, 'unit': field['unit']}
        try:
            DataSet.objects.filter(campaign__iexact=self.config["CAMPAIGN"])
        except DataSet.DoesNotExist:
            self.multilogger.log(logging.DEBUG, f"No related Metabar or Morphology Found",
                                 "{ \"level\" : \"error\",\"type\" : \"physico_chemical_sampling\", \"message\" : "
                                 "\"No related Metabar or Morphological related dataset found\"}")
            raise DatasetImportException

        events = list(Event.objects.filter(date=sampling_date,
                                           bind_dataset__campaign__iexact=self.config["CAMPAIGN"],
                                           depth_code__iexact=row[self.config["DEPTH_CODE_INDEX"]],
                                           station_code__iexact=row[self.config["STATION_CODE_INDEX"]]).distinct())

        if len(events) == 0:
            self.multilogger.log(logging.WARNING,
                                 f"No metabarcode or morphology sampling event found on date {sampling_date} / campaign {self.config['CAMPAIGN']} / depth code {row[self.config['DEPTH_CODE_INDEX']]}. Ignoring entry.",
                                 json.dumps({'level' : 'warning', 'type' : 'physico_chemical_sampling',
                                             'message' : 'No metabarcode or morphology sampling event found on date ' + sampling_date.strftime("%Y-%m-%d %H:%M:%S")+ ' / campaign '+self.config['CAMPAIGN'] +' / depth code '+row[self.config['DEPTH_CODE_INDEX']]+'. Ignoring entry.'}))
            return False

        if len(events) == 1:
            event = events[0]
            if not dry_run:
                (pcreleve, created) = PcReleve.objects.get_or_create(site=row[self.config["SITENAME_INDEX"]],
                                                                     date=sampling_date,
                                                                     profondeur=dict_parameter["DEPTH_CODE_INDEX"][
                                                                         'index_value'],
                                                                     coef_maree=dict_parameter["TIDE_COEFF_INDEX"][
                                                                         'index_value'],
                                                                     temperature=dict_parameter["TEMPERATURE_INDEX"][
                                                                         'index_value'],
                                                                     o=dict_parameter["OXYGEN_INDEX"]['index_value'],
                                                                     o_unit=dict_parameter["OXYGEN_UNIT"][
                                                                         'index_value'],
                                                                     temperature_unit=
                                                                     dict_parameter["TEMPERATURE_UNIT"]['index_value'],
                                                                     s=dict_parameter["SALINITY_INDEX"]['index_value'],
                                                                     s_unit=dict_parameter["SALINITY_UNIT"][
                                                                         'index_value'],
                                                                     ph=dict_parameter["PH_INDEX"]['index_value'],
                                                                     nh4=dict_parameter["NH4_INDEX"]['index_value'],
                                                                     nh4_unit=dict_parameter["NH4_UNIT"]['index_value'],
                                                                     no2=dict_parameter["NO2_INDEX"]['index_value'],
                                                                     no2_unit=dict_parameter["NO2_UNIT"]['index_value'],
                                                                     no3=dict_parameter["NO3_INDEX"]['index_value'],
                                                                     no3_unit=dict_parameter["NO3_UNIT"]['index_value'],
                                                                     po4=dict_parameter["PO4_INDEX"]['index_value'],
                                                                     po4_unit=dict_parameter["PO4_UNIT"]['index_value'],
                                                                     chla=dict_parameter["CHLA_INDEX"]['index_value'],
                                                                     chla_unit=dict_parameter["CHLA_UNIT"][
                                                                         'index_value'],
                                                                     event=event,
                                                                     custom_parameters=custom_field_dict)

            self.multilogger.log(logging.DEBUG, f"Saved physico-chemical sampling event {event.date}",
                                 json.dumps({'level' : 'debug', 'type': 'physico_chemical_sampling',
                                            'message' : 'Saved physico-chemical sampling event at date '+sampling_date.strftime("%Y-%m-%d %H:%M:%S")+
                                            ' and depth: '+dict_parameter['DEPTH_CODE_INDEX']['index_value']
                                             }))
        else:
            self.multilogger.log(logging.DEBUG,
                                 f"Multiple sampling events : {events} found on date {sampling_date} / campaign {self.config['CAMPAIGN']} / depth code {row[self.config['DEPTH_CODE_INDEX']]}. Ignoring entry.", )
            return False
        return True

    def load_data(self, csv_file, dry_run=False):
        self.multilogger.log(logging.INFO,"Starting import of physico-chemical data.",
                             json.dumps({"level": 'info', 'type' : 'physico_chemical_sampling',
                                         'message' : 'Starting import of physico-chemical data.'}))
        reader = csv.reader(csv_file, delimiter=self.config["DELIMITER"], quotechar='"')
        self._check_config(self.config)
        header_lines = self.config["HEADER_LINES"]
        for i in range(header_lines):
            next(reader)
        lines_ok = 0
        lines_skipped = 0
        for row in reader:
            status = self.__process_sample(row, dry_run)
            if status:
                lines_ok += 1
            else:
                lines_skipped += 1
        if lines_ok > 0:
            lines_ok_ratio = 100.0 * float(lines_ok) / float(lines_ok + lines_skipped)
            lines_skipped_ratio = 100.0 * float(lines_skipped) / float(lines_ok + lines_skipped)
        else:
            lines_ok_ratio = 0.0
            lines_skipped_ratio = 100.0
        self.multilogger.log(logging.INFO, f"Number of lines correctly processed {lines_ok} ({lines_ok_ratio:2.2f} %)",
                             json.dumps({'level': 'info', 'type' : 'physico_chemical_sampling',
                                         'message' : f"Number of lines correctly processed {lines_ok} ({lines_ok_ratio:2.2f} %)"}))
        self.multilogger.log(logging.INFO, f"Number of lines skipped {lines_skipped} ({lines_skipped_ratio:2.2f} %)",
                             json.dumps({'level': 'info', 'type' : 'physico_chemical_sampling',
                                         'message' : f"Number of lines skipped {lines_skipped} ({lines_skipped_ratio:2.2f} %)"}))
        if dry_run:
            self.multilogger.log(logging.INFO,
                                 f"Dry run is successful",
                                 "{ \"level\" : \"info\",\"type\" : \"physico_chemical_sampling\", "
                                 "\"message\" : \"Dry run is successful.\"}")
