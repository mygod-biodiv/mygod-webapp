import csv
import logging
import re
from datetime import datetime
from django.contrib.auth.models import Group, User
from guardian.shortcuts import assign_perm
from django.conf import settings
import logging
import pandas as pd
import json
from rpy2.robjects import pandas2ri
from rpy2.robjects.conversion import localconverter
import rpy2
import rpy2.robjects as ro
import os
import platform
from rpy2.robjects.packages import STAP
import rpy2.robjects.packages as rpackages
from mygodapp.models import Event, Taxon, Occurrence, Measure, Sequence, CumulatedMeasurement, MetabarDataSet
from mygodapp.tools.commons import DataWithConfigLoader, MetaDataLoader, fill_taxon_children_list, MetadataException, \
    DatasetImportException
from mygodapp.tools.delete_dataset import RemoveDataset

if platform.system() == 'Windows':
    def _cchar_to_str(c, encoding: str = 'cp1252') -> str:
        # TODO: use isStrinb and installTrChar
        s = rpy2.rinterface_lib.openrlib.ffi.string(c).decode(encoding)
        return s


    def _cchar_to_str_with_maxlen(c, maxlen: int, encoding: str = 'cp1252') -> str:
        # TODO: use isStrinb and installTrChar
        s = rpy2.rinterface_lib.openrlib.ffi.string(c, maxlen).decode('cp1252')
        return s


    rpy2.rinterface_lib.conversion._cchar_to_str = _cchar_to_str
    rpy2.rinterface_lib.conversion._cchar_to_str_with_maxlen = _cchar_to_str_with_maxlen

R_LIBS = None
R_LIB_NAMES = ('rtk', 'data.table')
R_RAREFACTION = None


def _init_r_libs():
    """
        Initialize R components.

        Actual initialization occurs only on first call to method.
    """
    global R_LIBS, R_RAREFACTION
    if R_LIBS is None:
        R_LIBS = list()
        for r_lib_name in R_LIB_NAMES:
            R_LIBS.append(rpackages.importr(r_lib_name))
            script_file = open(os.path.join(settings.R_SCRIPT_DIR, 'rarefaction.r'), encoding="utf8")
            rarefaction_script = ''.join(script_file.readlines())
            R_RAREFACTION = STAP(rarefaction_script, "rarefaction")


class MetabarMetadataLoader(MetaDataLoader):

    def __int__(self):
        super().__init__()

    def process_dataset_creation(self, metadata):
        try:
            dataset = MetabarDataSet.objects.get(name=metadata['name'])
            self.multilogger.log(logging.ERROR, f"Dataset already exists {dataset.name}",
                                 json.dumps({'level' : 'error', 'type' : 'dataset',
                                             'message' : f"Dataset already exists {dataset.name}"}))
            raise MetadataException
        except MetabarDataSet.DoesNotExist:

            self.multilogger.log(logging.DEBUG, f"Creating Dataset object.",
                                 "{ \"level\" : \"info\",\"type\" : \"dataset\", \"message\" : "
                                 "\"Creating Dataset object\"}")
            if 'base_layers' not in metadata :
                metadata['base_layers']=[]
            self.dataset = MetabarDataSet.objects.create(name=metadata["name"],
                                                         description=metadata["description"],
                                                         marker=metadata["marker"],
                                                         fraction=metadata["fraction"],
                                                         taxonomy=self.taxonomy,
                                                         campaign=metadata['campaign'],
                                                         site=metadata['site'],
                                                         datatype="metabarcode",
                                                         pc_parameter_list=metadata["pc_parameter"],
                                                         type=metadata["datatype"],
                                                         related_publications=metadata[
                                                             'related_publications'],
                                                         base_layers=metadata['base_layers'])

class MetabarcodingDataLoader(DataWithConfigLoader):
    MEASUREMENT_METHOD = 'metabarcode'
    MEASUREMENT_TYPE = 'read'

    def __init__(self, dataset, taxonomy):
        super().__init__()
        self.dataset = dataset
        self.taxonomy = taxonomy
        self.events = None
        self.remove_command = RemoveDataset()
        self.event_headers = None

    def remove_data(self, dataset_name):
        self.remove_command.remove_metabar_data(dataset_name)
        self.event_headers = None

    def rarefy(self, sample_file):
        _init_r_libs()
        sample_dataframe_init = pd.read_csv(sample_file, sep=self.config["COUNT_DELIMITER"])
        try:
            sample_dataframe = sample_dataframe_init.drop(columns=['taxonomy_score', 'taxonomy', 'sequence'])
        except KeyError as e :
            missing_key = re.sub(r"^.*(\[.*\]).*$", r"\1", e.args[0])
            self.multilogger.log(logging.DEBUG, "Error during data normalisation.",
                                 json.dumps({'level' : 'error', 'type' : 'sample',
                                             'message' : f"Error during data normalisation, missing header(s) in sample file: {missing_key}"}))
            self.remove_data(self.dataset.name)
            raise DatasetImportException()

        sample_dataframe_index = sample_dataframe.set_index('amplicon')
        sample_dataframe_index.index.names = [None]
        self.multilogger.log(logging.DEBUG, "Processing rarefaction.",
                             "{ \"level\" : \"info\",\"type\" : \"event\", \"message\" : "
                             "\"Processing rarefaction\"}")
        with localconverter(ro.default_converter + pandas2ri.converter):
            r_dataframe_data = ro.conversion.py2rpy(sample_dataframe_index)
        asv_rarefied_dataframe = R_RAREFACTION.rarefaction(r_dataframe_data)
        with localconverter(ro.default_converter + pandas2ri.converter):
            panda_asv_rarefied = ro.conversion.rpy2py(asv_rarefied_dataframe)
        rarefied_asv = panda_asv_rarefied.reset_index()
        self.multilogger.log(logging.DEBUG, "Done processing rarefaction.",
                             "{ \"level\" : \"info\",\"type\" : \"event\", \"message\" : "
                             "\"Done processing rarefaction\"}")

        return rarefied_asv

    def process_events(self, event_file):
        self.events = {}
        self.multilogger.log(logging.INFO, "Processing event table.",
                             "{ \"level\" : \"info\",\"type\" : \"event\", \"message\" : "
                             "\"Processing event table\"}")

        cluster = None
        # The following regexp captures the first parts of even_id with fraction_size.
        event_id_pattern = re.compile(self.config["EVENT_ID_REGEX"])
        self.dataset.first_sample_date = None
        self.dataset.last_sample_date = None
        row = 0
        error_rows = []
        if "CLUSTER_INDEX" in self.config:
            cluster = True
        for event in event_file:
            row = row + 1
            if cluster:
                cluster_parameter = event[self.config["CLUSTER_INDEX"]]
            else:
                cluster_parameter = "No cluster"
            date_string = event[self.config["DATE_TIME_INDEX"]]

            date_format = self.config["DATE_FORMAT"]
            try:
                datetime_object = datetime.strptime(date_string, date_format)
                date_object = datetime_object.date()
                if (self.dataset.last_sample_date is None) or (date_object > self.dataset.last_sample_date):
                    self.dataset.last_sample_date = date_object
                if (self.dataset.first_sample_date is None) or (date_object < self.dataset.first_sample_date):
                    self.dataset.first_sample_date = date_object
            except ValueError:
                message = f"Row {row}: date field ({date_string}) could not be parsed according to format {date_format}."
                self.multilogger.log(logging.ERROR,message)
                error_rows.append(message)
                continue

            event_id = None
            event_id_string = event[self.config["SAMPLE_ID_INDEX"]]
            m = re.match(event_id_pattern, event_id_string)
            if m is not None:
                event_id = m.group('event_id')
            else:
                message = f"Row {row}: event_id ({event_id}) could not be parsed according to pattern {event_id_pattern}."
                self.multilogger.log(logging.ERROR,message)
                error_rows.append(message)
                continue
            parameter = event[self.config["FRACTION_SIZE_INDEX"]]
            depth = event[self.config["DEPTH_CODE_INDEX"]]

            (event, created) = Event.objects.get_or_create(event_id=event_id,
                                                           date=date_object,
                                                           type="metabar",
                                                           station_code=event[self.config["STATION_CODE_INDEX"]],
                                                           depth_code=depth,
                                                           min_depth=event[self.config["DEPTH_MIN_INDEX"]],
                                                           max_depth=event[self.config["DEPTH_MAX_INDEX"]],
                                                           latitude=event[self.config["LATITUDE_INDEX"]],
                                                           longitude=event[self.config["LONGITUDE_INDEX"]],
                                                           cluster=cluster_parameter
                                                           )
            event.bind_dataset.add(self.dataset)
            event.save()
            self.multilogger.log(logging.DEBUG, f"Saved event {event.date}",
                                 "{ \"level\" : \"debug\",\"type\" : \"event\", \"message\" : \"Date : " +
                                 event.date.strftime("%Y-%m-%d %H:%M:%S") + " Depth : " + depth + "\"}")
            self.events[event_id_string] = {'event': event, 'parameter': parameter}

        self.multilogger.log(logging.INFO,
                             f"Total number of rows with errors in event table: {len(error_rows)}, out of {row} rows {len(error_rows) * 100.0 / float(row)}%.",
                             "{ \"level\" : \"info\",\"type\" : \"event\", \"message\" : "
                             "\"Total number of rows with errors in event table: " + str(
                                 len(error_rows)) + ", out of " + str(row) + " rows " + str(
                                 len(error_rows) * 100.0 / float(row)) + "%.\"}")
        if len(self.events) == 0:
            self.multilogger.log(logging.WARNING, "Dataset has no valid events. No data will be associated.",
                                 "{ \"level\" : \"error\",\"type\" : \"event\", \"message\" : "
                                 "\"Dataset has no valid events. No data will be associated.\"}")

        if self.dataset.first_sample_date is None:
            self.multilogger.log(logging.WARNING, "Dataset has no valid first sample date. Assigning 1901-01-01")
            self.dataset.first_sample_date = datetime(1901, 1, 1).date()
        if self.dataset.last_sample_date is None:
            self.multilogger.log(logging.WARNING, "Dataset has no valid last sample date. Assigning 2999-12-31")
            self.dataset.last_sample_date = datetime(2999, 12, 31).date()

        self.dataset.save()
        for error_row in error_rows:
            self.multilogger.log(logging.DEBUG, error_row)
        self.multilogger.log(logging.INFO, "Done processing event table.",
                             "{ \"level\" : \"info\",\"type\" : \"event\", \"message\" : "
                             "\"Done processing event table\"}")

    def process_taxon(self, taxon_description):
        compiled_taxonomy = ""
        try:
            taxon_components = taxon_description.split("|")
        except IndexError:
            self.multilogger.log(logging.ERROR, "Error during full taxonomy handling",
                                 json.dumps({'level' : 'error',
                                             'type' : 'taxon',
                                             'message' : f'Error during taxon object creation: compiled taxonomy contains no pipe (|) symbols: {taxon_description}.'}))
            self.remove_data(self.dataset.name)
            raise DatasetImportException()

        # Start by extracting or creating top level taxon
        parent = None
        for index in range(0, len(taxon_components)):
            name = taxon_components[index]
            compiled_taxonomy += name
            rank = self.taxonomy.rank_description[index]
            (taxon, created) = Taxon.objects.get_or_create(name=name, rank=rank, parent=parent,
                                                           compiled=compiled_taxonomy,
                                                           taxonomy_linked=self.taxonomy, taxon_id=name + rank)
            compiled_taxonomy += "|"
            parent = taxon

        # At this stage, the parent is the actual taxon of the lowest rank matching
        # the latest component of the taxon_components so we can use it as a proxy
        # for the actual database taxon matching the description given as argument.
        taxon = parent
        return taxon

    def process_sequence(self, amplicon, sequence, identity, taxon):
        identity_num = 0.0
        try:
            identity_num = float(identity)
        except ValueError:
            identity_num = 0.0

        sequence = Sequence.objects.create(amplicon=amplicon,
                                           sequence=sequence,
                                           score=identity_num,
                                           taxon=taxon)
        return sequence

    def process_occurrences(self, row, taxon, sequence, row_rarefied):
        for index in range(self.config["EVENT_START_INDEX"], len(row)):
            value = row[index]
            # Start by checking if there is a non-null value in the cell
            # If not, skip further processing for this cell.
            asv_value = 0.0
            try:
                asv_value = float(value)
            except ValueError:
                asv_value = 0.0
            if asv_value <= 0.0:
                continue
            event_id_header = self.event_headers[index]
            # As R rarefaction function tend to replace "-" by "." in sampling id there
            # is a missmatch between id contains in header of file and id returned by R leading to a key error
            r_formatted_event_id_header = event_id_header.replace("-", ".")
            rarefied_value = row_rarefied[r_formatted_event_id_header][0]
            try:
                event_info = self.events[event_id_header]
            except KeyError:
                self.multilogger.log(logging.WARNING,
                                     f"No event matching {event_id_header} found in database. Skipping column.",
                                     "{ \"level\" : \"error\",\"type\" : \"taxon\", \"message\" : "
                                     "\"No event matching " + event_id_header + " found in database. Skipping column \"}")
                continue
            event = event_info['event']
            fraction_size = event_info['parameter']
            occurrence_id = event.event_id + "_" + taxon.name
            (occurrence, created) = Occurrence.objects.get_or_create(occurrence_id=occurrence_id, from_taxon=taxon,
                                                                     event=event, from_dataset=self.dataset)
            Measure.objects.create(from_occurrence=occurrence,
                                   name="asv",
                                   type="read",
                                   methode="metabarcode",
                                   value=asv_value,
                                   link_sequence=sequence,
                                   fraction_size=fraction_size)

            Measure.objects.create(from_occurrence=occurrence,
                                   name="rarefied_asv",
                                   type="rarefied",
                                   methode="metabarcode",
                                   value=rarefied_value,
                                   link_sequence=sequence,
                                   fraction_size=fraction_size)

    def process_cumulative_measurements(self):
        self.multilogger.log(logging.INFO, f"*** Processing Cumulative Measurements",
                             "{ \"level\" : \"info\",\"type\" : \"measurements\", \"message\" : "
                             "\"Processing Cumulative Measurements\"}")
        total_events = len(list(self.events.values()))
        event_index = 0
        for event_info in self.events.values():
            event_index += 1
            if event_index % 10 == 0:
                fraction = 0.01 * int(10000.0 * event_index / (1.0 * total_events))
                self.multilogger.log(logging.INFO,
                                     f"Processing event {event_index} out of {total_events} ({fraction} %)",
                                     "{ \"level\" : \"info\",\"type\" : \"row\", \"message\" : "
                                     "\"Processing event " + str(event_index) + " out of " + str(
                                         total_events) + " (" + str(fraction) + " %)" + "\"}")

            taxon_dict = {}
            total_asv = 0
            total_rarefied_asv = 0
            event = event_info['event']
            fraction_size = event_info['parameter']
            measures = Measure.objects.filter(fraction_size=fraction_size).select_related("from_occurrence").filter(
                from_occurrence__event=event).filter(
                from_occurrence__from_dataset__description=self.dataset.description)

            for measure in measures:
                taxon = measure.from_occurrence.from_taxon
                if taxon not in taxon_dict:
                    taxon_dict[taxon] = {'asv_total': 0.0, 'relative_abundance_total': 0.0, 'rarefied_asv_total': 0.0,
                                         "occurrence": measure.from_occurrence}
                if measure.name == "asv":
                    total_asv += measure.value
                    taxon_dict[taxon]['asv_total'] += measure.value
                if measure.name == "rarefied_asv":
                    total_rarefied_asv += measure.value
                    taxon_dict[taxon]['rarefied_asv_total'] += measure.value
            for measure in measures:
                taxon = measure.from_occurrence.from_taxon
                name = None
                total_value = 0
                if measure.name == "asv":
                    name = "relative_abundance"
                    total_value = (measure.value * 100) / total_asv
                    taxon_dict[taxon]['relative_abundance_total'] += total_value
                if measure.name == "rarefied_asv":
                    name = "rarefied_relative_abundance"
                    total_value = (measure.value * 100) / total_rarefied_asv
                    taxon_dict[taxon]['rarefied_asv_total'] += total_value
                Measure.objects.create(from_occurrence=measure.from_occurrence,
                                       name=name,
                                       type="read",
                                       methode="metabarcode",
                                       value=total_value,
                                       link_sequence=measure.link_sequence,
                                       fraction_size=fraction_size)

            for taxon, value in taxon_dict.items():
                CumulatedMeasurement.objects.create(from_occurrence=value["occurrence"],
                                                    name="cumulative_asv",
                                                    value=value['asv_total'],
                                                    fraction_size_cumulated=fraction_size)

                CumulatedMeasurement.objects.create(from_occurrence=value["occurrence"],
                                                    name="cumulative_relative_abundance",
                                                    value=value['relative_abundance_total'],
                                                    fraction_size_cumulated=fraction_size)

                CumulatedMeasurement.objects.create(from_occurrence=value["occurrence"],
                                                    name="cumulative_rarefied_relative_abundance",
                                                    value=value['rarefied_asv_total'],
                                                    fraction_size_cumulated=fraction_size)
            if event_index % 10 == 0:
                self.multilogger.log(logging.INFO, f"Done processing event {event_index}",
                                     "{ \"level\" : \"info\",\"type\" : \"row\", \"message\" : "
                                     "\"Done processing event " + str(event_index) + "\"}")

        self.multilogger.log(logging.DEBUG, f"*** Done Processing Cumulative Measurements",
                             "{ \"level\" : \"info\",\"type\" : \"taxon\", \"message\" : "
                             "\"Done Processing Cumulative Measurements \"}")

    def process_taxon_children_list_creation(self):
        self.multilogger.log(logging.DEBUG, f"*** Processing Children List Creation for each Taxon in the Taxonomy ",
                             "{ \"level\" : \"info\",\"type\" : \"taxon_child\", \"message\" : "
                             "\"Processing Children List Creation for each Taxon in the Taxonomy \"}")
        taxon_list = Taxon.objects.filter(taxonomy_linked=self.taxonomy)
        for taxon in taxon_list:
            # Only process taxons that have not yet been processed by previous recursive calls to fill_taxon_children()
            if taxon.children_list is None or len(taxon.children_list) == 0:
                children_list = []
                index = 0
                children_list = fill_taxon_children_list(taxon, children_list, index)
                taxon.children_list = children_list
            taxon.save()
        self.multilogger.log(logging.DEBUG, f"*** Done Processing Children List Creation",
                             "{ \"level\" : \"info\",\"type\" : \"taxon_child\", \"message\" : "
                             "\"Done Processing Children List Creation \"}")

    def load_data(self, sample_file, event_file, rarefied_sample, dry_run=False):
        event_table = csv.reader(event_file, delimiter=self.config["EVENT_DELIMITER"], quotechar='"')
        next(event_table)
        self.process_events(event_table)
        self.multilogger.log(logging.INFO, "Processing sample table.",
                             "{ \"level\" : \"info\",\"type\" : \"event\", \"message\" : "
                             "\"Processing sample table\"}")
        total_samples = 0
        with open(sample_file, 'r') as sample_file_read:
            total_samples = sum(1 for line in sample_file_read)
        total_samples -= 1
        self.multilogger.log(logging.INFO, f"Total number of samples {total_samples}",
                             "{ \"level\" : \"info\",\"type\" : \"row\", \"message\" : "
                             "\"Total number of samples " + str(total_samples) + "\"}")
        with open(sample_file, 'r') as sample_file_read:
            reader = csv.reader(sample_file_read, delimiter=self.config["COUNT_DELIMITER"], quotechar='"')
            # Store the event identifiers of the header line to allow matching with event ids
            # when processing occurrences later on.
            self.event_headers = next(reader)
            row_index = 2
            for row in reader:
                if row_index % 100 == 0:
                    fraction = 0.01 * int(10000.0 * row_index / (1.0 * total_samples))
                    self.multilogger.log(logging.INFO,
                                         f"Processing sample {row_index} out of {total_samples} ({fraction} %)",
                                         "{ \"level\" : \"info\",\"type\" : \"row\", \"message\" : "
                                         "\"Processing sample " + str(row_index) + " out of " + str(
                                             total_samples) + " (" + str(fraction) + " %)" + "\"}")
                compiled_taxonomy = row[self.config["TAXONOMY_INDEX"]]
                taxon = self.process_taxon(compiled_taxonomy)
                sequence = self.process_sequence(amplicon=row[self.config["AMPLICON_INDEX"]],
                                                 sequence=row[self.config["SEQUENCE_INDEX"]],
                                                 identity=row[self.config["IDENTITY_INDEX"]],
                                                 taxon=taxon,
                                                 )

                row_asv_rarefied = rarefied_sample.loc[rarefied_sample['index'] == sequence.amplicon]
                rarefied_asv_dict = row_asv_rarefied.to_dict(orient='list')
                self.process_occurrences(row, taxon, sequence, rarefied_asv_dict)

                if row_index % 100 == 0:
                    self.multilogger.log(logging.INFO, f"Done Processing sample {row_index}",
                                         "{ \"level\" : \"info\",\"type\" : \"row\", \"message\" : "
                                         "\"Done processing sample " + str(row_index) + "\"}")

                row_index = row_index + 1

            # The actual measurements and occurrences can only be created once all rows have been processed :
            # they store the relative abundances for each occurrence/event/parameter and not the raw values
            if not dry_run:
                self.process_cumulative_measurements()
                self.process_taxon_children_list_creation()
                self.dataset.status = "complete"
                self.dataset.save()
                self.multilogger.log(logging.DEBUG, f"Status of dataset has been changed to {self.dataset.status}",
                                     "{ \"level\" : \"info\",\"type\" : \"row\", \"message\" : "
                                     "\"Status of dataset has been changed to " + self.dataset.status + "\"}")
            else:
                self.dataset.status = "dry_run"
                dataset_name = self.dataset.name
                self.remove_data(dataset_name)
                self.multilogger.log(logging.DEBUG,
                                     f"Dry run was successful and database has been cleaned of all imported data during this dry run",
                                     "{ \"level\" : \"info\",\"type\" : \"row\", \"message\" : "
                                     "\"Dry run was successful and database has been cleaned of all imported data during this dry run.\"}")


def isfloat(num):
    try:
        float(num)
        return True
    except ValueError:
        return False
