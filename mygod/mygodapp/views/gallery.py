import datetime
import logging
from django.http import HttpResponse
from django.shortcuts import render, redirect
from mygodapp.models import DashboardSettings, GalleryEntry
from mygodapp.forms import PublishDashForm, RejectionForm
from django.core.mail import send_mail, BadHeaderError
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.conf import settings


logger = logging.getLogger(__name__)


def edit_gallery_entry(request, status):
    """
    Allow modification of text and image field of gallery entry.
    Functionality is only accessible if entry is not promoted
    :param request: the HTTP request
    :param status: Name of modification to apply
    :type status: str
    :return: Render dahsboard page

    """

    gallery_entry = None
    if request.method == "POST":
        uuid = request.POST.get('uuid')
        if status == "edit":
            gallery_entry = GalleryEntry.objects.get(bind_dashboard__uuid__iexact=uuid)
            form = PublishDashForm(request.POST, request.FILES, instance=gallery_entry)
            if form.is_valid():
                gallery_entry.last_saved = datetime.datetime.utcnow()
                form.save()
                return redirect('mygodapp:user_page')
        if status == "promote":
            form = PublishDashForm(request.POST, request.FILES)
            dashboard = DashboardSettings.objects.get(uuid__iexact=uuid)
            if form.is_valid():
                gallery_entry = form.save()
                gallery_entry.bind_dashboard = dashboard
                gallery_entry.last_saved = datetime.datetime.utcnow()
                dashboard.promoted = True
                # dashboard.public = True
                dashboard.save()
                gallery_entry.save()
                return redirect('mygodapp:user_page')
    else:
        if request.method == "GET":
            uuid = request.GET.get("uuid", "")
            if status == "edit":
                gallery_entry = GalleryEntry.objects.get(bind_dashboard__uuid__iexact=uuid)
                form = PublishDashForm(instance=gallery_entry, initial={'uuid': uuid})
            if status == "promote":
                form = PublishDashForm(initial={'uuid': uuid, "summary": "Short Summary Here (288 Characters)",
                                                "text": "<b>Long</b> explanation about gallery entry"})
        else:
            form = PublishDashForm()
    return render(request, 'dashboard_edit.html',
                  context={'form': form, "entry": gallery_entry, "entry_status": status})


def change_gallery_entry_status(request, status, uuid):
    """
    Change status of a gallery entry.
    If status is public publisher status is checked.
    If not then request with email is sent to publisher and gallery is awaiting
    :param request: The HTTP request
    :param status: Name of the modification
    :type status: str
    :param uuid : Uuid of the gallery entry
    :type uuid: str
    """
    gallery_entry = GalleryEntry.objects.get(bind_dashboard__uuid__iexact=uuid)
    if status == GalleryEntry.StatusChoices.PUBLISHED:
        if request.user.groups.filter(name="publisher").exists():
            gallery_entry.status = status
            gallery_entry.awaiting_publication = False
            gallery_entry.rejected = False
        else:
            gallery_entry.awaiting_publication = True
            publisher_group = User.objects.filter(groups__name='publisher')
            for publisher in publisher_group:
                from_email = publisher.email
                subject = 'MyGod : %s %s requested publication for a gallery entry to MyGOD' % (
                    request.user.first_name, request.user.last_name)
                email_template_name = "publication_gallery_template.txt"
                c = {
                    'site_root_url': settings.SITE_DOMAIN + '/' + settings.APPLICATION_CONTEXT,
                    "publisher_name": publisher.first_name + " " + publisher.last_name,
                    "from_user": request.user.first_name + " " + request.user.last_name,
                    "title": gallery_entry.title,
                    "summary": gallery_entry.summary
                }
                email = render_to_string(email_template_name, c)
                if hasattr(settings, 'DISABLE_MAIL_SENDING') is False or settings.DISABLE_MAIL_SENDING is False:
                    try:
                        send_mail(subject, email, settings.DEFAULT_FROM_EMAIL, [from_email], fail_silently=False)
                    except BadHeaderError:
                        return HttpResponse('Invalid header found.')
                else:
                    logger.debug(f"Mail not sent : {subject}, {email}, {settings.DEFAULT_FROM_EMAIL}, {from_email}")

    else:
        gallery_entry.status = status
        gallery_entry.awaiting_publication = False
    gallery_entry.save()
    return redirect('mygodapp:user_page')


def reject_gallery_entry(request):
    """
    User with publisher status are allowed to reject a promotion request from lambda user.
    Form need to be completed for rejection text.
    Once form is submitted, an email is sent to user
    :param request: The HTTP request
    """
    if request.method == "POST":
        uuid = request.POST.get('uuid')
        gallery_entry = GalleryEntry.objects.get(bind_dashboard__uuid__iexact=uuid)
        form = RejectionForm(request.POST)
        if form.is_valid():
            gallery_entry.rejected = True
            gallery_entry.awaiting_publication = False
            gallery_entry.save()

            from_email = gallery_entry.bind_dashboard.user.email
            subject = 'MyGod : Your Gallery Entry :  %s has been rejected' % (gallery_entry.title)
            email_template_name = "rejection_text.txt"
            c = {
                "user_name": gallery_entry.bind_dashboard.user.first_name + " " + gallery_entry.bind_dashboard.user.last_name,
                "gallery_title": gallery_entry.title,
                "rejection_text": form.cleaned_data['text']
            }
            email = render_to_string(email_template_name, c)
            if hasattr(settings, 'DISABLE_MAIL_SENDING') is False or settings.DISABLE_MAIL_SENDING is False:
                try:
                    send_mail(subject, email, settings.DEFAULT_FROM_EMAIL, [from_email], fail_silently=False)
                except BadHeaderError:
                    return HttpResponse('Invalid header found.')
            else:
                logger.debug(f"Mail not sent : {subject}, {email}, {settings.DEFAULT_FROM_EMAIL}, {from_email}")
            return redirect('mygodapp:user_page')
    else:
        if request.method == "GET":
            uuid = request.GET.get("uuid", "")
            form = RejectionForm(initial={'uuid': uuid})

    return render(request, template_name="rejection_form.html",
                  context={'form': form})


def remove_gallery_entry(request, uuid):
    """
    Retrieve uuid of related saved dashboard and remove gallery entry
    :param request: The HTTP request
    :param uuid: The uuid of gallery entry
    :type uuid: str
    """
    gallery_entry = GalleryEntry.objects.get(bind_dashboard__uuid__iexact=uuid)
    dashboard = DashboardSettings.objects.get(uuid__iexact=uuid)
    dashboard.promoted = False
    dashboard.save()
    gallery_entry.delete()

    return redirect('mygodapp:user_page')