import asyncio
import time

from django.shortcuts import render, redirect
from mygodapp.models import Taxon, PcReleve, Event, Sequence, DataSet, \
    DashboardSettings, \
    Taxonomy, GalleryEntry, MetabarDataSet, MorphoDataSet
from mygodapp.forms import NewUserForm, MetabarDatasetForm, MorphoDatasetForm, ClusterForm
from django.contrib.auth import authenticate
from django.contrib import auth
from django.contrib import messages
from django.contrib.auth.forms import AuthenticationForm
from django.core.mail import send_mail, BadHeaderError
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.models import User
from django.template.loader import render_to_string
from django.db.models.query_utils import Q
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.conf import settings
from guardian.shortcuts import get_perms
import logging
from mygodapp.views.commons import PARAMETER_MAPPINGS
from django.http import HttpResponse
from django.forms import formset_factory
import os
import copy
from mygodapp.tools.metabar import MetabarcodingDataLoader, MetabarMetadataLoader
from mygodapp.tools.delete_dataset import RemoveDataset
from mygodapp.tools.physico_chemical import PhysicoChemicalCsvLoader
from mygodapp.forms import MetabarImportForm, PhysicoImportForm
import uuid
from mygodapp.models import ImportDataRequest
from datetime import datetime
from django.http import JsonResponse
from django_q.tasks import async_task
import json

import re
import shutil

logger = logging.getLogger(__name__)


###############User Function#####################

def register_request(request):
    """
    Called whenever the user registration form is submitted.
    The following fields are expected in the form :
        - first name : optional
        - last name : optional
        - username : the username (unique in the database) that the user will use to login
        - email : the email address of the user
        - password1 and password2 : two fields that are supposed to contain the same information
    :param request : the HTTP request
    :return: If the submitted form is valid return to home page, instead rerender registration page with message error.
    """
    if request.method == "POST":
        form = NewUserForm(data=request.POST)
        if form.is_valid():
            form.save()
            return index(request)
        return render(request=request, template_name="sign_up.html", context={"register_form": form})
    else:
        form = NewUserForm()
    return render(request=request, template_name="sign_up.html", context={"register_form": form})


def login_request(request):
    """
    Called whenever a user submits the login form.
    The following fields are expected in the form:
        - username : a valid (already registered and activated) username
        - password : the user's password
    :param request: The HTTP request
    :return: If form is valid render user page , if not display form with error message
    """
    form = AuthenticationForm(data=request.POST)
    if form.is_valid():
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect("mygodapp:user_page")
        else:
            messages.error(request, "Invalid username or password.")
            return redirect('mygodapp:login')
    return render(request=request, template_name="sign_in.html", context={"login_form": form})


def logout(request):
    auth.logout(request)

    return index(request)


def password_reset_request(request):
    """
    Called whenever the password reset form is submitted.
    The form is supposed to contain the email address of a user
    :param request: The HTTP request
    :return: If the address matches a user, an e-mail with password reset information is sent.
    """
    if request.method == "POST":
        form = PasswordResetForm(data=request.POST or None)
        if form.is_valid():
            data = form.cleaned_data['email']
            associated_users = User.objects.filter(Q(email=data))
            if associated_users.exists():
                for user in associated_users:
                    subject = "Password Reset Requested"
                    email_template_name = "password_reset_mail.txt"
                    c = {
                        "email": user.email,
                        'site_root_url': settings.SITE_DOMAIN + '/' + settings.APPLICATION_CONTEXT,
                        'site_name': 'Website',
                        "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                        "user": user,
                        'token': default_token_generator.make_token(user),
                    }
                    email = render_to_string(email_template_name, c)
                    if hasattr(settings, 'DISABLE_MAIL_SENDING') is False or settings.DISABLE_MAIL_SENDING is False:
                        try:
                            send_mail(subject, email, settings.DEFAULT_FROM_EMAIL, [email], fail_silently=False)
                        except BadHeaderError:
                            return HttpResponse('Invalid header found.')
                    else:
                        logger.debug(f"Mail not sent : {subject}, {email}, {settings.DEFAULT_FROM_EMAIL}, {email}")
                    return render(request=request, template_name="index.html")
    form = PasswordResetForm()
    return render(request=request, template_name="password_reset.html", context={"password_form": form})


def user_page(request):
    """
    Render User Page
    Give some information:
        - Status : publisher or None
        - Personal information
        - Saved Dashboard
        - Save gallery Entry
        - Dataset (for dataset owner)
        - Public dashboard (shared by other user)
    :param request: the HTTP request
    :return: Render user page
    """
    modify_dataset = []
    publisher_status = False
    importer = False
    user = User.objects.get(username=request.user.username)

    dashboard_saved = DashboardSettings.objects.filter(user=user)
    gallery_entries = GalleryEntry.objects.filter(bind_dashboard__user=user)
    dataset_list = DataSet.objects.all()
    gallery_awaiting = GalleryEntry.objects.filter(awaiting_publication=True)
    dashboard_shared = DashboardSettings.objects.filter(shared=True)
    import_list = ImportDataRequest.objects.filter(from_user=request.user).order_by('-date')

    if request.user.groups.filter(name="publisher").exists():
        publisher_status = True
    if request.user.groups.filter(name="importers").exists():
        importer = True
    for dataset in dataset_list:
        if 'view_dataset_init' in get_perms(user, dataset):
            modify_dataset.append(dataset)
    ctx = {"dashboard_list": dashboard_saved, "gallery_entries": gallery_entries, "dataset_owned": modify_dataset,
           "gallery_awaiting": gallery_awaiting, "publisher_status": publisher_status,
           "dashboard_shared": dashboard_shared, "importer": importer, "import_list": import_list}
    return render(request=request, template_name="user.html",
                  context=ctx)


def share_dashboard(request, uuid, status):
    """
    Called when User want to publish a dashboard to every user
    :param request: The HTTP request
    :param uuid: the uuid of Dashboard settings
    :type uuid : str
    :param status: status of Dashbord settings
    :type status :str
    :return: Render user page
    """
    dashboard = DashboardSettings.objects.get(uuid__iexact=uuid)
    if status == "share":
        dashboard.shared = True
    else:
        dashboard.shared = False
    dashboard.save()
    return redirect('mygodapp:user_page')


def remove_dashboard(request, uuid):
    """
    Remove saved Dashboard
    :param request: the HTTP request
    :param uuid: The uuid of Dashboard setting
    :return: Render user page
    """
    dashboard = DashboardSettings.objects.get(uuid__iexact=uuid)
    dashboard.delete()
    return redirect('mygodapp:user_page')


def modify_metadata_dataset(request):
    """
    Modify the metadata associate with the dataset, the form can only be submitted by a data owner
    :param request: the HTTP request
    :return: Render dataset_update.html page with pre-complete form field relative to selected dataset.
            All dataset field can be changed instead of , taxonomy and related other datasets
    """
    if request.method == "POST":
        dataset_name = request.POST.get('description')
        try:
            dataset = MetabarDataSet.objects.get(description__iexact=dataset_name)
            form = MetabarDatasetForm(instance=dataset, initial={'description': dataset_name})
        except MetabarDataSet.DoesNotExist:
            dataset = MorphoDataSet.objects.get(description__iexact=dataset_name)
            form = MorphoDatasetForm(instance=dataset, initial={'description': dataset_name})
        if form.is_valid():
            form.save()
            return redirect('mygodapp:user_page')
    else:
        if request.method == "GET":
            dataset_name = request.GET.get("dataset_name", "")
            request.session['dataset_name'] = dataset_name
            try:
                dataset = MetabarDataSet.objects.get(description__iexact=dataset_name)
                form = MetabarDatasetForm(instance=dataset, initial={'description': dataset_name})
            except MetabarDataSet.DoesNotExist:
                dataset = MorphoDataSet.objects.get(description__iexact=dataset_name)
                form = MorphoDatasetForm(instance=dataset, initial={'description': dataset_name})
    return render(request, 'dataset_update.html', context={'form': form})


##############Function rendering template##########

def gallery(request):
    """
    Render gallery, gallery entries visible can have three status, shared with everyone,
    only visible for logged user and awaiting publication
    :param request: the HTTP request
    :return: Render galley page
    """
    published_dashboard = GalleryEntry.objects.filter(status="published").order_by('-last_saved')
    ctx = {"published_dashboard": published_dashboard, "shared_dashboard": None, "user_only_dashboard": None,
           "awaiting_entries": None}
    if request.user.is_authenticated:
        shared_dashboard = GalleryEntry.objects.filter(status="shared")
        user_only_dashboard = GalleryEntry.objects.filter(status="private",
                                                          bind_dashboard__user=request.user)
        awaiting_gallery_entries = GalleryEntry.objects.filter(awaiting_publication=True)
        ctx['shared_dashboard'] = shared_dashboard
        ctx['user_only_dashboard'] = user_only_dashboard
        ctx['awaiting_entries'] = awaiting_gallery_entries

    return render(request, template_name="gallery.html", context=ctx)


# @cache_page(60 * 15)
def index(request):
    """
    Render Home page
    :param request: the HTTP request
    :return: The home page url
    """
    taxonomy_list = []
    taxonomy_done_list = []
    sampling_station_list = []
    dataset_summaries = list()
    datasets = DataSet.objects.all()
    taxons_count = 0
    parameter_list = []
    count_temporal_metabar = DataSet.objects.filter(type="Temporal").count()
    count_geographical_metabar = DataSet.objects.filter(type="Geographical").count()
    pc_releve_count = PcReleve.objects.all().count()
    for taxonomy in Taxonomy.objects.all():
        taxons_count += Taxon.objects.filter(taxonomy_linked__name__iexact=taxonomy.name).count()
        logger.debug(f"taxon number {taxons_count}")
        if taxonomy.name.casefold() not in taxonomy_done_list:
            taxonomy_done_list.append(taxonomy.name.casefold())
            taxons_number = Taxon.objects.filter(taxonomy_linked__name__iexact=taxonomy.name).count()
            sequences_number = Sequence.objects.filter(taxon__taxonomy_linked__name__iexact=taxonomy.name).count()
            taxonomy_list.append(
                {'name': taxonomy.name, 'rank_description': taxonomy.rank_description,
                 "referenced_taxon": taxons_number,
                 "sequence_number": sequences_number})
    for parameter, name in PARAMETER_MAPPINGS.items():
        parameter_list.append(name['label'])

    # Build list of base layers for map combining
    # base layer definitions from the settings
    # with base layer definitions included in dataset metadata.
    baselayersbyname = {}
    for baselayer in settings.BASE_LAYERS:
        baselayersbyname[baselayer['name']] = baselayer
    for dataset in datasets:
        dataset_summaries.append({'name': dataset.name,
                                  'description': dataset.description,
                                  'samples': dataset.event_set.count(),
                                  'occurrences': dataset.occurrence_set.count(),
                                  'first_sample_date': dataset.first_sample_date,
                                  'last_sample_date': dataset.last_sample_date,
                                  })
        for baselayer in dataset.base_layers:
            baselayersbyname[baselayer['name']]=baselayer

    markers = list()
    events = Event.objects.values('station_code', 'latitude', 'longitude', 'bind_dataset__name').distinct()
    pc_parameter_count = len(PARAMETER_MAPPINGS)
    for event in events:
        if event['station_code'] not in sampling_station_list:
            sampling_station_list.append(event['station_code'])
        markers.append(
            {'dataset': event['bind_dataset__name'], 'station': event['station_code'],
             'latitude': event['latitude'],
             'longitude': event['longitude']})
    ctx = {'view': 'index',
           'taxonomies': taxonomy_list, "number_taxonomy": len(taxonomy_list),
           "meta_temporal_dataset": count_temporal_metabar, "meta_geographical_dataset": count_geographical_metabar,
           'datasets': dataset_summaries,
           'baselayers' : baselayersbyname.values(),
           'markers': markers, 'total_sampling': pc_releve_count, 'sampling_station': len(sampling_station_list),
           'parameter_count': pc_parameter_count, 'parameters': parameter_list, 'total_taxon_count': taxons_count}

    return render(request, 'index.html', context=ctx)


def search(request):
    """
    This function can be called in two cases.
    By using Search by Taxon or by selecting a saved Dashboard or Gallery entries in these cases it comes with additionals parameters :
        - uuid : the uuid of Dashboard settings
        - type : can be Shared or Private if dashboard have been promoted or not
    :param request: the HTTP request
    :return: Render dashboard page
    """
    uuid = request.GET.get("uuid", "")
    type = request.GET.get("type", "")
    ctx = {"uuid": uuid, "type": type}

    return render(request, 'dashboard.html', context=ctx)


##Search form template##

def dataset_description(request):
    """
    Render description of dataset page
    :param request: The HTTP request
    :return: The dataset description url
    """
    campaign_list = []
    campaign_name_list = []
    datasets = DataSet.objects.all()
    map_id = 0

    for dataset in datasets:
        if dataset.campaign not in campaign_name_list:
            related_pc_parameters_list = []
            for parameter in dataset.pc_parameter_list:
                related_pc_parameters_list += parameter.keys()
            markers = list()
            events = Event.objects.filter(bind_dataset__name=dataset.name).values('station_code', 'latitude',
                                                                                  'longitude').distinct()
            for event in events:
                markers.append(
                    {'station': event['station_code'],
                     'latitude': event['latitude'],
                     'longitude': event['longitude']})
            campaign_name_list.append(dataset.campaign)
            campaign_list.append(
                {'campaign_name': dataset.campaign, "site": dataset.site,
                 "markers": markers,
                 "related_publications": dataset.related_publications,
                 "date_start": dataset.first_sample_date,
                 "date_end": dataset.last_sample_date, "related_pc_parameter": related_pc_parameters_list,
                 "type": dataset.type,
                 "datasets": [], 'div_id': 'map-' + str(map_id), 'callback': 'callback_' + str(map_id)})
            map_id += 1

        for campaign in campaign_list:
            if campaign['campaign_name'] == dataset.campaign:
                campaign["datasets"].append(dataset)

    dataset_number = DataSet.objects.all().count()
    ctx = {'view': 'metabar_dataset', "datasets": datasets, "dataset_number": dataset_number,
           "campaign_list": campaign_list}

    return render(request, 'metabar_dataset.html', context=ctx)


######Import Tools#######

def handle_uploaded_file(f, path, filename):
    with open(path + filename, 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)


#
def import_metabar_data(request):
    """
    Function is called when form related to metabar dataset is submitted.
    :param request: The HTTP request
    :return: The dataset import log url
    """
    pid = str(uuid.uuid4())
    if request.method == "POST":
        event_file = request.FILES["event_file"]
        sample_file = request.FILES["sample_file"]
        metadata_file = request.FILES["metadata_file"]
        config_file = request.FILES["table_config_file"]
        if not event_file.name.endswith('.csv') or not sample_file.name.endswith(
                '.csv') or not config_file.name.endswith('.json') or not metadata_file.name.endswith('.json'):
            messages.warning(request, 'One of the files is wrong format')
            return HttpResponse('Invalid Format File.')
        root_folder = settings.IMPORT_DATA_ROOT
        username = request.user.username
        user_dir_path = os.path.join(root_folder, username)
        if not os.path.exists(user_dir_path):
            os.mkdir(user_dir_path)
        new_dir_path = os.path.join(root_folder, username, pid)
        if not os.path.exists(new_dir_path):
            os.mkdir(new_dir_path)
        subfolder = os.path.join(new_dir_path, "metabar")
        os.mkdir(subfolder)
        import_data_request = ImportDataRequest()
        import_data_request.dry_run = True
        import_data_request.uuid = pid
        import_data_request.date = datetime.today()
        import_data_request.state = "waiting"
        import_data_request.files = True
        import_data_request.type = "metabar"
        import_data_request.from_user = request.user
        import_data_request.save()
        handle_uploaded_file(event_file, subfolder + "/", "event.csv")
        handle_uploaded_file(sample_file, subfolder + "/", "sample.csv")
        handle_uploaded_file(metadata_file, subfolder + "/", "metadata.json")
        handle_uploaded_file(config_file, subfolder + "/", "config.json")
        data = {'uuid': pid, 'type': 'metabar', "dry_run": import_data_request.dry_run}

        return render(request, "log.html", data)
    form = MetabarImportForm()
    data = {"form": form, "pid": pid}
    return render(request, "metabarimport.html", data)


def import_physico_chemical_data(request):
    """
    Function is called when form related to physico-chemical dataset is submitted.
    :param request: The HTTP request
    :return: The dataset import log url
    """
    pid = str(uuid.uuid4())
    if request.method == "POST":
        dataset_name = request.POST.get('dataset_name')
        event_file = request.FILES["event_file"]
        config_file = request.FILES["table_config_file"]
        if not event_file.name.endswith('.csv') or not config_file.name.endswith('.json'):
            messages.warning(request, 'One of the files is wrong format')
            return HttpResponse('Invalid Format File.')
        root_folder = settings.IMPORT_DATA_ROOT
        username = request.user.username
        user_dir_path = os.path.join(root_folder, username)
        if not os.path.exists(user_dir_path):
            os.mkdir(user_dir_path)
        new_dir_path = os.path.join(root_folder, username, pid)
        if not os.path.exists(new_dir_path):
            os.mkdir(new_dir_path)
        subfolder = os.path.join(new_dir_path, "pc")
        os.mkdir(subfolder)
        import_data_request = ImportDataRequest()
        import_data_request.dry_run = True
        import_data_request.dataset_name = dataset_name
        import_data_request.uuid = pid
        import_data_request.date = datetime.today()
        import_data_request.files = True
        import_data_request.from_user = request.user
        import_data_request.state = "waiting"
        import_data_request.type = "pc"
        import_data_request.save()
        handle_uploaded_file(event_file, subfolder + "/", "event.csv")
        handle_uploaded_file(config_file, subfolder + "/", "config.json")
        data = {'uuid': pid, 'type': 'pc', "dry_run": import_data_request.dry_run}

        return render(request, "log.html", data)
    dataset_owned = []
    dataset_list = DataSet.objects.all()
    for dataset in dataset_list:
        if 'view_dataset_init' in get_perms(request.user, dataset):
            dataset_owned.append(dataset.name)
    dataset_list = tuple((dataset, dataset) for dataset in dataset_owned)
    form = PhysicoImportForm(dataset_list)
    data = {"form": form, "pid": pid, 'data_type': 'pc'}
    return render(request, "pcimport.html", data)


def check_process(request, pid, data_type, dry_run):
    """
    Function is called while checking logs of imported dataset.
    :param request: The HTTP request
    :param pid: UUID of import process
    :param data_type: Data type can be metabar or physico-chemical
    :param dry_run: Boolean, determine if once imported dataset must be removed or not
    :return: ['done': True or False if import process isn't finished,
                'content': All logs related to import process]
    """
    log = {}
    import_process = ImportDataRequest.objects.get(uuid=pid)
    username = request.user.username
    root_folder = settings.IMPORT_DATA_ROOT
    subfolder = os.path.join(root_folder, username, pid, data_type)
    log['done'] = False
    if import_process.state == "waiting":
        logger.debug(f"import process is waiting")
        import_process.state = "in_progress"
        import_process.save()
        if data_type == "metabar":
            async_task(processing_metabar, subfolder, import_process, dry_run)
        if data_type == "pc":
            async_task(processing_physico_chemical, subfolder, import_process, dry_run)
    else:
        try:
            log['content'] = list()
            loglines = list()
            with open(subfolder + "/" + 'import.log', 'r') as log_file:
                # Load whole file contents to allow releasing file lock ASAP.
                loglines = log_file.readlines()
            for log_line in loglines:
                json_log_entry_string = re.sub(r"^.*###JSON:", "", log_line)
                if log_line == json_log_entry_string:
                    json_log_entry = {'type': 'message', 'messageinfo': log_line}
                else:
                    json_log_entry = json.loads(json_log_entry_string)
                log['content'].append(json_log_entry)
        except FileNotFoundError:
            log['content'] = [{'type': 'error', 'message': 'File Not Found'}]
        if import_process.state == "complete":
            log['done'] = True
    return JsonResponse(log)


def import_dataset_in_database(request, pid, data_type):
    import_process = ImportDataRequest.objects.get(uuid=pid)
    username = request.user.username
    root_folder = settings.IMPORT_DATA_ROOT
    subfolder = os.path.join(root_folder, username, pid, data_type)
    import_process.state = "in_progress"
    import_process.dry_run = False
    import_process.save()
    if data_type == "metabar":
        async_task(processing_metabar, subfolder, import_process, import_process.dry_run)
    if data_type == "pc":
        async_task(processing_physico_chemical, subfolder, import_process, import_process.dry_run)
    data = {'uuid': pid, 'type': data_type, "dry_run": import_process.dry_run}
    return render(request, "log.html", data)


def processing_metabar(subfolder, import_process, dry_run):
    """
    Function is called while checking logs of imported dataset.
    :param subfolder: Name of the subfolder that contains dataset files
    :param import_process: Object ImportDataRequest related to import
    :param dry_run: None or on, determine if once imported dataset it must be removed or not
    """
    subfolder = subfolder + "/"
    table_config_file_path = open(subfolder + 'config.json')
    metadata_file_path = open(subfolder + 'metadata.json')
    event_file_path = open(subfolder + 'event.csv')
    sample_file_path = subfolder + 'sample.csv'
    with open(subfolder + 'metadata.json') as metadata_file:
        metadata = json.load(metadata_file)
        import_process.dataset_name = metadata['name']
        import_process.save()
    metadata_loader = MetabarMetadataLoader()
    import_logger = copy.deepcopy(logging.getLogger('importlogger'))
    file_handler = import_logger.handlers[0]
    import_logger.removeHandler(file_handler)
    importlogfile = os.path.join(subfolder, "import.log")

    # On Windows, the lock on the import log file may not be correctly released,
    # making it impossible to remove the file. In this case, the file will accumulate
    # the output of the dry-run as well as the real import process :-/
    if os.path.exists(importlogfile):
        try:
            os.remove(importlogfile)
        except PermissionError:
            logger.warning(f"Unable to remove previous logfile.")

    file_handler = logging.FileHandler(subfolder + "import.log")
    import_logger.addHandler(file_handler)
    metadata_loader.set_logger_file(import_logger)
    metadata_loader.load(metadata_file_path)
    taxonomy = metadata_loader.get_taxonomy()
    dataset = metadata_loader.get_dataset()
    metabarcode_loader = MetabarcodingDataLoader(dataset, taxonomy)
    metabarcode_loader.set_logger_file(import_logger)
    metabarcode_loader.load_config(table_config_file_path)
    rarefied_sample = metabarcode_loader.rarefy(sample_file_path)
    metabarcode_loader.load_data(sample_file_path, event_file_path, rarefied_sample, dry_run)
    import_process.state = "complete"
    import_process.save()


def processing_physico_chemical(subfolder, import_process, dry_run):
    """
    Function is called while checking logs of imported dataset.
    :param subfolder: Name of the subfolder that contains dataset files
    :param import_process: Object ImportDataRequest related to import
    :param dry_run: None or on, determine if once imported dataset it must be removed or not
    """

    subfolder = subfolder + "/"
    table_config_file_path = open(subfolder + 'config.json')
    with open(subfolder + 'config.json') as config_file:
        config = json.load(config_file)
        import_process.dataset_name = config['CAMPAIGN']
        import_process.save()
    event_file_path = open(subfolder + 'event.csv')
    physico_chemical_data_loader = PhysicoChemicalCsvLoader()
    import_logger = copy.deepcopy(logging.getLogger('importlogger'))
    file_handler = import_logger.handlers[0]
    import_logger.removeHandler(file_handler)
    importlogfile = os.path.join(subfolder, "import.log")
    if os.path.exists(importlogfile):
        os.remove(importlogfile)
    file_handler = logging.FileHandler(subfolder + "import.log")
    import_logger.addHandler(file_handler)
    physico_chemical_data_loader.set_logger_file(import_logger)
    physico_chemical_data_loader.load_config(table_config_file_path)
    physico_chemical_data_loader.load_data(event_file_path, dry_run)
    import_process.state = "complete"
    import_process.save()


def display_logs(request, pid):
    """
    Function is called while checking logs of imported dataset.
    :param request: The HTTP request
    :param pid : UUId of related ImportDataRequest
    :return Url of logs
    """
    dry_run = None
    import_process = ImportDataRequest.objects.get(uuid=pid, from_user=request.user)
    try:
        DataSet.objects.get(name=import_process.dataset_name)
    except DataSet.DoesNotExist:
        dry_run = "dry_run"
    data = {'uuid': import_process.uuid, "type": "metabar", "dry_run": dry_run}
    return render(request, "log.html", data)


def remove_files(request, pid):
    """
    Function is called while checking logs of imported dataset.
    :param request: The HTTP request
    :param pid: UUid of ImportDataRequest
    """
    import_process = ImportDataRequest.objects.get(uuid=pid)
    username = request.user.username
    root_folder = settings.IMPORT_DATA_ROOT
    subfolder = os.path.join(root_folder, username, pid)
    if os.path.exists(subfolder):
        shutil.rmtree(subfolder)
    else:
        logger.warning(f"Trying to remove none-existing import files {subfolder}.")
    import_process.files = False
    import_process.save()
    return redirect('mygodapp:user_page')


def remove_metabar_dataset(request, dataset_name):
    """
    Function is called while checking logs of imported dataset.
    :param request: The HTTP request
    :param dataset_name: UUid of ImportDataRequest
    """

    data_eraser = RemoveDataset()
    data_eraser.remove_metabar_data(dataset_name)
    dashboard_settings_list = DashboardSettings.objects.all()
    for dashboard in dashboard_settings_list:
        settings_panel = dashboard.settings['panels']
        for panel_uuid, search_dict in settings_panel.items():
            if search_dict['dataset'] == dataset_name:
                dashboard.delete()
    try:
        import_process = ImportDataRequest.objects.filter(dataset_name=dataset_name)
        import_process.delete()
    except ImportDataRequest.DoesNotExist:
        logger.debug("Related import process not found")
    return redirect('mygodapp:user_page')


def modify_cluster_dataset(request):
    """
    Render dataset_update.html page with precomplete form field relative to selected dataset.
    All dataset field can be changed instead of , taxonomy and related other datasets

    """
    clusterformset = formset_factory(ClusterForm, can_delete=True)
    if request.method == "POST":
        dataset_name = request.session['dataset_name']
        event_list = Event.objects.filter(bind_dataset__name__iexact=dataset_name).distinct()
        stations = []
        for event in event_list:
            if event.station_code not in stations:
                stations.append(event.station_code)
        station_list = tuple((station_code, station_code) for station_code in stations)
        formset = clusterformset(request.POST, form_kwargs={'station_list': station_list})
        if formset.is_valid():
            for form in formset:
                cluster_name = form.cleaned_data.get('cluster_name')
                stations_related = form.cleaned_data.get('station_choices')
                if form.cleaned_data.get('DELETE'):
                    cluster_name = "No cluster"
                if stations_related:
                    for station in stations_related:
                        events = Event.objects.filter(bind_dataset__name__iexact=dataset_name,
                                                      station_code__iexact=station)
                        for event in events:
                            event.cluster = cluster_name
                            event.save()
            return redirect('mygodapp:user_page')

    else:
        if request.method == "GET":
            dataset_name = request.GET.get("dataset_name", "")
            request.session['dataset_name'] = dataset_name
            event_list = Event.objects.filter(bind_dataset__name__iexact=dataset_name).distinct()
            stations = []
            cluster_list = []
            for event in event_list:
                if event.cluster:
                    if not any(event.cluster in cluster['cluster_name'] for cluster in cluster_list):
                        cluster_list.append({'cluster_name': event.cluster, 'related_stations': []})
                    cluster = next((cluster for cluster in cluster_list if cluster["cluster_name"] == event.cluster),
                                   None)
                    if cluster:
                        if event.station_code not in cluster['related_stations']:
                            cluster['related_stations'].append(event.station_code)
                if event.station_code not in stations:
                    stations.append(event.station_code)
            extra_number = len(cluster_list)
            if len(cluster_list) == 0:
                extra_number = 1
            stations.sort()
            station_list = tuple((station_code, station_code) for station_code in stations)
            clusterformset = formset_factory(ClusterForm, extra=extra_number, can_delete=True)
            formset = clusterformset(form_kwargs={'station_list': station_list})
            index = 0
            if len(cluster_list) == 0:
                formset[0].fields['station_choices'].initial = None
                formset[0].fields['cluster_name'].initial = None
            else:
                for form in formset:
                    form.fields['station_choices'].initial = cluster_list[index]['related_stations']
                    form.fields['cluster_name'].initial = cluster_list[index]['cluster_name']
                    index += 1

    return render(request, 'cluster_form.html', context={'formset': formset})
