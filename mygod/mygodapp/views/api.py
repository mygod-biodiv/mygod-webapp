import calendar
import functools
import math
import random
import json
import logging
import datetime
import uuid
import pandas as pd
import rpy2
import rpy2.robjects as ro
import os
import platform
import string
from django.contrib.postgres.aggregates import ArrayAgg
from mygodapp.models import Measure, CumulatedMeasurement
from django.forms.models import model_to_dict
from rpy2.robjects.packages import STAP
import rpy2.robjects.packages as rpackages
from copy import deepcopy
from rpy2.robjects.conversion import localconverter
from rpy2.robjects import pandas2ri
from django.core.exceptions import FieldError, FieldDoesNotExist
from django.http import JsonResponse, HttpResponse
from mygodapp.models import Taxon, PcReleve, Event, Sequence, DataSet, \
    DashboardSettings, \
    Taxonomy, GalleryEntry, MetabarDataSet, MorphoDataSet
from django.db.models.query_utils import Q
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from rest_framework import status as http_status
from mygodapp.views.commons import MyGodApiException, PARAMETER_MAPPINGS, rawToVisibleOption
import math

logger = logging.getLogger(__name__)

# Monkey Patch r2py to prevent character encoding issues
if platform.system() == 'Windows':
    def _cchar_to_str(c, encoding: str = 'cp1252') -> str:
        # TODO: use isStrinb and installTrChar
        s = rpy2.rinterface_lib.openrlib.ffi.string(c).decode(encoding)
        return s


    def _cchar_to_str_with_maxlen(c, maxlen: int, encoding: str = 'cp1252') -> str:
        # TODO: use isStrinb and installTrChar
        s = rpy2.rinterface_lib.openrlib.ffi.string(c, maxlen).decode('cp1252')
        return s


    rpy2.rinterface_lib.conversion._cchar_to_str = _cchar_to_str
    rpy2.rinterface_lib.conversion._cchar_to_str_with_maxlen = _cchar_to_str_with_maxlen

R_LIBS = None
R_LIB_NAMES = ('vegan', 'data.table', 'parallelDist')
R_BETA_DIVERSITY_FUNC = None


def _init_r_libs():
    """
        Initialize R components.

        Actual initialization occurs only on first call to method.
    """
    global R_LIBS, R_BETA_DIVERSITY_FUNC
    if R_LIBS is None:
        logger.debug("Initializing R libraries & functions.")
        R_LIBS = list()
        for r_lib_name in R_LIB_NAMES:
            R_LIBS.append(rpackages.importr(r_lib_name))
            script_file = open(os.path.join(settings.R_SCRIPT_DIR, 'beta_diversity.r'), encoding="utf8")
            braycurtis_distance_mtrix_script = ''.join(script_file.readlines())
            R_BETA_DIVERSITY_FUNC = STAP(braycurtis_distance_mtrix_script, "braycurtis_distance_matrix")


########Generic Function########

def get_taxon(name):
    """
    Retrieve child taxon object using the name and rank selected

    :param name: Name of selected taxon prefixed with its ID (pk) and a pipe as in "1|Bacteria"
    :type name: str
    :return: Queryset containing the whole of the selected taxonomic subtree
    """

    pk_list = []
    name_list = name.split("|")
    if len(name_list) == 2:
        pk = name_list[0]
        try:
            taxon = Taxon.objects.get(pk=pk)
            for rank_child in taxon.children_list:
                pk_list = pk_list + rank_child['taxon_id']
            taxon_list = Taxon.objects.filter(pk__in=pk_list)
        except Taxon.DoesNotExist:
            raise MyGodApiException("get_taxon", f"No taxon matching primary key {pk} found.", logging.ERROR)
    else:
        raise MyGodApiException("get_taxon", f"Unable to extract pk from argument {name}.", logging.ERROR)

    return taxon_list


def get_dataset_datatype(request, dataset_name):
    """
    Returns the datatype of a given dataset
    :param request: the HTTP request
    :param dataset_name: Name of selected dataset
    :type dataset_name: str
    :return: A JSON string with dataset type
    """
    res = ""
    try:
        if dataset_name is None:
            raise MyGodApiException("get_dataset_datatype", "None argument given", logging.ERROR)
        dataset = DataSet.objects.get(name__iexact=dataset_name)
        res = dataset.datatype
    except MyGodApiException as e:
        e.log()

    return JsonResponse(res, safe=False)


def get_asv(asv, dataset_name):
    """
    Retrieve sequence object using the amplicon selected
    The following fields are expected

    :param asv: Amplicon relative to sequence
    :type asv: str
    :param dataset_name: Name of selected dataset
    :type: dataset_name:str
    :return: A JSON string with sequence
    """

    sequence = Sequence.objects.get(amplicon__iexact=asv, taxon__taxonomy_linked__dataset__name__iexact=dataset_name)
    return sequence


def get_season_from_date(event_date):
    """
    Return the name of the season matching a date
    :param event_date: A date object
    :type event_date: date
    :return: A string representation of the season matching the given date
    """
    md = event_date.month * 100 + event_date.day
    if (md > 320) and (md < 621):
        season = "Spring"
    elif (md > 620) and (md < 923):
        season = "Summer"
    elif (md > 922) and (md < 1223):
        season = "Autumn"
    else:
        season = "Winter"
    return season


#####Settings function#######

@require_http_methods(["POST"])
@csrf_exempt
def save_settings(request):
    """
    Save a user's dashboard and search panel settings.
    This method supports only POST to allow for potentially large settings to be sent.

    The message body is supposed to contain a 'data' field with a stringified JSON structure.
    The following fields are expected in the JSON structure :
        - settings : a JSON formatted dictionary with the complete dashboard configuration
        - id : the id of the settings to be saved (optional, will be generated if missing)
        - name : a human-readable name for the settings (optional, will be generated if missing)
    :param request: the HTTP request
    :return: Id of the saved/updated settings.
    """

    status_code = http_status.HTTP_200_OK
    status_message = ''

    res = {}

    try:

        if not request.user.is_authenticated:
            status_message = "Authentication needed to save settings."
            status_code = http_status.HTTP_403_FORBIDDEN
            raise MyGodApiException("save_settings", status_message)

        user = request.user

        if 'data' not in request.POST:
            status_message = "Missing 'data' field in POST request."
            status_code = http_status.HTTP_400_BAD_REQUEST
            raise MyGodApiException('save_settings', status_message)

        post_data = json.loads(request.POST['data'])

        dashboard_settings = DashboardSettings()
        dashboard_settings.uuid = str(uuid.uuid4())
        dashboard_settings.user = user
        dashboard_settings.created = datetime.datetime.utcnow()
        dashboard_settings.last_saved = dashboard_settings.created

        if 'id' in post_data:
            try:
                db_settings = DashboardSettings.objects.get(uuid=post_data['id'])
                dashboard_settings = db_settings
                dashboard_settings.last_saved = datetime.datetime.utcnow()
            except DashboardSettings.DoesNotExist:
                status_code = http_status.HTTP_400_BAD_REQUEST
                status_message = 'Invalid settings ID provided.'
                raise MyGodApiException('save_settings', status_message)
            if user != db_settings.user:
                raise MyGodApiException('save_settings',
                                        f"Unauthorized save attempt of dashboard {db_settings.uuid} by user {user}.")

        if 'name' in post_data:
            dashboard_settings.name = post_data['name']
        else:
            dashboard_settings.name = f"Unnamed settings ({dashboard_settings.uuid})"

        if 'settings' not in post_data:
            status_message = "Missing 'settings' field on form data."
            status_code = http_status.HTTP_400_BAD_REQUEST
            raise MyGodApiException('save_settings', status_message)

        dashboard_settings.settings = post_data['settings']

        dashboard_settings.save()
        res['id'] = dashboard_settings.uuid

    except MyGodApiException as e:
        e.log()

    response = None

    if status_code == http_status.HTTP_200_OK:
        response = JsonResponse(res, safe=False)
    else:
        response = HttpResponse(status=status_code, reason=status_message)

    return response


def load_settings(request, settings_id):
    """
    Returns previously saved settings (Dashboard + Taxon Search Panels)
    :param request: The HTTP request
    :param settings_id: The identifier of a previously saved settings configuration
    :type settings_id:str
    :return: A JSON structure with the complete settings
    """

    status_code = http_status.HTTP_200_OK
    status_message = ''

    res = {}

    try:

        user = None
        if request.user.is_authenticated:
            user = request.user

        if settings_id is None:
            status_code = http_status.HTTP_400_BAD_REQUEST
            status_message = 'Missing settings identifier'
            raise MyGodApiException('load_settings', 'Missing settings_id')

        try:
            db_settings = DashboardSettings.objects.get(uuid=settings_id)
            if db_settings.user != user and db_settings.shared is False:
                status_code = http_status.HTTP_403_FORBIDDEN
                status_message = 'User unauthorized to access requested settings'
                raise MyGodApiException('load_settings', status_message)

            res = {
                'id': db_settings.uuid,
                'name': db_settings.name,
                'creationdate': db_settings.created,
                'lastsaved': db_settings.last_saved,
                'settings': db_settings.settings,
            }
        except DashboardSettings.DoesNotExist:
            status_code = http_status.HTTP_404_NOT_FOUND
            status_message = 'Unable to load settings with provided id'
            raise MyGodApiException('load_settings', status_message)

    except MyGodApiException as e:
        e.log()

    response = None

    if status_code == http_status.HTTP_200_OK:
        response = JsonResponse(res, safe=False)
    else:
        response = HttpResponse(status=status_code, reason=status_message)

    return response


def list_settings(request):
    """
    Returns the list of available (previously saved) settings for a user
    :param request: The HTTP request
    :return: A JSON list with dictionary entries. Each entry has the following keys:
     {'id': a UUID-like string identifying the settings, 'name': the name that was given by the user when saving the settings,
     'creationdate': the date & time these settings where first saved,'lastsaved': the date & time these settings where last saved}
    """

    status_code = http_status.HTTP_200_OK
    status_message = ''

    res = {}
    settings_user = []
    settings_shared = []

    try:
        if request.user.is_authenticated:
            user = request.user
            settings_user_db = DashboardSettings.objects.filter(user=user)
            settings_shared_db = DashboardSettings.objects.filter(shared=True)
            for settings_db in settings_user_db:
                settings_info = {
                    'id': settings_db.uuid,
                    'name': settings_db.name,
                    'creationdate': settings_db.created,
                    'lastsaved': settings_db.last_saved,
                }
                settings_user.append(settings_info)
            settings_user.sort(key=lambda e: e['name'])
            for settings_db in settings_shared_db:
                settings_info = {
                    'id': settings_db.uuid,
                    'name': settings_db.name,
                    'creationdate': settings_db.created,
                    'lastsaved': settings_db.last_saved,
                }
                settings_shared.append(settings_info)

            settings_shared.sort(key=lambda e: e['name'])
            res = {"Your Dashboards": settings_user, "Shared Dashboards": settings_shared}
        else:
            raise MyGodApiException('list_settings', 'user is None', logging.WARNING)
    except MyGodApiException as e:
        e.log()

    return JsonResponse(res, safe=False)


######Gallery Entry Info######

def retrieve_gallery_entry(request, uuid):
    """
    CHeck if Dashboard has a gallery entry
    :param request: the HTTP request
    :param uuid: The uuid of related dashboard settings
    :type uuid:str
    :return: True if the selected dashboard is published
    """

    published_dash = None
    entry = {}
    try:
        if uuid is None:
            raise MyGodApiException("check_published_dash", "None argument given", logging.ERROR)
        gallery_entry = GalleryEntry.objects.get(bind_dashboard__uuid__iexact=uuid)
        entry = {'text': gallery_entry.text, 'summary': gallery_entry.summary, "title": gallery_entry.title,
                 "image": settings.MEDIA_URL + str(gallery_entry.image)}
    except MyGodApiException as e:
        e.log()

    return JsonResponse(
        {"published": published_dash, "entry": entry}, safe=False)


#######Api Function########


def get_dataset_names(request):
    """
    Return the list of dataset names.
    Used by the API to build a selector with the dataset names (TaxonSelector)
    :param request: the HTTP request
    :return: A JSON formatted dictionary with the following keys:
            {'text' : a string with the name of the dataset, 'value' : a unique string representation of the dataset name}
    """

    datasets = DataSet.objects.filter(status="complete").order_by('name')
    dataset_names = [{'text': dataset.name, 'value': dataset.name} for dataset in datasets]
    return JsonResponse(dataset_names, safe=False)


def get_dataset_type(request, dataset_name):
    """
    Return the type of a dataset.
    Used by the API to display default dashboard

    :param request: the HTTP request
    :param dataset_name: Name of dataset selected
    :type dataset_name : str
    :return: Type of dataset selected (Temporal or Geographical)
    """

    dataset = DataSet.objects.get(name__iexact=dataset_name)
    return JsonResponse(dataset.type, safe=False)


def get_taxonomy_ranks(request, dataset_name):
    """
    Return information about the taxonomy ranks of a dataset.
    Used by the API to fill a selection menu with the names of the ranks in the taxonomy
    associated to a given dataset.
    :param request: The HTTP request
    :param dataset_name: The name of the dataset selected
    :type dataset_name : str
    :return: A JSON formatted dictionary with  following keys:
            {'text' : the actual name of the taxonomical rank ('Species,'Genus,'Order,'Class',...),
            'value' :  a normalized (lowercase) representation of the rank name}
    """
    taxonomy_ranks = []
    try:
        if dataset_name is None:
            raise MyGodApiException("get_taxonomy_ranks", "No dataset name provided.", logging.WARNING)

        dataset = DataSet.objects.get(name=dataset_name)
        if dataset is None:
            raise MyGodApiException("get_taxonomy_ranks", f"No dataset found with name {dataset_name}", logging.WARNING)

        taxonomy_ranks = [{'text': description, 'value': description.lower()} for description in
                          dataset.taxonomy.rank_description]
        taxonomy_ranks.append({"text": "Any", "value": "Any"})

    except MyGodApiException as e:
        e.log()

    return JsonResponse(taxonomy_ranks, safe=False)


def get_taxonomy_subranks(request, dataset_name, rank, taxon):
    """
    Return information about the taxonomy ranks of a dataset.
    Used by the API to fill a selection menu with the names of the ranks in the taxonomy
    associated to a given dataset
    :param request: the HTTP request
    :param dataset_name: The name of the dataset for which the taxonomy rank information is requested
    :type dataset_name: str
    :param rank: The rank for which the taxonomy rank information is requested
    :type rank: str
    :param taxon: A string composed of the ID of a taxon, a pipe symbol, and a taxon name
    :type taxon : str
    :return: A JSON formatted list of ranks
    """
    sub_taxonomy_ranks = []
    taxon_name_list = taxon.split('|')
    if len(taxon_name_list) == 2:
        taxon_name = taxon_name_list[1]
        try:
            if dataset_name is None:
                raise MyGodApiException("get_taxonomy_ranks", "No dataset name provided.", logging.WARNING)

            dataset = DataSet.objects.get(name=dataset_name)
            if dataset is None:
                raise MyGodApiException("get_taxonomy_ranks", f"No dataset found with name {dataset_name}",
                                        logging.WARNING)

            if rank == "Any":
                taxon = Taxon.objects.get(name__iexact=taxon_name, taxonomy_linked__dataset__name__iexact=dataset_name)
            for rank_child in taxon.children_list:
                sub_taxonomy_ranks.append(string.capwords(rank_child['rank_name']))
        except MyGodApiException as e:
            e.log()
    else:
        e = MyGodApiException("get_taxonomy_ranks", f"Unable to extract taxon id from {taxon}", logging.WARNING)
        e.log()

    return JsonResponse(sub_taxonomy_ranks, safe=False)


def get_taxon_proposals(request, rank, taxon_header, dataset_name):
    """
    Return taxon names of a specific rank, and starting with a specifc string.
    Used by the API to fill an autocompleter with taxon name proposals, based on matches with
    the string given as argument
    :param request: The HTTP request
    :param rank: The taxonomy rank where the lookups will be performed
    :type rank : str
    :param taxon_header: A string matching the beginning of taxon names to look for
    :type taxon_header : str
    :param dataset_name: name of dataset selected
    :type dataset_name : str
    :return: A list of strings : taxon names of the specified rank beginning with the taxon_header string
    """
    res = []
    try:
        if rank is None or taxon_header is None:
            raise MyGodApiException("get_taxon_proposals", f"None argument given: {rank}, {taxon_header}",
                                    logging.WARNING)
        if rank != "Any":
            proposals = Taxon.objects.filter(name__istartswith=taxon_header).filter(rank__iexact=rank).filter(
                taxonomy_linked__dataset__name__iexact=dataset_name)
        else:
            proposals = Taxon.objects.filter(name__istartswith=taxon_header).filter(
                taxonomy_linked__dataset__name__iexact=dataset_name)
        for proposal in proposals:
            res.append({"name": proposal.name, "id": str(proposal.pk) + "|" + proposal.name})
    except MyGodApiException as e:
        e.log()

    return JsonResponse(res, safe=False)


def get_tree_taxonomic(taxon):
    """
    Return the taxonomic subtree starting at the taxon provided as argument.
    Called by get_taxonomic_tree_for_dataset()
    :param taxon: Taxon used as root for the subtree construction
    :type taxon : object
    :return: A (recursively built) dictionary with the following keys :
                {'id' : the name of the taxon provided as argument,
                'label' : a human-readable version of the taxon name
                'children' :  a list of dictionaries with the same keys, one entry per child node}
    """
    tree = {"id": str(taxon.pk) + "|" + taxon.name, "label": rawToVisibleOption(taxon.name)}
    if taxon.children.all().exists():
        children = list()
        for child in taxon.children.all():
            child_dict = get_tree_taxonomic(child)
            if child_dict is not None:
                children.append(child_dict)
        tree['children'] = children
        return tree
    return tree


def get_taxonomic_tree_for_dataset(request, dataset_name):
    """
    Used by the API to generate search tree in TaxonSearchPanel.
    :param request: The HTTP request
    :param dataset_name: The name of a dataset used for computing the tree
    :type dataset_name : str
    :return: Taxonomic tree for selected dataset.
    """

    res = []

    try:
        if dataset_name is None:
            raise MyGodApiException("get_taxonomic_tree_for_dataset",
                                    f"None argument provided: {dataset_name}")

        taxonomy = Taxonomy.objects.get(dataset__name__iexact=dataset_name)
        high_rank = taxonomy.rank_description[0]
        taxons = Taxon.objects.filter(taxonomy_linked__dataset__name__iexact=dataset_name, rank__iexact=high_rank)
        for taxon in taxons:
            final_tree = get_tree_taxonomic(taxon)
            res.append(final_tree)
    except MyGodApiException as e:
        e.log()

    return JsonResponse(res, safe=False)


def get_dataset_date_interval(request, dataset_name):
    """
    Return the earliest and latest sampling dates for a dataset.

    :param request: The HTTP request
    :param dataset_name: The name of the dataset used to determine start and end dates
    :type dataset_name : str
    :return: A JSON formatted dictionary with two keys :
            {'date_start' : the ddte of the earliest samples(format YYYY-MM-DD),
             'date_end' : the ddte of the latest sample (format YYYY-MM-DD)}
    """

    res = {'date_start': None, 'date_end': None}

    try:

        if dataset_name is None:
            raise MyGodApiException("get_dataset_date_interval", "No dataset name provided.", logging.WARNING)

        datasets = list(DataSet.objects.filter(name=dataset_name).all())
        if len(datasets) != 1:
            raise MyGodApiException("get_dataset_date_interval",
                                    f"Unexpected number of datasets matching f{dataset_name} : f{len(datasets)}.",
                                    logging.WARNING)

        dataset = datasets[0]
        res['date_start'] = dataset.first_sample_date
        res['date_end'] = dataset.last_sample_date

    except MyGodApiException as e:
        e.log()

    return JsonResponse(res)


def get_matches_for_summary(request, dataset_name, rank, taxon, date_start, date_end):
    """
    Return the number of sequences available for a specific taxon.
    Used by the API to generate information in the SummaryWidget (Dashboard component)
    :param request: The HTTP request
    :param dataset_name: The name of a dataset used for computing the sequence count
    :param rank: Rank used for computing the sequence count
    :param taxon: Taxon name used for computing the sequence count
    :param date_start: Start of the date range
    :param date_end: End of the date range
    :return: A dictionary with the following keys :
            {'taxonomy_name'-- name of taxonomy database used for taxon assignation,
            'taxonomy_version' --Version of Taxonomic database
            'campaign' -- name of related campaign
            'project' -- name of related project
            'doi' -- doi
            'marker' -- marker used for metabarcoding analysis
            'samples' -- number of reltaed sampling event
            'sites' -- number of sampling sites
            'depths' -- number of sampling depths
            'taxons' -- number of taxon belonging to taxonomic subtree
            'sequences' -- number of sequences related to taxonomic subtree }
    """
    matches = {}
    try:
        if dataset_name is None or rank is None or taxon is None or date_start is None or date_end is None:
            raise MyGodApiException("get_matches_for_summary",
                                    f"None argument provided: {dataset_name}, {rank}, {taxon}")
        stations_list = []
        depths_list = []
        cluster_list = []
        taxons = get_taxon(taxon)
        dataset_type = DataSet.objects.get(name__iexact=dataset_name)
        marker = None
        if dataset_type.datatype == "metabarcode":
            dataset = MetabarDataSet.objects.get(name__iexact=dataset_name)
            sequences = sum([taxon.sequence_set.count() for taxon in taxons])
            marker = dataset.marker
        if dataset_type.datatype == "morphological":
            dataset = MorphoDataSet.objects.get(name__iexact=dataset_name)
            sequences = "None"
        taxonomy = Taxonomy.objects.get(dataset__name__iexact=dataset_name)
        events = Event.objects.filter(bind_dataset__name__iexact=dataset_name,
                                      date__range=(date_start, date_end)).values("station_code", "depth_code",
                                                                                 "cluster")

        for event in events:
            if event['station_code'] not in stations_list:
                stations_list.append(event['station_code'])
            if event['depth_code'] not in depths_list:
                depths_list.append(event['depth_code'])
            if event['cluster']:
                if not any(event['cluster'] in cluster['cluster_name'] for cluster in cluster_list):
                    cluster_list.append({'cluster_name': event['cluster'], 'related_stations': []})
                cluster = next((cluster for cluster in cluster_list if cluster["cluster_name"] == event['cluster']),
                               None)
                if cluster:
                    if event['station_code'] not in cluster['related_stations']:
                        cluster['related_stations'].append(event['station_code'])
        matches['dataset'] = dataset_name
        matches['start_date'] = date_start
        matches['end_date'] = date_end
        matches['taxon'] = taxon
        matches['rank'] = rank
        matches['taxonomy_name'] = taxonomy.name
        matches["taxonomy_version"] = taxonomy.version
        matches['campaign'] = dataset.campaign
        matches['project'] = dataset.project
        matches["doi"] = dataset.doi
        matches["marker"] = marker
        matches["samples"] = events.count()
        matches['sites'] = len(stations_list)
        matches['depths'] = len(depths_list)
        matches['taxons'] = len(taxons)
        matches['sequences'] = sequences
        matches['clusters'] = cluster_list

    except MyGodApiException as e:
        e.log()

    return JsonResponse(matches, safe=False)


def get_dataset_metadata(request, dataset_name):
    """
    Return the metadata available for a dataset.
    Used by the API to fill a metadata file for export (Dashboard export functionnality)
    :param request: the HTTP request
    :param dataset_name: the name of the dataset
    :type dataset_name : str
    :return: A JSON-formatted list with available metadata for the dataset
    """
    metadata = []
    try:
        if dataset_name is None:
            raise MyGodApiException("get_dataset_metadata", f"None argument provided: {dataset_name}")
        try:
            dataset = MetabarDataSet.objects.get(name=dataset_name)
        except MetabarDataSet.DoesNotExist:
            dataset = MorphoDataSet.objects.get(name=dataset_name)
        metadata.extend(dataset.get_fields())
    except MyGodApiException as e:
        e.log()
    return JsonResponse(metadata, safe=False)


def get_fasta_sequences_for_taxon_in_dataset(request, dataset_name, taxon_name):
    """
    Return a FASTA formatted text block with the sequences for a taxon in a dataset
    Used by the API in the summary widget
    :param request: The HTTP request
    :param dataset_name: The name of the dataset
    :type dataset_name : str
    :param taxon_name: The name of a taxon
    :type taxon_name : str
    :return: A string with a FASTA formatted set of sequences
    """
    sequences = ""
    try:
        if dataset_name is None or taxon_name is None:
            raise MyGodApiException("get_fasta_sequences_for_taxon_in_dataset",
                                    f"None argument provided: dataset {dataset_name} and/or taxon_name {taxon_name}")
        taxons = get_taxon(taxon_name)
        for taxon in taxons:
            sequences_list = Sequence.objects.filter(taxon=taxon)
            for sequence in sequences_list:
                sequences += ">" + sequence.amplicon + taxon.compiled + "\n" + sequence.sequence + "\n"
    except MyGodApiException as e:
        e.log()

    return JsonResponse(sequences, safe=False)


def get_physico_chemical_parameters_for_dataset(request, dataset_name):
    """
    Return information about the physico-chemical parameters available for a dataset
    Used by the API to fill a selector in the ScatterPlot (Dashboard Component)
    :param request: The HTTP request
    :param dataset_name: The name of the dataset used to retrieve available parameters
    :type dataset_name : str
    :return: A dictionary with the following keys :
            {'text' : A human readable string with the name of a parameter
            'value' : A short unique string representing the parameter }
    """

    # TODO : Fix method to use dynamic dataset based lookup instead of returning a fixed dictionary.
    parameters = []
    dataset = DataSet.objects.get(name__iexact=dataset_name)
    for parameter in dataset.pc_parameter_list:
        for key, value in parameter.items():
            parameters.append({
                'text': key,
                'value': value
            })
    return JsonResponse(parameters, safe=False)


def get_date_list_for_dataset(request, dataset_name, date_start, date_end):
    """
    Returns information about all taxons present in the subtree of a specific taxon
    Used by the API to generate a selector with all taxons 'below' a given taxon in a taxonomic tree (ScatterPlot
    in the Dashboard). The root node of the subtree is defined by the arguments
    :param request: The HTTP request
    :param dataset_name: The name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param date_start: The start of the date range
    :type date_start : date
    :param date_end: The end of the date range
    :type date_end : date
    :return: A list with all dates matching events in the dataset.
    """
    date_list = []

    try:
        if dataset_name is None or date_start is None or date_end is None:
            raise MyGodApiException("get_date_list_for_dataset",
                                    f"None argument provided: {dataset_name}, {date_start}, {date_end}")
        events = Event.objects.filter(occurrence__from_dataset__name__iexact=dataset_name,
                                      date__range=(date_start, date_end)).values("date")
        for event in events:
            date_list.append(event['date'])
        date_list = list(dict.fromkeys(date_list))
    except MyGodApiException as e:
        e.log()

    return JsonResponse(date_list, safe=False)


def get_depth_codes_for_dataset(request, dataset_name):
    """
    Return information about the available depth code for a dataset
    Used by the API to generate  selector buttons with depth code (MapWidget in the Dashboard).
    :param request: The HTTP request
    :param dataset_name: The name of the dataset used to retrieve available depht code
    :type dataset_name : str
    :return:  A dictionary with the following keys :
                {'depth' : A human readable string with a representation of the Depth_code,
                'visible' : A bool used by map to display or not layer related to depth}
    """

    res = []
    try:
        if dataset_name is None:
            raise MyGodApiException("get_depth_code_for_dataset", f"None argument provided: {dataset_name}")
        depth_codes = []
        events = Event.objects.filter(occurrence__from_dataset__name__iexact=dataset_name)
        for event in events:
            if event.depth_code not in depth_codes:
                depth_codes.append(event.depth_code)
        depth_codes.sort()
        visible = True
        for depth in depth_codes:
            res.append({"depth": depth, "visible": visible})
            visible = False

    except MyGodApiException as e:
        e.log()

    return JsonResponse(res, safe=False)


def get_fraction_sizes_for_dataset(request, dataset_name):
    """
    Return information about the available fraction sizes for a dataset
    Used by the API to generate a list of radio buttons with fraction sizes (ScatterPlot in the Dashboard).
    :param request: The HTTP request
    :param dataset_name: The name of the dataset used to retrieve available fraction sizes
    :return: A dictionary with the following keys :
            {'text' : A human readable string with a representation of the fraction size,
            'value' : A short unique string representing the fraction size}
    """

    # TODO : Fix method to dynamically lookup fraction sizes instead of returning a fixed dictionary.
    fraction_sizes = []
    try:
        if dataset_name is None:
            raise MyGodApiException("get_fraction_size_for_dataset", "No dataset name provided.", logging.WARNING)

        dataset = DataSet.objects.get(name=dataset_name)

        if dataset is None:
            raise MyGodApiException("get_fraction_size_for_dataset", f"No dataset found with name {dataset_name}",
                                    logging.WARNING)

        for fraction in dataset.fraction:
            for text, value in fraction.items():
                fraction_sizes.append({'text': text, 'value': value})

        return JsonResponse(fraction_sizes, safe=False)
    except MyGodApiException as e:
        e.log()
    return JsonResponse(fraction_sizes, safe=False)


def get_taxon_subtree(request, rank, taxon_name):
    """
    Returns information about all taxons present in the subtree of a specific taxon
    Used by the API to generate a selector with all taxons 'below' a given taxon in a taxonomic tree (ScatterPlot
    in the Dashboard). The root node of the subtree is defined by the arguments
    :param request: The HTTP request
    :param rank: the name of the rank of the taxon
    :type rank : str
    :param taxon_name: the name of the taxon
    :type taxon_name : str
    :return: A dictionary with the following keys :
            {'text' : A human readable string with a human readable representation of the fraction size,
            'value' : A short unique string concatenating the rank / name of each of the taxons in the subtree}
    """
    subtree = {}
    try:
        if rank is None or taxon_name is None:
            raise MyGodApiException("get_taxon_subtree", f"None argument provided:{rank}, {taxon_name}")
        name_list = taxon_name.split("|")
        pk = name_list[0]
        taxon = Taxon.objects.get(pk=pk)
        for rank_child in taxon.children_list:
            subtree[rank_child['rank_name']] = []
            for children_pk in rank_child['taxon_id']:
                taxon_child = Taxon.objects.get(pk=children_pk)
                subtree[taxon_child.rank].append(
                    {"text": rawToVisibleOption(taxon_child.name),
                     "value": taxon.rank + "@" + str(taxon_child.pk) + "|" + taxon_child.name})
    except MyGodApiException as e:
        e.log()

    return JsonResponse(subtree, safe=False)


def get_asv_subtree(request, dataset_name, rank, taxon_name):
    """
    Returns information about all asv present in the subtree of each taxon composing a specific taxon
    Used by the API to generate a selector with all asv 'below' a given taxon in a taxonomic tree (ScatterPlot
    in the Dashboard). The root node of the subtree is defined by the arguments.
    :param request: The HTTP request
    :param dataset_name: The name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param rank: The name of the rank of the taxon
    :type rank : str
    :param taxon_name: the name of taxon
    :type taxon_name : str
    :return: A dictionary with the following keys :
            - 'text' : A human readable string with a human readable representation of the subtree
            - 'value' : A short unique string concatenating the rank / name of each of the taxons in the subtree
    """
    asv_subtree = {}
    try:
        if dataset_name is None or rank is None or taxon_name is None:
            raise MyGodApiException("get_asv_subtree", f"None argument provided: {dataset_name}, {rank}, {taxon_name}")

        taxons = get_taxon(taxon_name)
        for taxon in taxons:
            sequences = Sequence.objects.filter(
                taxon__taxonomy_linked__dataset__name__iexact=dataset_name).filter(taxon__name__iexact=taxon.name)
            if sequences:
                for sequence in sequences:
                    taxon_name = taxon.rank + " - " + taxon.name
                    if taxon_name not in asv_subtree:
                        asv_subtree[taxon_name] = []
                    asv_subtree[taxon_name].append(
                        {"text": str(sequence.score) + " - " + sequence.amplicon, "value": sequence.amplicon})
    except MyGodApiException as e:
        e.log()
    return JsonResponse(asv_subtree, safe=False)


def get_relative_abundances_for_taxon(request, dataset_name, rank, taxon_name, fraction, parameter, date_start,
                                      date_end,
                                      depth, representation, split_by):
    """
    Return data with the relative abundances of taxons according to search criteria
    Used by the API to plot relative abundances (ScatterPlot in Dashboard) depending on search criteria defined by
    the arguments
    :param request: the HTTP request
    :param dataset_name: Name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param rank: Name of rank of the taxon
    :type rank : str
    :param taxon_name: Name of the taxon
    :type taxon_name : str
    :param fraction: The fraction size
    :type fraction : str
    :param parameter: Name of the parameter
    :type parameter : str
    :param date_start: Start of the date range
    :type date_start  : date
    :param date_end: End of the date range
    :type date_end : date
    :param depth: The depth code
    :type depth : str
    :param representation: Type of data relative abundance or cells count
    :type representation : str
    :param split_by : additional information to split data by depth or year
    :type split_by : str
    :return:  A list of dictionaries sorted in ascending order of the paramvalue key of the data points.
            Each entry matches a single data point and has the following keys :
            {'paramvalue' : The value of the requester parameter (paramname argument) for a data point,
            'taxon_name' : The name of the taxon for a data point,
            'abundance' : The relative abundance of the given taxon for that specific value of the parameter}

    """

    # FIXME : not checked yet.
    # TODO : replace threshold constant value with value defined in dataset metadata.
    res = {}
    data = []
    y_max_value = None
    y_min_value = None
    x_max_value = None
    x_min_value = None
    try:
        if dataset_name is None or rank is None or taxon_name is None or fraction is None or parameter is None or \
                date_start is None or date_end is None or depth is None:
            raise MyGodApiException("get_relative_abundances_for_taxon", "None argument given", logging.ERROR)

        abundances = {}
        custom = False
        try:
            PcReleve._meta.get_field(parameter)
            model_attribute = PARAMETER_MAPPINGS[parameter]['attribute']
        except FieldDoesNotExist:
            model_attribute = 'event__pcreleve__custom_parameters'
            custom = True

        if representation == "taxon":
            cumulative_measurement_name = "cumulative_relative_abundance"
        if representation == "count":
            cumulative_measurement_name = "cumulative_count"
        taxons = get_taxon(taxon_name)
        for taxon in taxons:
            if split_by == "depth":
                measurements = taxon.occurrence_set.filter(
                    cumulatedmeasurement__name=cumulative_measurement_name) \
                    .filter(measure__fraction_size=fraction, from_dataset__name=dataset_name, ) \
                    .values(model_attribute, "event__depth_code") \
                    .filter(event__date__range=(date_start, date_end)) \
                    .annotate(cumulatedmeasurement_value=ArrayAgg("cumulatedmeasurement__value")).distinct()
            else:
                measurements = taxon.occurrence_set.filter(
                    cumulatedmeasurement__name=cumulative_measurement_name) \
                    .filter(measure__fraction_size=fraction, from_dataset__name=dataset_name, event__depth_code=depth) \
                    .values(model_attribute, "event__date", "event__cluster", "event__station_code") \
                    .filter(event__date__range=(date_start, date_end)) \
                    .annotate(cumulatedmeasurement_value=ArrayAgg("cumulatedmeasurement__value")).distinct()
            for measurement in measurements:
                if custom:
                    param_value = measurement[model_attribute][parameter]['value']
                else:
                    param_value = measurement[model_attribute]
                if param_value:
                    if param_value not in abundances:
                        abundances[param_value] = []
                    abundances[param_value].append(measurement['cumulatedmeasurement_value'][0])
                    split_option = "grouped"
                    if split_by == "depth":
                        split_option = measurement['event__depth_code']
                    if split_by == "year":
                        split_option = measurement['event__date'].year
                    if split_by == "cluster":
                        split_option = measurement['event__cluster']
                    if split_by == "station":
                        split_option = measurement['event__station_code']
                    if not any(dictionary.get('group') == split_option for dictionary in data):
                        data.append({'group': split_option, 'values': []})
                    group = next(item for item in data if item["group"] == split_option)
                    if not any(dictionary.get('paramvalue') == param_value for dictionary in group['values']):
                        group['values'].append({'paramvalue': param_value, 'taxon_name': taxon_name, 'abundance': 0})
                    subgroup = next(item for item in group['values'] if item["paramvalue"] == param_value)
                    subgroup['abundance'] += measurement['cumulatedmeasurement_value'][0]
                    y_tmp_value = subgroup['abundance']
                    x_tmp_value = param_value
                    if x_min_value is None or x_min_value > x_tmp_value:
                        x_min_value = x_tmp_value
                    if x_max_value is None or x_max_value < x_tmp_value:
                        x_max_value = x_tmp_value
                    if y_min_value is None or y_min_value > y_tmp_value:
                        y_min_value = y_tmp_value
                    if y_max_value is None or y_max_value < y_tmp_value:
                        y_max_value = y_tmp_value
        if y_min_value is None or y_max_value is None or x_min_value is None or x_max_value is None:
            raise MyGodApiException("get_relative_abundances_for_taxon", f"One of the bound parameters is None",
                                    logging.WARNING)

        if split_by != 'cluster':
            data.sort(key=lambda obj: obj["group"])
        res['data'] = data
        res['domain'] = {'ymin': y_min_value, 'ymax': y_max_value, 'xmin': x_min_value, 'xmax': x_max_value}

    except MyGodApiException as e:
        e.log()

    return JsonResponse(res, safe=False)


def get_relative_abundances_for_asv(request, dataset_name, asv_list, fraction, parameter, date_start, date_end, depth,
                                    split_by):
    """
    Return data with the relative abundances of taxons according to search criteria
    Used by the API to plot relative abundances (ScatterPlot in Dashboard) depending on search criteria defined by
    the arguments
    :param request: The HTTP request
    :param dataset_name: Name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param asv_list: List of amplicon selected
    :type asv_list : json list
    :param fraction: The fraction size
    :type fraction : str
    :param parameter: Name of the parameter
    :type parameter : str
    :param date_start: Start of date range
    :type date_start : str
    :param date_end: End of date range
    :type date_end : str
    :param depth: The depth code
    :type depth : str
    :param split_by : additional information to split data by depth or year
    :type split_by : str
    :return: A list of dictionaries sorted in ascending order of the paramvalue key of the data points. Each entry matches a single data point and has the following keys :
            {'paramvalue' : The value of the requester parameter (paramname argument) for a data point,
            'asv_list' : The names of the amplicons (same as input argument),
            'abundance' : The relative abundance of the amplicons given as arguments}
    """

    res = {}
    data = []
    y_max_value = None
    y_min_value = None
    x_max_value = None
    x_min_value = None
    api_asv_list = json.loads(asv_list)
    try:
        if dataset_name is None or asv_list is None or fraction is None or parameter is None or \
                date_start is None or date_end is None or depth is None:
            raise MyGodApiException("get_relative_abundances_by_asv", "None argument given", logging.ERROR)

        abundances = {}
        custom = False
        try:
            PcReleve._meta.get_field(parameter)
            model_attribute = "from_occurrence__" + PARAMETER_MAPPINGS[parameter]['attribute']
        except FieldDoesNotExist:
            model_attribute = 'from_occurrence__event__pcreleve__custom_parameters'
            custom = True

        model_attribute = "from_occurrence__" + PARAMETER_MAPPINGS[parameter]['attribute']
        for asv in api_asv_list:
            if split_by == "depth":
                measurements = Measure.objects.filter(name="relative_abundance") \
                    .filter(link_sequence__amplicon__iexact=asv,
                            from_occurrence__from_dataset__name__iexact=dataset_name) \
                    .values(model_attribute, "from_occurrence__event__depth_code") \
                    .filter(fraction_size=fraction) \
                    .filter(from_occurrence__event__date__range=(date_start, date_end)) \
                    .annotate(value=ArrayAgg("value"))
            else:
                measurements = Measure.objects.filter(name="relative_abundance") \
                    .filter(link_sequence__amplicon__iexact=asv,
                            from_occurrence__from_dataset__name__iexact=dataset_name) \
                    .values(model_attribute, "from_occurrence__event__date", "from_occurrence__event__cluster",
                            "from_occurrence__event__station_code") \
                    .filter(fraction_size=fraction) \
                    .filter(from_occurrence__event__date__range=(date_start, date_end),
                            from_occurrence__event__depth_code__exact=depth) \
                    .annotate(value=ArrayAgg("value"))

            for measurement in measurements:
                if custom:
                    param_value = measurement[model_attribute][parameter]['value']

                else:
                    param_value = measurement[model_attribute]
                if param_value not in abundances:
                    abundances[param_value] = []
                abundances[param_value].append(measurement['value'][0])
                split_option = "grouped"
                if split_by == "depth":
                    split_option = measurement["from_occurrence__event__depth_code"]
                if split_by == "year":
                    split_option = measurement["from_occurrence__event__date"].year
                if split_by == "cluster":
                    split_option = measurement["from_occurrence__event__cluster"]
                if split_by == "station":
                    split_option = measurement["from_occurrence__event__station_code"]
                if not any(dictionary.get('group') == split_option for dictionary in data):
                    data.append({'group': split_option, 'values': []})
                group = next(item for item in data if item["group"] == split_option)
                param_value = measurement[model_attribute]
                if not any(dictionary.get('paramvalue') == param_value for dictionary in group['values']):
                    group['values'].append({'paramvalue': param_value, 'taxon_name': asv_list, 'abundance': 0})
                subgroup = next(item for item in group['values'] if item["paramvalue"] == param_value)
                subgroup['abundance'] += measurement['value'][0]
                y_tmp_value = subgroup['abundance']
                x_tmp_value = param_value
                if x_min_value is None or x_min_value > x_tmp_value:
                    x_min_value = x_tmp_value
                if x_max_value is None or x_max_value < x_tmp_value:
                    x_max_value = x_tmp_value
                if y_min_value is None or y_min_value > y_tmp_value:
                    y_min_value = y_tmp_value
                if y_max_value is None or y_max_value < y_tmp_value:
                    y_max_value = y_tmp_value
        if y_min_value is None or y_max_value is None or x_min_value is None or x_max_value is None:
            raise MyGodApiException("get_relative_abundances_for_taxon", f"One of the bound parameters is None",
                                    logging.WARNING)

        if split_by != 'cluster':
            data.sort(key=lambda obj: obj["group"])
        res['data'] = data
        res['domain'] = {'ymin': y_min_value, 'ymax': y_max_value, 'xmin': x_min_value, 'xmax': x_max_value}
    except MyGodApiException as e:
        e.log()

    return JsonResponse(res, safe=False)


def get_axis_label(request, dataset_name, parameter):
    """
    Return a label with the unit of a physico-chemical parameter in a specific dataset
    :param request: The HTTP request
    :param dataset_name: Name of the dataset used to determine start and end dates
    :type dataset_name : str
    :param parameter: The name of the physico-chemical parameter
    :type parameter : str
    :return: A JSON formatted dictionary with one key :
            {'label' : the label usable for this parameter in this dataset (or 'Unknown if not applicable)}
    """

    res = {'label': 'Unknown'}
    try:
        if dataset_name is None or parameter is None:
            raise MyGodApiException("get_axis_label", f"None parameter received {dataset_name}, {parameter}",
                                    logging.WARNING)
        dataset = DataSet.objects.get(name=dataset_name)
        if dataset is None:
            raise MyGodApiException("get_axis_label", f"Unknown dataset {dataset_name}", logging.WARNING)

        unit = parameter + "_unit"
        try:
            PcReleve._meta.get_field(parameter)
            unit = parameter + "_unit"
            label = PcReleve.objects.filter(event__occurrence__from_dataset=dataset).values(unit).distinct()
            if len(label) > 0:
                res['label'] = label[0][unit]
        except FieldDoesNotExist:
            custom_fields = PcReleve.objects.filter(event__occurrence__from_dataset=dataset).values(
                "custom_parameters").first()
            res['label'] = custom_fields['custom_parameters'][parameter]['unit']

    except MyGodApiException as e:
        e.log()
    return JsonResponse(res)


def get_discrete_temporal_distribution_for_taxon(request, dataset_name, rank, taxon_name, fraction, date_start,
                                                 date_end,
                                                 depth, group_by, representation, split_by):
    """
    Return data used to build a box plot with the seasonal distribution of abundances
    :param request: the HTTP request
    :param dataset_name: Name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param rank: Name of the rank of the taxon
    :type rank : str
    :param taxon_name: Name of the taxon
    :type taxon_name : str
    :param fraction: the fraction size
    :type fraction : str
    :param date_start: the start of the date range
    :type date_start : date
    :param date_end: the end of the date range
    :type date_end : date
    :param depth: the depth code
    :type depth : str
    :param group_by: name of the parameter to use for building groupings ('season','year','month')
    :type group_by : str
    :param representation: type of data (relative_abundance or cells count for a taxon)
    :type representation : str
    :param split_by : additional information to split data by depth or year
    :type split_by : str
    :return: A list of dictionaries. Each entry matches a single data point and has the following keys :
            {'season' : A string value representing the season ('spring','summer','autumn,'winter'),
            'values' : The cumulative measurements (in %) for the occurrences matching the arguments}
    """
    res = {}
    data = []
    max_value = 0.0

    SEASONS = {
        'winter': 1,
        'spring': 2,
        'summer': 3,
        'autumn': 4,
    }

    MONTHS = {month.lower(): index for index, month in enumerate(calendar.month_abbr) if month}

    def _case_insensitive_string_compare(s1, s2):
        s1 = str(s1)
        s2 = str(s2)
        if s1.lower() < s2.lower():
            return -1
        if s2.lower() < s1.lower():
            return 1
        return 0

    SORTING_FUNCTIONS = {
        'station_code': lambda c1, c2: _case_insensitive_string_compare(c1['subgroup'], c2['subgroup']),
        'season': lambda s1, s2: SEASONS[s1['subgroup'].lower()] - SEASONS[s2['subgroup'].lower()],
        'year': lambda y1, y2: y1['subgroup'] - y2['subgroup'],
        'month': lambda m1, m2: MONTHS[m1['subgroup'].lower()] - MONTHS[m2['subgroup'].lower()],
        'depth_code': lambda d1, d2: _case_insensitive_string_compare(d1['subgroup'], d2['subgroup'])
    }

    try:
        if dataset_name is None or rank is None or taxon_name is None or fraction is None or \
                date_start is None or date_end is None or depth is None:
            raise MyGodApiException("get_discrete_temporal_distribution_for_taxon", "None argument given",
                                    logging.ERROR)

        taxons = get_taxon(taxon_name)
        cumulative_measurement_name = ""
        if representation == "taxon":
            cumulative_measurement_name = "cumulative_relative_abundance"
        if representation == "count":
            cumulative_measurement_name = "cumulative_count"

        for taxon in taxons:
            if split_by == 'depth' or group_by == "depth":
                occurrences = taxon.occurrence_set.filter(
                    cumulatedmeasurement__name=cumulative_measurement_name).filter(
                    event__date__range=(date_start, date_end)).filter(
                    measure__fraction_size=fraction, from_dataset__name=dataset_name).values('event__date',
                                                                                             'event__cluster',
                                                                                             'event__station_code',
                                                                                             'event__depth_code').annotate(
                    measure_pks=ArrayAgg("cumulatedmeasurement__pk"))
            else:
                occurrences = taxon.occurrence_set.filter(
                    cumulatedmeasurement__name=cumulative_measurement_name).filter(
                    event__date__range=(date_start, date_end), event__depth_code=depth).filter(
                    measure__fraction_size=fraction, from_dataset__name=dataset_name).values('event__date',
                                                                                             'event__cluster',
                                                                                             'event__station_code',
                                                                                             'event__depth_code').annotate(
                    measure_pks=ArrayAgg("cumulatedmeasurement__pk"))

            for occurrence in occurrences:
                split_option = "grouped"
                occurrence_date = occurrence['event__date']
                if split_by == "depth":
                    split_option = occurrence['event__depth_code']
                if split_by == "cluster":
                    split_option = occurrence['event__cluster']
                if split_by == "station":
                    split_option = occurrence['event__station_code']
                if split_by == "year":
                    split_option = occurrence_date.year
                if not any(dictionary.get('group') == split_option for dictionary in data):
                    data.append({'group': split_option, 'values': []})
                group = next(item for item in data if item["group"] == split_option)
                measurements = CumulatedMeasurement.objects.filter(pk__in=occurrence['measure_pks']). \
                    filter(fraction_size_cumulated=fraction)
                pattern_group = None
                if group_by == "season":
                    pattern_group = get_season_from_date(occurrence_date)
                if group_by == 'year':
                    pattern_group = occurrence_date.year
                if group_by == 'month':
                    pattern_group = occurrence_date.strftime("%b")
                if group_by == 'station':
                    pattern_group = occurrence['event__station_code']
                if group_by == 'cluster':
                    pattern_group = occurrence['event__cluster']
                if group_by == 'depth':
                    pattern_group = occurrence['event__depth_code']
                tmp_max_value = sum([measurement.value for measurement in measurements])
                if tmp_max_value > max_value:
                    max_value = tmp_max_value
                group['values'].append({
                    'subgroup': pattern_group,
                    'values': sum([measurement.value for measurement in measurements])})

        if split_by != "cluster":
            group['values']=sorted(group['values'], key=functools.cmp_to_key(SORTING_FUNCTIONS[group_by]))

        res['data'] = data
        res['domain'] = {"ymax": max_value}
    except MyGodApiException as e:
        e.log()

    return JsonResponse(res, safe=False)


def get_discrete_temporal_distribution_for_asv(request, dataset_name, asv_list, fraction, date_start, date_end, depth,
                                               group_by, split_by):
    """
    Return data used to build a box plot with the seasonal distribution of abundances.
    :param request: the HTTP request
    :param dataset_name: the name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param asv_list: amplicon id
    :type asv_list : json field
    :param fraction: the fraction size
    :type fraction : str
    :param date_start: the start of the date range
    :type date_start : date
    :param date_end: the end of the date range
    :type ate_end : date
    :param depth: the depth code
    :type depth : str
    :param group_by: name of the parameter to use for building groupings ('season','year','month')
    :type group_by : str
    :param split_by : additional information to split data by depth or year
    :type split_by : str
    :return:  A list of dictionaries. Each entry matches a single data point and has the following keys :
            {'season' : A string value representing the season ('spring','summer','autumn,'winter'),
            'values' : The cumulative measurements (in %) for the occurrences matching the arguments}
    """
    res = {}
    data = []
    api_asv_list = json.loads(asv_list)
    max_value = 0.0

    SEASONS = {
        'winter': 1,
        'spring': 2,
        'summer': 3,
        'autumn': 4,
    }

    MONTHS = {month.lower(): index for index, month in enumerate(calendar.month_abbr) if month}

    def _case_insensitive_string_compare(s1, s2):
        s1 = str(s1)
        s2 = str(s2)
        if s1.lower() < s2.lower():
            return -1
        if s2.lower() < s1.lower():
            return 1
        return 0

    SORTING_FUNCTIONS = {
        'station_code': lambda c1, c2: _case_insensitive_string_compare(c1['subgroup'], c2['subgroup']),
        'season': lambda s1, s2: SEASONS[s1['subgroup'].lower()] - SEASONS[s2['subgroup'].lower()],
        'year': lambda y1, y2: y1['subgroup'] - y2['subgroup'],
        'month': lambda m1, m2: MONTHS[m1['subgroup'].lower()] - MONTHS[m2['subgroup'].lower()],
        'depth_code': lambda d1, d2: _case_insensitive_string_compare(d1['subgroup'], d2['subgroup'])
    }

    try:
        if dataset_name is None or asv_list is None or fraction is None or \
                date_start is None or date_end is None or depth is None:
            raise MyGodApiException("get_seasonal_distribution", "None argument given", logging.ERROR)

        for asv in api_asv_list:
            if split_by == 'depth' or group_by == "depth":
                measurements = Measure.objects.filter(name="relative_abundance") \
                    .filter(link_sequence__amplicon__iexact=asv,
                            from_occurrence__from_dataset__name__iexact=dataset_name) \
                    .values("from_occurrence__event__date", "value", "from_occurrence__event__depth_code",
                            "from_occurrence__event__station_code", "from_occurrence__event__cluster") \
                    .filter(fraction_size=fraction) \
                    .filter(from_occurrence__event__date__range=(date_start, date_end))
            else:
                measurements = Measure.objects.filter(name="relative_abundance") \
                    .filter(link_sequence__amplicon__iexact=asv,
                            from_occurrence__from_dataset__name__iexact=dataset_name) \
                    .values("from_occurrence__event__date", "value", "from_occurrence__event__cluster",
                            "from_occurrence__event__depth_code", "from_occurrence__event__station_code") \
                    .filter(fraction_size=fraction) \
                    .filter(from_occurrence__event__date__range=(date_start, date_end),
                            from_occurrence__event__depth_code__exact=depth)

            group = {}
            for measurement in measurements:
                split_option = "grouped"
                if split_by == "depth":
                    split_option = measurement["from_occurrence__event__depth_code"]
                if split_by == "year":
                    split_option = measurement["from_occurrence__event__date"].year
                if split_by == "cluster":
                    split_option = measurement["from_occurrence__event__cluster"]
                if split_by == "station":
                    split_option = measurement["from_occurrence__event__station_code"]
                if not any(dictionary.get('group') == split_option for dictionary in data):
                    data.append({'group': split_option, 'values': []})
                group = next(item for item in data if item["group"] == split_option)
                event_date = measurement["from_occurrence__event__date"]
                pattern_group = None
                if group_by == "season":
                    pattern_group = get_season_from_date(event_date)
                if group_by == 'year':
                    pattern_group = event_date.year
                if group_by == 'month':
                    pattern_group = event_date.strftime("%b")
                if group_by == 'station':
                    pattern_group = measurement['event__station_code']
                if group_by == 'depth':
                    pattern_group = measurement['event__depth_code']
                if group_by == 'cluster':
                    pattern_group = measurement['event__cluster']
                tmp_max_value = measurement["value"]
                if tmp_max_value > max_value:
                    max_value = tmp_max_value
                group['values'].append({
                    'subgroup': pattern_group,
                    'values': measurement["value"]})

            if split_by != "cluster" and 'values' in group:
                group['values']=sorted(group['values'], key=functools.cmp_to_key(SORTING_FUNCTIONS[group_by]))

        res['data'] = data
        res['domain'] = {"ymax": max_value}

    except MyGodApiException as e:
        e.log()

    return JsonResponse(res, safe=False)


def get_occurrences_by_date_for_taxon(request, dataset_name, rank, taxon_name, fraction, date_start, date_end, depth,
                                      representation, split_by):
    """
    Return data with the relative abundances of taxons  according to search criteria
    Used by the API to plot relative abundances (DateGraph in Dashboard) depending on search criteria defined by
    the arguments
    :param request: the HTTP request
    :param dataset_name: the name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param rank: the name of the rank of the taxon
    :type rank : str
    :param taxon_name: the name of the taxon
    :type taxon_name : str
    :param fraction: the fraction size
    :type fraction : str
    :param date_start: the start of the date range
    :type date_start : date
    :param date_end: the end of the date range
    :type date_end : str
    :param depth: the depth code
    :type depth : str
    :param representation: Type of data, relative abundance or cells count
    :type representation : str
    :param split_by : additional information to split data by depth or year
    :type split_by : str
    :return:  A list of dictionaries sorted in ascending order of the date key of the data points. Each entry matches a single data point and has the following keys :
            {'date' : the date the abundance was measured,
            'name' : The name of a taxon measured at the given date,
            'occurrences' : The relative abundance of the given taxon_name at that specific date}
    """
    res = {}
    data = []
    max_value = 0.0

    try:
        if dataset_name is None or rank is None or taxon_name is None or fraction is None or \
                date_start is None or date_end is None or depth is None:
            raise MyGodApiException("get_occurrences_by_date", "None argument given", logging.ERROR)

        cumulative_measurement_name = ""
        if representation == "taxon":
            cumulative_measurement_name = "cumulative_relative_abundance"
        if representation == "count":
            cumulative_measurement_name = "cumulative_count"

        taxons = get_taxon(taxon_name)

        for taxon in taxons:
            if split_by == "depth":
                occurrences = taxon.occurrence_set \
                    .filter(cumulatedmeasurement__name=cumulative_measurement_name) \
                    .filter(event__date__range=(date_start, date_end), from_dataset__name=dataset_name) \
                    .values('event__date', 'event__depth_code') \
                    .annotate(measure_pks=ArrayAgg("cumulatedmeasurement__pk"))
            else:
                occurrences = taxon.occurrence_set \
                    .filter(cumulatedmeasurement__name=cumulative_measurement_name) \
                    .filter(event__date__range=(date_start, date_end), from_dataset__name=dataset_name,
                            event__depth_code=depth) \
                    .values('event__date', 'event__cluster') \
                    .annotate(measure_pks=ArrayAgg("cumulatedmeasurement__pk"))
            for occurrence in occurrences:
                measurements = CumulatedMeasurement.objects.filter(pk__in=occurrence['measure_pks']).filter(
                    fraction_size_cumulated=fraction)
                event_date = occurrence['event__date']
                split_option = "grouped"
                if split_by == "depth":
                    split_option = occurrence['event__depth_code']
                if split_by == "year":
                    split_option = occurrence['event__date'].year
                if split_by == "cluster":
                    split_option = occurrence['event__cluster']
                if not any(dictionary.get('group') == split_option for dictionary in data):
                    data.append({'group': split_option, 'values': []})
                group = next(item for item in data if item["group"] == split_option)
                if not any(dictionary.get('date') == event_date for dictionary in group['values']):
                    group['values'].append({'date': event_date, 'name': taxon_name, 'occurrences': 0})
                subgroup = next(item for item in group['values'] if item["date"] == event_date)
                subgroup['occurrences'] += sum([measurement.value for measurement in measurements])
                tmp_max_value = subgroup['occurrences']
                if tmp_max_value > max_value:
                    max_value = tmp_max_value
        if split_by != "cluster":
            data.sort(key=lambda obj: obj["group"])
        for group in data:
            group['values'].sort(key=lambda obj: obj["date"])
        res['data'] = data
        res['domain'] = {'ymax': max_value}

    except MyGodApiException as e:
        e.log()

    return JsonResponse(res, safe=False)


def get_occurrences_by_date_for_asv(request, dataset_name, asv_list, fraction, date_start, date_end, depth, split_by):
    """
    Return data with the relative abundances of taxons according to search criteria
    Used by the API to plot relative abundances (DateGraph in Dashboard) depending on search criteria defined by
    the arguments
    :param request: the HTTP request
    :param dataset_name: the name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param asv_list: amplicon id
    :type asv_list : json list
    :param fraction: the fraction size
    :type fraction: str
    :param date_start: the start of the date range
    :type date_start: date
    :param date_end: the end of the date range
    :param depth: the depth code
    :param split_by : additional information to split data by depth or year
    :type split_by : str
    :return: A list of dictionaries is sorted in ascending order of the date key of the data points. Each entry matches
            a single data point and has the following keys :
            {'date' : the date the abundance was measured,
            'name' : The name of a taxon measured at the given date,
            'occurrences' : The relative abundance of the given asv at that specific date}


    """
    res = {}
    data = []
    max_value = 0.0

    try:
        if dataset_name is None or asv_list is None or fraction is None or \
                date_start is None or date_end is None or depth is None:
            raise MyGodApiException("get_occurrences_by_date_for_asv", "None argument given", logging.ERROR)

        api_asv_list = json.loads(asv_list)

        for asv in api_asv_list:
            if split_by == "depth":
                measurements = Measure.objects.filter(name="relative_abundance") \
                    .filter(link_sequence__amplicon__iexact=asv,
                            from_occurrence__from_dataset__name__iexact=dataset_name) \
                    .values("from_occurrence__event__date", "value", "from_occurrence__event__depth_code") \
                    .filter(fraction_size=fraction) \
                    .filter(from_occurrence__event__date__range=(date_start, date_end))
            else:
                measurements = Measure.objects.filter(name="relative_abundance") \
                    .filter(link_sequence__amplicon__iexact=asv,
                            from_occurrence__from_dataset__name__iexact=dataset_name) \
                    .values("from_occurrence__event__date", "value", "from_occurrence__event__cluster") \
                    .filter(fraction_size=fraction) \
                    .filter(from_occurrence__event__date__range=(date_start, date_end),
                            from_occurrence__event__depth_code__exact=depth)

            for measurement in measurements:
                split_option = "grouped"
                if split_by == "depth":
                    split_option = measurement["from_occurrence__event__depth_code"]
                if split_by == "year":
                    split_option = measurement["from_occurrence__event__date"].year
                if split_by == "cluster":
                    split_option = measurement["from_occurrence__event__cluster"]
                if not any(dictionary.get('group') == split_option for dictionary in data):
                    data.append({'group': split_option, 'values': []})
                group = next(item for item in data if item["group"] == split_option)
                if not any(dictionary.get('date') == measurement["from_occurrence__event__date"] for dictionary in
                           group['values']):
                    group['values'].append(
                        {'date': measurement["from_occurrence__event__date"], 'name': 'asv', 'occurrences': 0})
                subgroup = next(
                    item for item in group['values'] if item["date"] == measurement["from_occurrence__event__date"])
                subgroup['occurrences'] += measurement['value']
                tmp_max_value = subgroup['occurrences']
                if tmp_max_value > max_value:
                    max_value = tmp_max_value
            if split_by != "cluster":
                data.sort(key=lambda obj: obj["group"])
            for group in data:
                group['values'].sort(key=lambda obj: obj["date"])
            res['data'] = data
            res['domain'] = {'ymax': max_value}

    except MyGodApiException as e:
        e.log()

    return JsonResponse(res, safe=False)


def get_tree(taxon, fraction, dataset_name, date_start, date_end, depth, dataset_type):
    """
    Return Tree related to Richness plot, created recursively,
    :param taxon: Name of taxon
    :type taxon : objet
    :param fraction: the fraction size selected
    :type fraction : str
    :param dataset_name: the name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param date_start: the start of the date range
    :type date_start : date
    :param date_end: the end of the date range
    :type date_end : date
    :param depth: the depth code selected
    :type depth  :str
    :param dataset_type: the type of data selected
    :type dataset_type : str
    :return: A tree structure (nested dictionaries).
             {'name' : name of root taxon (nodes),
             'value' : number of occurrences for each sequence belonging to rank selected,
             'children' : list of dictionaries containing the same keys as the toplevel dictionary}
    """
    value = 0
    cumulative_measurement_name = ""
    if dataset_type == "metabarcode":
        cumulative_measurement_name = "cumulative_relative_abundance"
        measurements = taxon.sequence_set.filter(measure__name="relative_abundance",
                                                 measure__fraction_size__iexact=fraction,
                                                 taxon__occurrence__event__date__range=(date_start, date_end)) \
            .filter(taxon__occurrence__event__depth_code__iexact=depth) \
            .values("measure__value").distinct()

    if dataset_type == "morphological":
        cumulative_measurement_name = "cumulative_count"
        measurements = taxon.morphobs_set.filter(measure__name="count",
                                                 measure__fraction_size__iexact=fraction,
                                                 taxon__occurrence__event__date__range=(date_start, date_end)) \
            .filter(taxon__occurrence__event__depth_code__iexact=depth) \
            .values("measure__value").distinct()
    for measurement in measurements:
        if len(measurement) > 0:
            value += 1
    if taxon.children.all().exists():
        tree = model_to_dict(taxon, fields=['name'])
        tree['value'] = value
        children = list()
        for child in taxon.children.all():
            child_dict = get_tree(child, fraction, dataset_name, date_start, date_end, depth, dataset_type)
            if child_dict is not None:
                children.append(child_dict)
        tree['children'] = children
        return tree
    else:
        if value > 0:
            tree = model_to_dict(taxon, fields=['name'])
            tree['value'] = value
            return tree
    return


def fix_value_for_none_leaf_taxon(parent):
    """
    Add leaf "Other" for each parent where sum of children doesn't match value
    :param parent: a (nested) dictionary with keys 'name', 'value' and 'children' (cf. get_tree())
    :type parent : dict
    :return: The same tree
    """
    if "children" in parent.keys():
        if parent['value'] > 0:
            parent['children'].append({"name": "Other", "value": parent['value']})
        parent.pop("value")
        for child in parent['children']:
            fix_value_for_none_leaf_taxon(child)
    else:
        return


def get_nested_abundance_for_taxon(request, dataset_name, rank, taxon_name, fraction, date_start, date_end, depth):
    """
    Return nested data with the relative abundances of taxons according to search criteria
    Used by the API to plot relative abundances (DateGraph in Dashboard) depending on search criteria defined by
    the arguments
    :param request: the HTTP request
    :param dataset_name: the name of the dataset used to retrieve available fraction sizes
    :type dataset_name  : str
    :param rank: the name of the rank of the taxon
    :type rank : str
    :param taxon_name: a string composed of the ID of a taxon, a pipe symbol '|' and a taxon name
    :type taxon_name : str
    :param fraction: the fraction size
    :type fraction : str
    :param date_start: the start of the date range
    :type date_start : date
    :param date_end: the end of the date range
    :type date_end : str
    :param depth: selected depth
    :type depth : str
    :param split_by : additional information to split data by depth or year
    :type split_by : str
    :return: A nested dictionary. Each entry matches a single data point and has the following keys :
            {'name' : parent taxon,
            'value' : number of reads associated with taxon,
            'children' : a list of dict with name, value and children as key}
    """

    final_tree = []
    try:
        taxon_name_list = taxon_name.split("|")
        if len(taxon_name_list) != 2:
            raise MyGodApiException("get_nested_abundance_for_taxon", f"Unable to extract taxon id from {taxon_name}.",
                                    logging.ERROR)
        taxon_name = taxon_name_list[1]
        if dataset_name is None or rank is None or taxon_name is None or fraction is None or \
                date_start is None or date_end is None:
            raise MyGodApiException("get_nested_abundance_for_taxon", "None argument given", logging.ERROR)
        dataset = DataSet.objects.get(name__iexact=dataset_name)
        dataset_type = dataset.datatype
        taxon = Taxon.objects.get(name__iexact=taxon_name, taxonomy_linked__dataset__name__iexact=dataset_name)
        final_tree = get_tree(taxon, fraction, dataset_name, date_start, date_end, depth, dataset_type)
        fix_value_for_none_leaf_taxon(final_tree)
    except MyGodApiException as e:
        e.log()
    return JsonResponse(final_tree, safe=False)


def get_grouped_by_depth(event_list, taxon_name_dict, taxons, rank_name_list, date_start, date_end,
                         dataset_name, observation_values, fraction, split_by, abundance_grouped_by_event, data_type):
    """
    Return data with the relative abundances of taxons according to search criteria
    Used get_grouped_abundances (BarChart in Dashboard) depending on search criteria defined by
    the arguments
    :param event_list: list of selected event
    :type event_list: list
    :param taxon_name_dict: A dictionary where taxon name is key and value associated is relative abundance bind (or asv read value)
    :type taxon_name_dict: dict
    :param taxons: Taxon selected
    :type taxons: queryset
    :param rank_name_list: List of subranks of taxon selected
    :type rank_name_list: list
    :param date_start: The start of the date range
    :type date_start: date
    :param date_end: The end of the date range
    :type date_end: date
    :param dataset_name: Name of selected Dataset
    :type dataset_name: str
    :param observation_values: Used by query (amplicon are called if dataset is Metabar type and observator if dataset is Morpho type)
    :type observation_values:str
    :param fraction: Fraction size selected
    :type fraction: str
    :param data_type: Qualitative or quantitative representation
    :type data_type: str
    :param abundance_grouped_by_event: List where for each event associate taxon_name_dict
    :type abundance_grouped_by_event: list
    :param split_by : additional information to split data by depth,year or cluster
    :type split_by : str
    :return:A dictionary. Each entry matches a single depth and has the following keys/values :
            - 'depth' : defined by group_type selected, year is year selected... .
            - 'taxon_name_dict': dictionary where taxon name are associated with their abundances values
    """
    sub_abundance_grouped_by_event = []
    date_list = []
    for event in event_list:
        data_line = {}
        event_group = event.station_code + event.depth_code
        if event_group not in date_list:
            date_list.append(event_group)
            data_line["group"] = event_group  # Rajouter un .year pour transformer en repartition en année
            for taxon in taxon_name_dict:
                data_line[taxon] = 0.0
            sub_abundance_grouped_by_event.append(data_line)
    sub_abundance_grouped_by_event = sorted(sub_abundance_grouped_by_event, key=lambda d: d['group'])

    for taxon in taxons:
        rank_name = None
        taxonomy_from_taxon = taxon.compiled.split("|")
        for taxon_name in taxonomy_from_taxon:
            if taxon_name in rank_name_list:
                rank_name = taxon_name
        if rank_name:
            occurrences = taxon.occurrence_set \
                .filter(measure__name="relative_abundance") \
                .filter(event__date__range=(date_start, date_end), from_dataset__name=dataset_name) \
                .values('event__station_code', "event__depth_code", "event__date") \
                .annotate(measure_pks=ArrayAgg("measure__pk"))
            for occurrence in occurrences:
                measurements = Measure.objects.filter(pk__in=occurrence['measure_pks']).filter(
                    fraction_size=fraction).values("value", observation_values,
                                                   'from_occurrence__event__date', 'from_occurrence__event__cluster',
                                                   'from_occurrence__event__station_code')
                event_place = occurrence['event__station_code'] + occurrence['event__depth_code']
                for measurement in measurements:
                    split_option = "grouped"
                    if split_by == "year":
                        split_option = measurement['from_occurrence__event__date'].year
                    if split_by == "cluster":
                        split_option = measurement['from_occurrence__event__cluster']
                    if split_by == "station":
                        split_option = measurement['from_occurrence__event__station_code']
                    if not any(
                            dictionary.get('group') == split_option for dictionary in
                            abundance_grouped_by_event):
                        abundance_grouped_by_event.append(
                            {'group': split_option, 'values': deepcopy(sub_abundance_grouped_by_event)})
                    subgrouped_abundances = next(
                        item for item in abundance_grouped_by_event if item["group"] == split_option)
                    for event_dict in subgrouped_abundances['values']:
                        if event_place == event_dict['group']:
                            if data_type == "richness":
                                amplicon_list = measurement[observation_values]
                                asv_list = [*set(amplicon_list)]
                                event_dict[rank_name] += len(asv_list)
                            else:
                                event_dict[rank_name] += measurement['value']

    return abundance_grouped_by_event


def get_grouped_by_year(event_list, taxon_name_dict, taxons, rank_name_list, date_start, date_end,
                        dataset_name, observation_values, fraction, split_by, abundance_grouped_by_event, data_type):
    """
    Return data with the relative abundances of taxons according to search criteria
    Used get_grouped_abundances (BarChart in Dashboard) depending on search criteria defined by
    the arguments
    :param event_list: list of selected event
    :type event_list: list
    :param taxon_name_dict: A dictionary where taxon name is key and value associated is relative abundance bind (or asv read value)
    :type taxon_name_dict: dict
    :param taxons: Taxon selected
    :type taxons: queryset
    :param rank_name_list: List of subranks of taxon selected
    :type rank_name_list: list
    :param date_start: The start of the date range
    :type date_start: date
    :param date_end: The end of the date range
    :type date_end: date
    :param dataset_name: Name of selected Dataset
    :type dataset_name: str
    :param observation_values: Used by query (amplicon are called if dataset is Metabar type and observator if dataset is Morpho type)
    :type observation_values:str
    :param fraction: Fraction size selected
    :type fraction: str
    :param data_type: Qualitative or quantitative representation
    :type data_type: str
    :param abundance_grouped_by_event: Dictionary where for each event associate taxon_name_dict
    :type abundance_grouped_by_event: list
    :param split_by : additional information to split data by depth or year
    :type split_by : str
    :return:A dictionary. Each entry matches a single depth and has the following keys/values :
            - 'depth' : defined by group_type selected, year is year selected... .
            - 'taxon_name_dict': dictionary where taxon name are associated with their abundances values
    """
    sub_abundance_grouped_by_event = []
    date_list = []
    for event in event_list:
        data_line = {}
        if event.date.year not in date_list:
            date_list.append(event.date.year)
            data_line["group"] = event.date.year  # Rajouter un .year pour transformer en repartition en année
            for taxon in taxon_name_dict:
                data_line[taxon] = 0.0
            sub_abundance_grouped_by_event.append(data_line)
    sub_abundance_grouped_by_event = sorted(sub_abundance_grouped_by_event, key=lambda d: d['group'])

    for taxon in taxons:
        rank_name = None
        taxonomy_from_taxon = taxon.compiled.split("|")
        for taxon_name in taxonomy_from_taxon:
            if taxon_name in rank_name_list:
                rank_name = taxon_name
        if rank_name:
            occurrences = taxon.occurrence_set \
                .filter(measure__name="relative_abundance") \
                .filter(event__date__range=(date_start, date_end), from_dataset__name=dataset_name) \
                .values('event__date', 'event__depth_code') \
                .annotate(measure_pks=ArrayAgg("measure__pk"))

            for occurrence in occurrences:
                measurements = Measure.objects.filter(pk__in=occurrence['measure_pks']).filter(
                    fraction_size=fraction).values("value", observation_values,
                                                   'from_occurrence__event__depth_code',
                                                   'from_occurrence__event__cluster',
                                                   'from_occurrence__event__station_code')
                event_date = occurrence['event__date']

                for measurement in measurements:
                    split_option = "grouped"
                    if split_by == "depth":
                        split_option = measurement['from_occurrence__event__depth_code']
                    if split_by == "cluster":
                        split_option = measurement['from_occurrence__event__cluster']
                    if split_by == "station":
                        split_option = measurement['from_occurrence__event__station_code']
                    if not any(
                            dictionary.get('group') == split_option for dictionary in
                            abundance_grouped_by_event):
                        abundance_grouped_by_event.append(
                            {'group': split_option, 'values': deepcopy(sub_abundance_grouped_by_event)})
                    subgrouped_abundances = next(
                        item for item in abundance_grouped_by_event if item["group"] == split_option)
                    for event_dict in subgrouped_abundances['values']:
                        if event_date.year == event_dict['group']:
                            if data_type == "richness":
                                if 'asv_list' not in event_dict.keys():
                                    event_dict['asv_list'] = []
                                asv = measurement[observation_values]
                                if asv not in event_dict['asv_list']:
                                    event_dict['asv_list'].append(asv)
                                    event_dict[rank_name] += 1
                            else:
                                event_dict[rank_name] += measurement['value']
    return abundance_grouped_by_event


def get_grouped_by_season(taxon_name_dict, taxons, rank_name_list, date_start, date_end,
                          dataset_name, observation_values, fraction, split_by, abundance_grouped_by_event, data_type):
    """
    Return data with the relative abundances of taxons according to search criteria
    Used get_grouped_abundances (BarChart in Dashboard) depending on search criteria defined by
    the arguments
    :param taxon_name_dict: A dictionary where taxon name is key and value associated is relative abundance bind (or asv read value)
    :type taxon_name_dict: dict
    :param taxons: Taxon selected
    :type taxons: queryset
    :param rank_name_list: List of subranks of taxon selected
    :type rank_name_list: list
    :param date_start: The start of the date range
    :type date_start: date
    :param date_end: The end of the date range
    :type date_end: date
    :param dataset_name: Name of selected Dataset
    :type dataset_name: str
    :param observation_values: Used by query (amplicon are called if dataset is Metabar type and observator if dataset is Morpho type)
    :type observation_values:str
    :param fraction: Fraction size selected
    :type fraction: str
    :param data_type: Qualitative or quantitative representation
    :type data_type: str
    :param abundance_grouped_by_event: List where for each event associate taxon_name_dict
    :type abundance_grouped_by_event: list
    :param split_by : additional information to split data by depth or year
    :type split_by : str
    :return:A dictionary. Each entry matches a single depth and has the following keys/values :
            - 'depth' : defined by group_type selected, year is year selected... .
            - 'taxon_name_dict': dictionary where taxon name are associated with their abundances values
    """
    sub_abundance_grouped_by_event = []
    season_list = ["Winter","Spring", "Summer", "Autumn"]
    for season in season_list:
        data_line = {"group": season}
        for taxon in taxon_name_dict:
            data_line[taxon] = 0.0
        sub_abundance_grouped_by_event.append(data_line)

    for taxon in taxons:
        rank_name = None
        taxonomy_from_taxon = taxon.compiled.split("|")
        for taxon_name in taxonomy_from_taxon:
            if taxon_name in rank_name_list:
                rank_name = taxon_name
        if rank_name:
            occurrences = taxon.occurrence_set \
                .filter(measure__name="relative_abundance") \
                .filter(event__date__range=(date_start, date_end), from_dataset__name=dataset_name) \
                .values('event__date') \
                .annotate(measure_pks=ArrayAgg("measure__pk"))

            for occurrence in occurrences:
                measurements = Measure.objects.filter(pk__in=occurrence['measure_pks']).filter(
                    fraction_size=fraction).values("value", observation_values,
                                                   'from_occurrence__event__depth_code',
                                                   'from_occurrence__event__date', 'from_occurrence__event__cluster',
                                                   'from_occurrence__event__station_code')
                event_date = occurrence['event__date']

                season = get_season_from_date(event_date)
                for measurement in measurements:
                    split_option = "grouped"
                    if split_by == "depth":
                        split_option = measurement['from_occurrence__event__depth_code']
                    if split_by == "year":
                        split_option = measurement['from_occurrence__event__date'].year
                    if split_by == "cluster":
                        split_option = measurement['from_occurrence__event__cluster']
                    if split_by == "station":
                        split_option = measurement['from_occurrence__event__station_code']
                    if not any(
                            dictionary.get('group') == split_option for dictionary in
                            abundance_grouped_by_event):
                        abundance_grouped_by_event.append(
                            {'group': split_option, 'values': deepcopy(sub_abundance_grouped_by_event)})
                    subgrouped_abundances = next(
                        item for item in abundance_grouped_by_event if item["group"] == split_option)
                    for event_dict in subgrouped_abundances['values']:
                        if season == event_dict['group']:
                            if data_type == "richness":
                                if 'asv_list' not in event_dict.keys():
                                    event_dict['asv_list'] = []
                                asv = measurement[observation_values]
                                if asv not in event_dict['asv_list']:
                                    event_dict['asv_list'].append(asv)
                                    event_dict[rank_name] += 1
                            else:
                                event_dict[rank_name] += measurement['value']
    return abundance_grouped_by_event


def get_grouped_by_sample(event_list, taxon_name_dict, taxons, rank_name_list, date_start, date_end,
                          dataset_name, observation_values, fraction, split_by, abundance_grouped_by_event, data_type):
    """
    Return data with the relative abundances of taxons according to search criteria
    Used get_grouped_abundances (BarChart in Dashboard) depending on search criteria defined by
    the arguments
    :param event_list: list of selected event
    :type event_list: list
    :param taxon_name_dict: A dictionary where taxon name is key and value associated is relative abundance bind (or asv read value)
    :type taxon_name_dict: dict
    :param taxons: Taxon selected
    :type taxons: queryset
    :param rank_name_list: List of subranks of taxon selected
    :type rank_name_list: list
    :param date_start: The start of the date range
    :type date_start: date
    :param date_end: The end of the date range
    :type date_end: date
    :param dataset_name: Name of selected Dataset
    :type dataset_name: str
    :param observation_values: Used by query (amplicon are called if dataset is Metabar type and observator if dataset is Morpho type)
    :type observation_values:str
    :param fraction: Fraction size selected
    :type fraction: str
    :param data_type: Qualitative or quantitative representation
    :type data_type: str
    :param abundance_grouped_by_event: List where for each event associate taxon_name_dict
    :type abundance_grouped_by_event: list
    :param split_by : additional information to split data by depth or year
    :type split_by : str
    :return:A dictionary. Each entry matches a single depth and has the following keys/values :
            - 'depth' : defined by group_type selected, year is year selected... .
            - 'taxon_name_dict': dictionary where taxon name are associated with their abundances values
    """
    sub_abundance_grouped_by_event = []
    date_list = []
    for event in event_list:
        data_line = {}
        event_group = getattr(event, "event_id")
        if event_group not in date_list:
            date_list.append(event_group)
            data_line["group"] = event_group
            for taxon in taxon_name_dict:
                data_line[taxon] = 0.0
            sub_abundance_grouped_by_event.append(data_line)
    sub_abundance_grouped_by_event = sorted(sub_abundance_grouped_by_event, key=lambda d: d['group'])

    for taxon in taxons:
        rank_name = None
        taxonomy_from_taxon = taxon.compiled.split("|")
        for taxon_name in taxonomy_from_taxon:
            if taxon_name in rank_name_list:
                rank_name = taxon_name
        if rank_name:
            asv_dict = {}
            occurrences = taxon.occurrence_set \
                .filter(measure__name="relative_abundance") \
                .filter(event__date__range=(date_start, date_end), from_dataset__name=dataset_name) \
                .values("event__date", "event__event_id") \
                .annotate(measure_pks=ArrayAgg("measure__pk"))

            for occurrence in occurrences:
                measurements = Measure.objects.filter(pk__in=occurrence['measure_pks']).filter(
                    fraction_size=fraction).values("value", observation_values,
                                                   'from_occurrence__event__depth_code',
                                                   'from_occurrence__event__date', 'from_occurrence__event__event_id',
                                                   'from_occurrence__event__cluster',
                                                   'from_occurrence__event__station_code')
                event_group_parameter = occurrence["event__event_id"]
                for measurement in measurements:
                    split_option = "grouped"
                    if split_by == "depth":
                        split_option = measurement['from_occurrence__event__depth_code']
                    if split_by == "year":
                        split_option = measurement['from_occurrence__event__date'].year
                    if split_by == "cluster":
                        split_option = measurement['from_occurrence__event__cluster']
                    if split_by == "station":
                        split_option = measurement['from_occurrence__event__station_code']
                    if not any(dictionary.get('group') == split_option for dictionary in
                               abundance_grouped_by_event):
                        abundance_grouped_by_event.append({'group': split_option, 'values': []})
                    subgrouped_abundances = next(
                        item for item in abundance_grouped_by_event if item["group"] == split_option)
                    if not any(dictionary.get('group') == measurement['from_occurrence__event__event_id'] for
                               dictionary in subgrouped_abundances['values']):
                        sub_abundance_for_date = next((item for item in sub_abundance_grouped_by_event if
                                                       item['group'] == measurement[
                                                           'from_occurrence__event__event_id']), None)
                        subgrouped_abundances['values'].append(sub_abundance_for_date)
                    subgroup = next(item for item in subgrouped_abundances['values'] if
                                    item["group"] == measurement['from_occurrence__event__event_id'])
                    if event_group_parameter == subgroup['group']:
                        if event_group_parameter not in asv_dict.keys():
                            asv_dict[event_group_parameter] = []
                        if data_type == "richness":
                            amplicon_list = measurement[observation_values]
                            tmp_asv_list = [*set(amplicon_list)]
                            for tmp_asv in tmp_asv_list:
                                if tmp_asv not in asv_dict[event_group_parameter]:
                                    asv_dict[event_group_parameter].append(tmp_asv)
                                    subgroup[rank_name] += 1
                        else:
                            subgroup[rank_name] += measurement['value']
    return abundance_grouped_by_event


def get_grouped_by_station(event_list, taxon_name_dict, taxons, rank_name_list, date_start, date_end,
                           dataset_name, observation_values, fraction, split_by, abundance_grouped_by_event, data_type):
    """
    Return data with the relative abundances of taxons according to search criteria
    Used get_grouped_abundances (BarChart in Dashboard) depending on search criteria defined by
    the arguments
    :param event_list: list of selected event
    :type event_list: list
    :param taxon_name_dict: A dictionary where taxon name is key and value associated is relative abundance bind (or asv read value)
    :type taxon_name_dict: dict
    :param taxons: Taxon selected
    :type taxons: queryset
    :param rank_name_list: List of subranks of taxon selected
    :type rank_name_list: list
    :param date_start: The start of the date range
    :type date_start: date
    :param date_end: The end of the date range
    :type date_end: date
    :param dataset_name: Name of selected Dataset
    :type dataset_name: str
    :param observation_values: Used by query (amplicon are called if dataset is Metabar type and observator if dataset is Morpho type)
    :type observation_values:str
    :param fraction: Fraction size selected
    :type fraction: str
    :param data_type: Qualitative or quantitative representation
    :type data_type: str
    :param abundance_grouped_by_event: List where for each event associate taxon_name_dict
    :type abundance_grouped_by_event: list
    :param split_by : additional information to split data by depth or year
    :type split_by : str
    :return:A dictionary. Each entry matches a single depth and has the following keys/values :
            - 'depth' : defined by group_type selected, year is year selected... .
            - 'taxon_name_dict': dictionary where taxon name are associated with their abundances values
    """
    sub_abundance_grouped_by_event = []

    date_list = []
    for event in event_list:
        data_line = {}
        event_group = getattr(event, "station_code")
        if event_group not in date_list:
            date_list.append(event_group)
            data_line["group"] = event_group  # Rajouter un .year pour transformer en repartition en année
            for taxon in taxon_name_dict:
                data_line[taxon] = 0.0
            sub_abundance_grouped_by_event.append(data_line)
    sub_abundance_grouped_by_event = sorted(sub_abundance_grouped_by_event, key=lambda d: d['group'])

    for taxon in taxons:
        rank_name = None
        taxonomy_from_taxon = taxon.compiled.split("|")
        for taxon_name in taxonomy_from_taxon:
            if taxon_name in rank_name_list:
                rank_name = taxon_name
        if rank_name:
            occurrences = taxon.occurrence_set \
                .filter(measure__name="relative_abundance") \
                .filter(event__date__range=(date_start, date_end), from_dataset__name=dataset_name) \
                .values("event__station_code") \
                .annotate(measure_pks=ArrayAgg("measure__pk"))

            for occurrence in occurrences:
                measurements = Measure.objects.filter(pk__in=occurrence['measure_pks']).filter(
                    fraction_size=fraction).values("value", observation_values,
                                                   'from_occurrence__event__depth_code',
                                                   'from_occurrence__event__date', 'from_occurrence__event__cluster')
                event_group_parameter = occurrence["event__station_code"]
                for measurement in measurements:
                    split_option = "grouped"
                    if split_by == "depth":
                        split_option = measurement['from_occurrence__event__depth_code']
                    if split_by == "year":
                        split_option = measurement['from_occurrence__event__date'].year
                    if split_by == "cluster":
                        split_option = measurement['from_occurrence__event__cluster']
                    if not any(
                            dictionary.get('group') == split_option for dictionary in
                            abundance_grouped_by_event):
                        abundance_grouped_by_event.append(
                            {'group': split_option, 'values': deepcopy(sub_abundance_grouped_by_event)})
                    subgrouped_abundances = next(
                        item for item in abundance_grouped_by_event if item["group"] == split_option)
                    for event_dict in subgrouped_abundances['values']:
                        if event_group_parameter == event_dict['group']:
                            if data_type == "richness":
                                if 'asv_list' not in event_dict.keys():
                                    event_dict['asv_list'] = []
                                asv = measurement[observation_values]
                                if asv not in event_dict['asv_list']:
                                    event_dict['asv_list'].append(asv)
                                    event_dict[rank_name] += 1
                            else:
                                event_dict[rank_name] += measurement['value']

    return abundance_grouped_by_event


def get_grouped_abundances_core(dataset_name, rank, taxon_name, fraction, date_start, date_end, group_by, sublevel,
                                data_type, number_displayed, split_by):
    """
    Return data with the relative abundances of taxons according to search criteria
    Used by the API to plot relative abundances (BarChart in Dashboard) depending on search criteria defined by
    the arguments

    :param dataset_name: the name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param rank: the name of the rank of the taxon
    :type rank : str
    :param taxon_name: the name of the taxon
    :type taxon_name : str
    :param fraction: the fraction size
    :type fraction : str
    :param date_start: the start of the date range
    :type date_start : date
    :param date_end: the end of the date range
    :type date_end : str
    :param group_by: grouping type for result data ("sampling", "season", "year", ...)
    :type group_by : str
    :param sublevel: name of the level in the taxonomic tree
    :type sublevel: str
    :param data_type: qualitative or quantitative representation
    type data_type : str
    :param number_displayed: number of groups in which to bin the data
    :type number_displayed : int
    :return: A list of dictionaries. Each entry matches a single data point and has the following keys :
                {'group' : defined by group_type selected, year is year selected...,
                'category_name': abundance bind to taxon_name}
    """
    res = {}
    try:
        if dataset_name is None or rank is None or taxon_name is None or fraction is None or \
                date_start is None or date_end is None or group_by is None or sublevel is None or data_type is None:
            raise MyGodApiException("get_grouped_abundance", "None argument given", logging.ERROR)
        abundance_grouped_by_event = []
        rank_name_list = []
        taxon_name_dict = {}
        dataset = DataSet.objects.get(name__iexact=dataset_name)
        if dataset.datatype == "metabarcode":
            observation_values = "link_sequence__amplicon"
        if dataset.datatype == "morphological":
            observation_values = "link_morpho__observator"
        taxons = get_taxon(taxon_name)
        name_list = taxon_name.split("|")
        pk = name_list[0]
        taxon = Taxon.objects.get(pk=pk)
        for rank_child in taxon.children_list:
            if rank_child['rank_name'] == sublevel:
                taxon_list = Taxon.objects.filter(pk__in=rank_child['taxon_id'])
                for taxon in taxon_list:
                    taxon_name_dict[taxon.name] = 0.0
                    rank_name_list.append(taxon.name)
        event_list = list(Event.objects.filter(occurrence__from_dataset__name__iexact=dataset_name,
                                               date__range=(date_start, date_end)).distinct())

        if group_by == "depth":
            abundance_grouped_by_event = get_grouped_by_depth(event_list, taxon_name_dict, taxons,
                                                              rank_name_list,
                                                              date_start, date_end,
                                                              dataset_name, observation_values, fraction, split_by,
                                                              abundance_grouped_by_event, data_type)

        elif group_by == "year":
            abundance_grouped_by_event = get_grouped_by_year(event_list, taxon_name_dict, taxons,
                                                             rank_name_list,
                                                             date_start, date_end,
                                                             dataset_name, observation_values, fraction, split_by,
                                                             abundance_grouped_by_event, data_type)

        if group_by == "season":
            abundance_grouped_by_event = get_grouped_by_season(taxon_name_dict, taxons,
                                                               rank_name_list,
                                                               date_start, date_end,
                                                               dataset_name, observation_values, fraction, split_by,
                                                               abundance_grouped_by_event, data_type)

        if group_by == "sample":
            abundance_grouped_by_event = get_grouped_by_sample(event_list, taxon_name_dict, taxons,
                                                               rank_name_list,
                                                               date_start, date_end,
                                                               dataset_name, observation_values, fraction, split_by,
                                                               abundance_grouped_by_event, data_type)

        if group_by == "station":
            abundance_grouped_by_event = get_grouped_by_station(event_list, taxon_name_dict, taxons,
                                                                rank_name_list,
                                                                date_start, date_end,
                                                                dataset_name, observation_values, fraction, split_by,
                                                                abundance_grouped_by_event, data_type)

        res['data'] = abundance_grouped_by_event
        ranking_dict = {}
        ranked_list = []
        ranking_list = []
        final_ranking_list = []
        abundance_grouped_by_event_copy = deepcopy(abundance_grouped_by_event)

        if len(list(taxon_name_dict.keys())) <= int(number_displayed):
            res['columns'] = list(taxon_name_dict.keys())
        else:
            for taxon_by_order in abundance_grouped_by_event_copy:
                for sample in taxon_by_order['values']:
                    sample.pop("group")
                    if 'asv_list' in sample.keys():
                        del sample["asv_list"]
                    for taxon, value in sample.items():
                        if taxon not in ranking_dict:
                            ranking_dict[taxon] = 0
                        float(value)
                        ranking_dict[taxon] += value
                if len(ranking_dict) > int(number_displayed):
                    to_pop = len(ranking_dict) - int(number_displayed) + 1
                    sorted_ranking_list = sorted(ranking_dict.items(), key=lambda x: x[1], reverse=True)
                    sorted_ranking_dict = dict(sorted_ranking_list)
                    for number in range(1, to_pop):
                        sorted_ranking_dict.popitem()
                    ranked_list = list(sorted_ranking_dict.keys())
                    ranked_list.append('Other')
                ranking_list.append({"group": taxon_by_order['group'], "ranking": ranked_list})
                final_ranking_list.extend(ranked_list)
            res['columns'] = [*set(final_ranking_list)]

            if len(ranking_dict) > int(number_displayed):
                for grouped_abundance in res["data"]:
                    for sample in grouped_abundance['values']:
                        sample_copy = sample.copy()
                        if 'asv_list' in sample_copy.keys():
                            del sample_copy["asv_list"]
                        sample['Other'] = 0
                        for taxon, value in sample_copy.items():
                            if taxon != "group":
                                if taxon not in [*set(final_ranking_list)]:
                                    del sample[taxon]
                                    float(value)
                                    sample['Other'] += value
    except MyGodApiException as e:
        e.log()
    return res


def get_grouped_abundances_api(request, dataset_name, rank, taxon_name, fraction, date_start, date_end, group_by,
                               sublevel,
                               data_type, number_displayed, split_by):
    """
    Return data with the relative abundances of taxons according to search criteria
    Used by the API to plot relative abundances (BarChart in Dashboard) depending on search criteria defined by
    the arguments

    :param request:the HTTP request
    :param dataset_name: the name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param rank: the name of the rank of the taxon
    :type rank : str
    :param taxon_name: the name of the taxon
    :type taxon_name : str
    :param fraction: the fraction size
    :type fraction : str
    :param date_start: the start of the date range
    :type date_start : date
    :param date_end: the end of the date range
    :type date_end : str
    :param group_by: grouping type for result data ("sampling", "season", "year", ...)
    :type group_by : str
    :param sublevel: name of the level in the taxonomic tree
    :type sublevel: str
    :param data_type: qualitative or quantitative representation
    type data_type : str
    :param number_displayed: number of groups in which to bin the data
    :type number_displayed : int
    :return: A list of dictionaries. Each entry matches a single data point and has the following keys :
                {'group' : defined by group_type selected, year is year selected...,
                'category_name': abundance bind to taxon_name}
    """
    res = {}
    try:
        if dataset_name is None or rank is None or taxon_name is None or fraction is None or \
                date_start is None or date_end is None or group_by is None or sublevel is None or data_type is None:
            raise MyGodApiException("get_grouped_abundance", "None argument given", logging.ERROR)
        res = get_grouped_abundances_core(dataset_name, rank, taxon_name, fraction, date_start, date_end, group_by,
                                          sublevel,
                                          data_type, number_displayed, split_by)

    except MyGodApiException as e:
        e.log()
    return JsonResponse(res, safe=False)


def get_station_reference_coordinates(request, dataset_name):
    """Return data with the coordinate associated to sampling station

    Used by the API to display marker on map (MapWidget in Dashboard) depending on search criteria defined by
    the arguments.

    :param request: the HTTP request
    :param dataset_name: the name of the dataset
    :type dataset_name: str
    :return: {'station_name': the name of the station,
                'coordinates': a (lat, long) tuple with the station's coordinates}
    """
    res = []
    try:
        if dataset_name is None:
            raise MyGodApiException("get_station_reference_coordinates", "None argument given", logging.ERROR)

        event_list = Event.objects.filter(bind_dataset__name__iexact=dataset_name).distinct()
        coordinate_dict = {}
        for event in event_list:
            if not event.station_code in coordinate_dict:
                coordinate_dict[event.station_code] = {'latitude': [], 'longitude': []}
            longitude = event.longitude
            coordinate_dict[event.station_code]['latitude'].append(event.latitude)
            coordinate_dict[event.station_code]['longitude'].append(longitude)
        for station, coordinate in coordinate_dict.items():
            res.append({'station_name': station,
                        'coordinate': [sum(coordinate['latitude']) / len(coordinate['latitude']),
                                       sum(coordinate['longitude']) / len(coordinate['longitude'])]})
    except MyGodApiException as e:
        e.log()
    return JsonResponse(res, safe=False)


def get_abundances_with_coordinates(request, dataset_name, rank, taxon_name, fraction, depth, date_start, date_end):
    """
    Return data with the relative abundances of taxons according to search criteria
    Used by the API to display marker on map (MapWidget in Dashboard) depending on search criteria defined by
    the arguments
    :param request: the HTTP request
    :param dataset_name: the name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param rank: the name of the rank of the taxon
    :type rank : str
    :param taxon_name: the name of the taxon
    :type taxon_name : str
    :param fraction: the fraction size
    :type fraction : str
    :param depth: list of all depth from dataset
    :type depth : json list
    :param date_start: the start of the date range
    :type date_start : date
    :param date_end: the end of the date range
    :type date_end: date
    :return: A list of dictionaries. Each entry matches a single data point and has the following keys :
            {'center' : coordinate of station [lat,long],
            'radius' : marker size (int),
            'color' : marker color (string or hex-RGB),
            'text' : marker tooltip contents}
    """

    relative_abundance_by_station = []
    color_palette = settings.COLOR_PALETTE

    try:
        if dataset_name is None or rank is None or taxon_name is None or fraction is None or \
                date_start is None or date_end is None or depth is None:
            raise MyGodApiException("get_abundance_by_coordinate", "None argument given", logging.ERROR)
        depth_list = json.loads(depth)
        taxons = get_taxon(taxon_name)
        cumulative_measurement_name = "cumulative_relative_abundance"
        res = {}
        min_log_value = 0

        for taxon in taxons:
            occurrences = taxon.occurrence_set \
                .filter(cumulatedmeasurement__name=cumulative_measurement_name) \
                .filter(event__date__range=(date_start, date_end)) \
                .values('event__latitude', 'event__longitude', 'event__depth_code', 'event__date') \
                .annotate(measure_pks=ArrayAgg("cumulatedmeasurement__pk"))
            for occurrence in occurrences:
                for depth_code in depth_list:
                    depth = depth_code['depth']
                    if not any(d == depth for d in res):
                        res[depth] = []
                    if occurrence['event__depth_code'] == depth_code['depth']:
                        measurements = CumulatedMeasurement.objects.filter(pk__in=occurrence['measure_pks']).filter(
                            fraction_size_cumulated=fraction)
                        latitude = occurrence['event__latitude']
                        longitude = occurrence['event__longitude']
                        date = occurrence['event__date']
                        value = sum([measurement.value for measurement in measurements])
                        if value > 0:
                            log_value = abs(math.log(value))
                            if value < 1:
                                if min_log_value > log_value:
                                    min_log_value = log_value
                            if not any(d['center'] == [latitude, longitude] and d['date'] == date for d in
                                       res[depth]):
                                res[depth].append(
                                    {'date': date, 'center': [latitude, longitude], "radius": log_value, "value": value,
                                     "text": "Taxon: " + taxon_name})
                            else:
                                for circle in res[depth]:
                                    if circle['date'] == date and circle['center'] == [latitude, longitude]:
                                        circle['radius'] += log_value
                                        circle['value'] += value
        current_color_palette = color_palette.copy()
        for depth_value, coordinate in res.items():
            color = random.choice(current_color_palette)
            current_color_palette.remove(color)
            for sample_circle in coordinate:
                sample_circle['radius'] += min_log_value
            relative_abundance_by_station.append({"depth": depth_value, "coordinate": coordinate, "color": color})
    except MyGodApiException as e:
        e.log()
    return JsonResponse(relative_abundance_by_station, safe=False)


def get_physico_chemical_repartition(request, dataset_name, date_start, date_end, pc_parameter, group_by, depth,
                                     split_by):
    """
    Return data with the physico-chemical values bind to dataset according to search criteria
    Used by the API to dcreate plot (PcRepartition) depending on search criteria defined by
    the arguments
    :param request: the HTTP request
    :param dataset_name: the name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param date_start: the start of the date range
    :type date_start : date
    :param date_end: the end of the date range
    :type date_end : date
    :param pc_parameter: the name of the physico-chemical parameter
    :type pc_parameter : str
    :param group_by: name of the parameter to use for building groupings ('station','season','year','month','depth')
    :type group_by : str
    :param depth: selected depth
    :type depth  : str
    :return: A list of dictionaries. Each entry matches a single data of representation selected
    """

    GROUPING_ATTRBUTES = {
        'station_code': 'station_code',
        'season': 'date',
        'year': 'date',
        'month': 'date',
        'depth_code': 'depth_code'
    }

    SEASONS = {
        'winter': 1,
        'spring': 2,
        'summer': 3,
        'autumn': 4,
    }

    MONTHS = {month.lower(): index for index, month in enumerate(calendar.month_abbr) if month}

    def _case_insensitive_string_compare(s1, s2):
        s1 = str(s1)
        s2 = str(s2)
        if s1.lower() < s2.lower():
            return -1
        if s2.lower() < s1.lower():
            return 1
        return 0

    SORTING_FUNCTIONS = {
        'station_code': lambda c1, c2: _case_insensitive_string_compare(c1['group'], c2['group']),
        'season': lambda s1, s2: SEASONS[s1['group'].lower()] - SEASONS[s2['group'].lower()],
        'year': lambda y1, y2: y1['group'] - y2['group'],
        'month': lambda m1, m2: MONTHS[m1['group'].lower()] - MONTHS[m2['group'].lower()],
        'depth_code': lambda d1, d2: _case_insensitive_string_compare(d1['group'], d2['group'])
    }

    res = {}
    data = []
    max_value = 0.0
    min_value = None
    try:
        if dataset_name is None or \
                date_start is None or date_end is None:
            raise MyGodApiException("get_physico_chemical_repartition", "None argument given", logging.ERROR)
        if group_by not in GROUPING_ATTRBUTES:
            raise MyGodApiException("get_physico_chemical_repartition", f"Unrecognized grouping parameter {group_by}.",
                                    logging.ERROR)

        event_parameter = "event__" + GROUPING_ATTRBUTES[group_by]
        try:
            PcReleve._meta.get_field(pc_parameter)
            parameter_selected = pc_parameter
        except FieldDoesNotExist:
            parameter_selected = "custom_parameters"

        if split_by == "depth":
            pc_releve_list = PcReleve.objects.filter(event__bind_dataset__name__iexact=dataset_name,
                                                     event__date__range=(date_start, date_end)).values(
                parameter_selected,
                event_parameter,
                "event__depth_code")
        else:

            pc_releve_list = PcReleve.objects.filter(event__bind_dataset__name__iexact=dataset_name,
                                                     event__date__range=(date_start, date_end),
                                                     event__depth_code__iexact=depth).values(parameter_selected,
                                                                                             event_parameter,
                                                                                             "event__date",
                                                                                             "event__cluster",
                                                                                             "event__station_code")

        for pc_releve in pc_releve_list:
            if parameter_selected == "custom_parameters":
                value = float(pc_releve["custom_parameters"][pc_parameter]['value'])

            else:
                value = pc_releve[pc_parameter]
            split_option = "grouped"
            if split_by == "depth":
                split_option = pc_releve['event__depth_code']
            if split_by == "year":
                split_option = pc_releve['event__date'].year
            if split_by == "cluster":
                split_option = pc_releve['event__cluster']
            if split_by == "station":
                split_option = pc_releve['event__station_code']
            if not any(dictionary.get('group') == split_option for dictionary in data):
                data.append({'group': split_option, 'values': []})
            group = next(item for item in data if item["group"] == split_option)
            data_line = {'values': value}
            tmp_value = value
            if tmp_value:
                if not min_value:
                    min_value = tmp_value
                if tmp_value > max_value:
                    max_value = tmp_value
                if tmp_value < min_value:
                    min_value = tmp_value
            if group_by == "season":
                data_line['group'] = get_season_from_date(pc_releve[event_parameter])
            if group_by == "station_code" or group_by == "depth_code":
                data_line['group'] = pc_releve[event_parameter]
            if group_by == 'year':
                event = pc_releve[event_parameter]
                data_line['group'] = event.year
            if group_by == 'month':
                data_line['group'] = pc_releve[event_parameter].strftime("%b")
            group['values'].append(data_line)

        # Check if all group values can be converted to integers before sorting.
        num_ok = True
        for group in data:
            for data_line in group['values']:
                try:
                    num_group = int(data_line['group'])
                except ValueError:
                    num_ok = False
                    logger.debug("Pc Releve: not all groups can be converted to int. Keeping strings.")
                    break

        if num_ok is True:
            logger.debug("Pc Releve: all groups can be converted to int.")

        for group in data:
            if num_ok is True:
                for data_line in group['values']:
                    data_line['group'] = int(data_line['group'])
            group['values']=sorted(group['values'], key=functools.cmp_to_key(SORTING_FUNCTIONS[group_by]))

        res['data'] = data
        res['domain'] = {'ymin': min_value, 'ymax': max_value}

    except MyGodApiException as e:
        e.log()
    return JsonResponse(res, safe=False)


def get_beta_diversity_dissimilarity(request, dataset_name, rank, taxon_name, fraction, date_start, date_end,
                                     dissimilarity_type):
    """
    Return data with the NMDS result of matrix distance computation using bray_curtis or Jaccard dissimilarity of taxons according to search criteria
    Used by the API to display NMDS plot (BetaDiversityDissimilarity) depending on search criteria defined by
    the arguments
    :param request: the HTTP request
    :param dataset_name: the name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param rank: the name of the rank of the taxon
    :type rank : str
    :param taxon_name: the name of the taxon
    :type taxon_name : str
    :param fraction: the fraction size
    :type fraction : str
    :param date_start: the start of the date range
    :type date_start : date
    :param date_end: the end of the date range
    :type date_end : date
    :param dissimilarity_type: Jaccard or Bray-Curtis
    :type dissimilarity_type : str
    :return: A dictionary of dictionaries. Each entry matches a single sampling point and has the following keys :
            {'NMDS1': nmds2 value,
              'NMDS2': nmds2 value,
            'min_frac_size': fraction size,
            'depth' : sampling depth
            'station' : sampling station
            'season' : date is converted to associated season}
    """

    res = {}

    _init_r_libs()

    try:
        if dataset_name is None or rank is None or taxon_name is None or fraction is None or \
                date_start is None or date_end is None:
            raise MyGodApiException("get_beta_diversity_dissimilarity", "None argument given", logging.ERROR)
        asv_dict = {'ASV_ID': []}
        sample_table = {"sample_id": [], 'min_frac_size': [], 'station': [], 'depth': [], "season": [], "event_id": []}
        amplicon_by_index = {}
        max_index = 0
        event_names = []
        measures_list = []
        fraction = json.loads(fraction)
        taxons = get_taxon(taxon_name)
        if dissimilarity_type == "jaccard":
            measure_type = "rarefied_asv"
        else:
            measure_type = "asv"
        dataset_type = DataSet.objects.get(name__iexact=dataset_name)
        if dataset_type.datatype == "metabarcode":
            query_observation_sequence = "link_sequence__amplicon"
        if dataset_type.datatype == "morphological":
            query_observation_sequence = "link_morpho__observator"
        for taxon in taxons:
            if dataset_type.datatype == "metabarcode":
                measures = Measure.objects.filter(link_sequence__taxon__name__iexact=taxon.name,
                                                  from_occurrence__from_dataset__name__iexact=dataset_name).filter(
                    name__iexact=measure_type, from_occurrence__event__date__range=(date_start, date_end)).values(
                    "value",
                    "link_sequence__amplicon",
                    "fraction_size",
                    "from_occurrence__event__event_id")
            if dataset_type.datatype == "morphological":
                measures = Measure.objects.filter(link_morpho__taxon__name__iexact=taxon.name,
                                                  from_occurrence__from_dataset__name__iexact=dataset_name).filter(
                    name__iexact=measure_type, from_occurrence__event__date__range=(date_start, date_end)).values(
                    "value",
                    "link_morpho__observator",
                    "fraction_size",
                    "from_occurrence__event__event_id")

            measures_list.extend(measures)
        for measure in measures_list:
            name_event = measure["from_occurrence__event__event_id"] + "_" + measure["fraction_size"]
            if name_event not in sample_table["sample_id"]:
                sample_table["sample_id"].append(
                    name_event)
                event = Event.objects.get(event_id__iexact=measure["from_occurrence__event__event_id"])
                sample_table['min_frac_size'].append(measure["fraction_size"])
                sample_table['station'].append(event.station_code)
                sample_table['depth'].append(event.depth_code)
                season = get_season_from_date(event.date)
                sample_table['season'].append(season)
                sample_table['event_id'].append(measure["from_occurrence__event__event_id"])
            amplicon = measure[query_observation_sequence]
            if amplicon not in amplicon_by_index:
                asv_dict['ASV_ID'].append(amplicon)
                amplicon_by_index[amplicon] = max_index
                max_index += 1
            event_names.append(name_event)
        for event_name in event_names:
            event_values = max_index * [0.0]
            asv_dict[event_name] = event_values
        for measure in measures_list:
            amplicon = measure[query_observation_sequence]
            index = amplicon_by_index[amplicon]
            name_event = measure["from_occurrence__event__event_id"] + "_" + measure["fraction_size"]
            asv_dict[name_event][index] = measure['value']
        r_asv_table = pd.DataFrame(asv_dict)
        r_sample_table = pd.DataFrame(sample_table)
        with localconverter(ro.default_converter + pandas2ri.converter):
            r_dataframe_asv = ro.conversion.py2rpy(r_asv_table)
            r_dataframe_sample = ro.conversion.py2rpy(r_sample_table)
        bray_curtis_nmds_result = R_BETA_DIVERSITY_FUNC.beta_diversity_distance_matrix(r_dataframe_asv,
                                                                                       r_dataframe_sample,
                                                                                       fraction, dissimilarity_type)
        with localconverter(ro.default_converter + pandas2ri.converter):
            bray_curtis_nmds_result_convert = ro.conversion.rpy2py(bray_curtis_nmds_result[0])
            stress = ro.conversion.rpy2py(bray_curtis_nmds_result[1])
        res['nmds_result'] = bray_curtis_nmds_result_convert.to_dict('records')
        res['stress'] = stress[0]
    except MyGodApiException as e:
        e.log()

    return JsonResponse(res, safe=False)


def get_physico_chemical_data_beta_diversity(request, dataset_name, date_start, date_end, parameter):
    """
        Return dictionnary structure where every sample event is related to value of physico-chemical parameter selected
        Used by the API to display NMDS plot (BetaDiversityDissimilarity) depending on search criteria defined by
        the arguments
        :param request: the HTTP request
        :param dataset_name: the name of the dataset used to retrieve available fraction sizes
        :type dataset_name : str
        :param date_start: the start of the date range
        :type date_start : date
        :param date_end: the end of the date range
        :type date_end : date
        :param parameter : physico-chemical parameter
        :type parameter : str
        :return: A dictionary of dictionaries. Each entry matches a single sampling point and has the following keys :
                {'event_id': id of sample
                 'value': related physico-chemical value for parameter selected}
        """

    res = {'domain': {}, 'data': []}
    value = 0
    max_value = None
    min_value = None
    try:
        if dataset_name is None or date_start is None or date_end is None or parameter is None:
            raise MyGodApiException("get_beta_diversity_dissimilarity", "None argument given", logging.ERROR)
        try:
            PcReleve._meta.get_field(parameter)
            parameter_selected = parameter
        except FieldDoesNotExist:
            parameter_selected = 'custom_parameters'
            custom = True
        pc_releve_list = PcReleve.objects.filter(event__bind_dataset__name__iexact=dataset_name,
                                                 event__date__range=(date_start, date_end)).values(parameter_selected,
                                                                                                   "event__event_id")
        for pc_releve in pc_releve_list:

            if parameter_selected == "custom_parameters":
                for custom_field in pc_releve["custom_parameters"]:
                    if custom_field['name'] == parameter:
                        value = custom_field['value']
            else:
                value = pc_releve[parameter_selected]
            if value:
                tmp_value = value
                if min_value is None or min_value > tmp_value:
                    min_value = tmp_value
                if max_value is None or max_value < tmp_value:
                    max_value = tmp_value
                res['data'].append({'event_id': pc_releve['event__event_id'], 'value': value})
        res['domain'] = {'min': min_value, 'max': max_value}
    except MyGodApiException as e:
        e.log()
    return JsonResponse(res, safe=False)


def get_alpha_diversity(request, dataset_name, rank, taxon_name, fraction, date_start, date_end, group_by, split_by):
    """
    Return data with the NMDS result of matrix distance computation using bray_curtis or Jaccard dissimilarity of taxons according to search criteria
    Used by the API to display plot (AlphaDivsersity) depending on search criteria defined by
    the arguments
    :param request: the HTTP request
    :param dataset_name: the name of the dataset used to retrieve available fraction sizes
    :type dataset_name : str
    :param rank: the name of the rank of the taxon
    :type rank : str
    :param taxon_name: the name of the taxon
    :type taxon_name : str
    :param fraction: the fraction size
    :type fraction : str
    :param date_start: the start of the date range
    :type date_start : date
    :param date_end: the end of the date range
    :type date_end : date
    :param group_by: name of the parameter to use for building groupings ('season','year','month','depth')
    :type group_by : str
    :param split_by
    :return: A list of dictionaries. Each entry matches a single data point and has the following keys :
                {'group' : defined by group_type selected, year is year selected...,
                'category_name': abundance bind to taxon_name}
    """
    max_value = None
    try:
        if dataset_name is None or rank is None or taxon_name is None or fraction is None or \
                date_start is None or date_end is None:
            raise MyGodApiException("get_alpha_diversity", "None argument given", logging.ERROR)
        MAX_TAXON_DISPLAYED = 100000  # Initialize arbitrary value of taxon selected to avoid creation of group "other"
        dict_values = get_grouped_abundances_core(dataset_name, rank, taxon_name, fraction, date_start, date_end,
                                                  'sample',
                                                  'Species', "relative_abundance", MAX_TAXON_DISPLAYED, split_by)

        for group in dict_values['data']:
            for sampling_event in group['values']:
                logger.debug(sampling_event)
                sample_value_list = []
                for subgroup, value in sampling_event.items():
                    if subgroup != "group":
                        if value != 0.0:
                            sample_value_list.append(value)
                    else:
                        logger.debug(value)
                        event = Event.objects.get(event_id=value, bind_dataset__name__iexact=dataset_name)
                        if group_by == "cluster":
                            group_value = event.cluster
                        if group_by == "depth":
                            group_value = event.depth_code
                        if group_by == "station":
                            group_value = event.station_code
                        if group_by == "season":
                            group_value = get_season_from_date(event.date)
                        if group_by == "year":
                            group_value = event.date.year
                        if group_by == "month":
                            group_value = event.date.strftime("%b")
                index_value = -sum((abundance / 100) * math.log(abundance / 100) for abundance in sample_value_list)
                sampling_event['values'] = index_value
                sampling_event['subgroup'] = group_value
                tmp_value = index_value
                if max_value is None or max_value < tmp_value:
                    max_value = tmp_value
            dict_values['domain'] = {'max_value': max_value}
    except MyGodApiException as e:
        e.log()
    return JsonResponse(dict_values, safe=False)


def get_base_layers(request, dataset_name):
    # Build list of base layers for map combining
    # base layer definitions from the settings
    # with base layer definitions included in dataset metadata.
    baselayersbyname = {}
    for baselayer in settings.BASE_LAYERS:
        baselayersbyname[baselayer['name']] = baselayer

    dataset = DataSet.objects.get(name__iexact=dataset_name)
    for layer in dataset.base_layers:
        baselayersbyname[layer['name']]=layer


    return JsonResponse(list(baselayersbyname.values()),safe=False)
