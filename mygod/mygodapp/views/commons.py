import logging

logger = logging.getLogger(__name__)

# Mapping between the names of the parameters used in the UI and the model attributes.
PARAMETER_MAPPINGS = {
    'temperature': {'label': 'Temperature', 'attribute': 'event__pcreleve__temperature'},
    'tidecoefficient': {'label': 'Tide Coefficient', 'attribute': 'event__pcreleve__coef_maree'},
    's': {'label': 'Salinity', 'attribute': 'event__pcreleve__s'},
    'ph': {'label': 'pH', 'attribute': 'event__pcreleve__ph'},
    'o': {'label': 'Dissolved Oxygen', 'attribute': 'event__pcreleve__o'},
    'nh4': {'label': 'Ammonium (NH4)', 'attribute': 'event__pcreleve__nh4'},
    'no3': {'label': 'Nitrate (NO3)', 'attribute': 'event__pcreleve__no3'},
    'no2': {'label': 'Nitrite (NO2)', 'attribute': 'event__pcreleve__no2'},
    'po4': {'label': 'Phosphate (PO4)', 'attribute': 'event__pcreleve__po4'},
    'chla': {'label': 'Chlorophyll A', 'attribute': 'event__pcreleve__chla'},
}


#####Common function  in views files#####

def rawToVisibleOption(raw: str):
    """Delete '_' in taxon_name when displayed in selector"""
    return raw.capitalize().replace("_", " ")


class MyGodApiException(Exception):
    """
    Utility class to ease exception handling in API calls.
    """

    def __init__(self, method, message, level=logging.DEBUG):
        self.method = method
        self.message = message
        self.level = level

    def log(self):
        logger.log(self.level, f"Exception In Method {self.method} : {self.message}")
