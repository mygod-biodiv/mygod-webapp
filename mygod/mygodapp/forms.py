from django import forms

from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm
from .models import GalleryEntry, MetabarDataSet, MorphoDataSet
from django.forms import ModelForm, formset_factory
from django_summernote.widgets import SummernoteWidget


class NewLoginForm(AuthenticationForm):
    username = forms.TextInput(attrs={'class': 'form-control'})
    password = forms.PasswordInput()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class NewUserForm(UserCreationForm):
    email = forms.EmailField(required=True)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password1'].label = 'Password'
        self.fields['password2'].label = 'password2 label'

        self.fields['password1'].help_text = 'password1 help_text'
        self.fields['password2'].help_text = 'password2 help_text'

    class Meta:
        model = User
        fields = ("username", "first_name", "last_name", "email", "password1", "password2")
        help_texts = {
            'username': None,
            'email': None,
            'first_name': None,
            'last_name': None
        }

    def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        if commit:
            user.save()
        return user


# Create the form class.
class MetabarDatasetForm(ModelForm):
    class Meta:
        model = MetabarDataSet
        exclude = ['is_part_of', "includes", "taxonomy"]
        widgets = {
            'text': SummernoteWidget(),
        }


class MorphoDatasetForm(ModelForm):
    class Meta:
        model = MorphoDataSet
        exclude = ['is_part_of', "includes", "taxonomy"]
        widgets = {
            'text': SummernoteWidget(),
        }


class PublishDashForm(forms.ModelForm):
    uuid = forms.CharField(required=True,
                           widget=forms.HiddenInput())

    class Meta:
        model = GalleryEntry
        fields = ['image', 'summary', 'text', 'title']
        widgets = {
            'summary': SummernoteWidget(
                attrs={'summernote': {'toolbar': [['font', ['bold', 'underline', 'clear']],
                                                  ['insert', ['link', 'hr']]], 'maxTextLength': 288}}),
            'text': SummernoteWidget(
                attrs={'summernote': {'toolbar': [['style', ['style']], ['para', ['ul', 'ol', 'paragraph']],
                                                  ['font', ['bold', 'underline', 'clear']],
                                                  ['insert', ['link', 'hr', 'picture']]]}}),
        }

    def save(self, commit=True):
        return super(PublishDashForm, self).save(commit=commit)


class PublicationStatusForm(forms.Form):
    publication_status = forms.CharField(required=True,
                                         widget=forms.HiddenInput())


class RejectionForm(forms.Form):
    text = forms.CharField(widget=SummernoteWidget(
        attrs={'summernote': {'toolbar': [['style', ['style']], ['para', ['ul', 'ol', 'paragraph']],
                                          ['font', ['bold', 'underline', 'clear']],
                                          ['insert', ['link', 'hr']]]}}))
    uuid = forms.CharField(required=True,
                           widget=forms.HiddenInput())


class EditProfileForm(UserCreationForm):
    class Meta:
        model = User
        fields = ("username", "first_name", "last_name", "email")

    def save(self, commit=True):
        user = super(EditProfileForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']
        if commit:
            user.save()
        return user


class ClusterForm(forms.Form):
    cluster_name = forms.CharField()
    station_choices = forms.MultipleChoiceField(label="Stations")

    def __init__(self, *args, station_list, **kwargs):
        super(ClusterForm, self).__init__(*args, **kwargs)
        self.fields['station_choices'].choices = station_list


class MetabarImportForm(forms.Form):
    event_file = forms.FileField()
    sample_file = forms.FileField()
    metadata_file = forms.FileField()
    table_config_file = forms.FileField()


class PhysicoImportForm(forms.Form):
    dataset_name = forms.ChoiceField()
    event_file = forms.FileField()
    table_config_file = forms.FileField()

    def __init__(self, dataset_list, *args, **kwargs):
        super(PhysicoImportForm, self).__init__(*args, **kwargs)
        self.fields['dataset_name'].choices = dataset_list
