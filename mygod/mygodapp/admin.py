import logging
import copy
from .models import DataSet
from django.utils.safestring import mark_safe
from django.contrib.auth.models import User, Group
from django.contrib.auth.admin import UserAdmin, GroupAdmin
from django import forms
from django.contrib import admin
from django.contrib.admin.widgets import FilteredSelectMultiple
from guardian.admin import GuardedModelAdmin


def roles(self):
    p = sorted([u"<a title='%s'>%s</a>" % (x, x) for x in self.groups.all()])
    if self.user_permissions.count(): p += ['+']
    value = ', '.join(p)
    return mark_safe("<nobr>%s</nobr>" % value)


roles.allow_tags = True
roles.short_description = u'Groups'


def last(self):
    value = self.last_login
    return mark_safe("<nobr>%s</nobr>" % value)


last.allow_tags = True
last.admin_order_field = 'last_login'


def adm(self):
    return self.is_superuser


adm.boolean = True
adm.admin_order_field = 'is_superuser'


def staff(self):
    return self.is_staff


staff.boolean = True
staff.admin_order_field = 'is_staff'


def persons(self):
    return ', '.join([' %s' % x.username for x in
                      self.user_set.all().order_by('username')])


persons.allow_tags = True


class DatasetAdmin(GuardedModelAdmin):
    prepopulated_fields = {"description": ("description",)}
    list_display = ('description','name')


class UserInline(admin.StackedInline):
    model = User
    can_delete = False
    verbose_name_plural = 'Profil'


class NewUserAdmin(UserAdmin):
    list_display = ['username', 'email', 'first_name', 'last_name', 'is_active', staff, adm, roles, last]
    list_filter = ['groups', 'is_staff', 'is_superuser', 'is_active']


class GroupAdminForm(forms.ModelForm):
    class Meta:
        model = Group
        exclude = []

    # Add the users field.
    users = forms.ModelMultipleChoiceField(
        queryset=User.objects.all(),
        required=False,
        # Use the pretty 'filter_horizontal widget'.
        widget=FilteredSelectMultiple('users', False)
    )

    def __init__(self, *args, **kwargs):
        # Do the normal form initialisation.
        super(GroupAdminForm, self).__init__(*args, **kwargs)
        # If it is an existing group (saved objects have a pk).
        if self.instance.pk:
            # Populate the users field with the current Group users.
            self.fields['users'].initial = self.instance.user_set.all()

    def save_m2m(self):
        # Add the users to the Group.
        self.instance.user_set.set(self.cleaned_data['users'])

    def save(self, *args, **kwargs):
        # Default save
        instance = super(GroupAdminForm, self).save()
        # Save many-to-many data
        self.save_m2m()
        return instance


class NewGroupAdmin(GroupAdmin):
    list_display = ['name', persons]
    list_display_links = ['name']
    form = GroupAdminForm
    # Filter permissions horizontal as well.
    filter_horizontal = ['permissions']


admin.site.unregister(User)
admin.site.unregister(Group)
admin.site.register(User, NewUserAdmin)
admin.site.register(Group, NewGroupAdmin)
admin.site.register(DataSet, DatasetAdmin)
