from django.test import TestCase
from datetime import datetime
from unittest import skip
from mygodapp.models import Event


class EventTest(TestCase):

    @skip("Sample test for demonstration purposes only.")
    def test_create_event(self):
        event = Event(event_id="Random Event", type="Random Type",
                      date=datetime.strptime("2022-01-20 10:57:00", "%Y-%m-%d %H:%M:%S"),
                      min_depth=1.0,
                      max_depth=60.0,
                      latitude=48.68,
                      longitude=3.45)
        self.assertIsNotNone(event)
