from django.test import TestCase
from django.urls import reverse
from django.contrib.auth.models import User



class BaseTest(TestCase):
    def setUp(self):
        user = User.objects.create_user(email='testemail@gmail.com', username='username', password='password')
        user.is_active = True
        user.save()
        self.register_url = reverse('visuapp:sign_up')
        self.login_url = reverse('visuapp:sign_in')
        self.user_url = reverse('visuapp:user_page')
        self.user = {
            'email': 'testemail@gmail.com',
            'username': 'username',
            'password1': 'password',
            'password2': 'password',
            'first_name': 'fullname',
            "last_name": "lastname"
        }
        self.user_short_password = {
            'email': 'testemail@gmail.com',
            'username': 'username',
            'password1': 'tes',
            'password2': 'tes',
            'name': 'fullname'
        }
        self.user_unmatching_password = {

            'email': 'testemail@gmail.com',
            'username': 'username',
            'password1': 'teslatt',
            'password2': 'teslatto',
            'name': 'fullname'
        }

        self.user_invalid_email = {

            'email': 'test.com',
            'username': 'username',
            'password1': 'teslatt',
            'password2': 'teslatto',
            'name': 'fullname'
        }
        return super().setUp()


class RegisterTest(BaseTest):
    def test_can_view_page_correctly(self):
        response = self.client.get(self.register_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'sign_up.html')

    def test_can_register_user(self):
        response = self.client.post(self.register_url, self.user, format='text/html')
        self.assertEqual(response.status_code, 200)

    def test_cant_register_user_withshortpassword(self):
        response = self.client.post(self.register_url, self.user_short_password, format='text/html')
        self.assertEqual(response.status_code, 200)

    def test_cant_register_user_with_unmatching_passwords(self):
        response = self.client.post(self.register_url, self.user_unmatching_password, format='text/html')
        self.assertEqual(response.status_code, 200)

    def test_cant_register_user_with_invalid_email(self):
        response = self.client.post(self.register_url, self.user_invalid_email, format='text/html')
        self.assertEqual(response.status_code, 200)

    def test_cant_register_user_with_taken_email(self):
        self.client.post(self.register_url, self.user, format='text/html')
        response = self.client.post(self.register_url, self.user, format='text/html')
        self.assertEqual(response.status_code, 200)


class LoginTest(BaseTest):
    def test_can_access_page(self):
        response = self.client.get(self.login_url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'sign_in.html')

    def test_login_success(self):
        self.client.post(self.register_url, self.user, format='text/html')
        user = User.objects.get(username=self.user['username'])
        response = self.client.post(self.user_url, user, format='text/html')
        self.assertEqual(response.status_code, 200)

    def test_cantlogin_with_no_username(self):
        response = self.client.post(self.login_url, {'password': 'passwped', 'username': ''}, format='text/html')
        self.assertEqual(response.status_code, 200)

    def test_cantlogin_with_no_password(self):
        response = self.client.post(self.login_url, {'username': 'passwped', 'password': ''}, format='text/html')
        self.assertEqual(response.status_code, 200)
