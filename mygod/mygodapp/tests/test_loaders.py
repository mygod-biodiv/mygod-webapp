from django.contrib.auth.models import User
from django.core import management
from django.test import TestCase

from mygodapp.models import Event, PcReleve
from mygodapp.tools.metabar import MetabarcodingDataLoader, MetaDataLoader
from mygodapp.tools.morphology import MorphologyDataLoader
from mygodapp.tools.physico_chemical import PhysicoChemicalCsvLoader

REBUILD_DB = True

TEST_METABARCODE_METADATA_FILE = "test_data/astan_18S_metadata.json"
TEST_METABARCODE_DATA_FILE = "test_data/astan_18S_subsasv.csv"
TEST_EVENT_DATA_FILE = "test_data/astan_18S_subsample.csv"
TEST_METABARCODE_CONFIG_FILE = "test_data/astan18S_config.json"
TEST_PHYSICO_CHEMICAL_CONFIG_FILE = "test_data/somlit_astan_physico_chemical_config.json"
TEST_PHYSICO_CHEMICAL_DATA_FILE = "test_data/somlit_astan_physico_chemical_data.csv"
TEST_MORPHOLOGICAL_DATA_FILE = "test_data/at-so-phytobs_morphological_data.csv"
TEST_MORPHOLOGICAL_METADATA_FILE = "test_data/at-so-phytobs_morphological_metadata.json"
TEST_MORPHOLOGICAL_CONFIG_FILE = "test_data/at-so-phytobs_morphological_config.json"


class MetabarDataLoaderTest(TestCase):

    # The setUpTestData class method needs to be user because test methods themselves
    # are ran in transaction which is rolled back.
    @classmethod
    def setUpTestData(cls):
        global REBUILD_DB
        if REBUILD_DB is True:
            REBUILD_DB = False
            management.call_command("flush", "--no-input")
            metadata_loader = MetaDataLoader()
            with open(TEST_METABARCODE_METADATA_FILE) as mf:
                metadata_loader.load(mf)
            dataset = metadata_loader.get_dataset()
            taxonomy = metadata_loader.get_taxonomy()

            metabarcode_data_loader = MetabarcodingDataLoader(dataset, taxonomy)
            with open(TEST_METABARCODE_CONFIG_FILE) as cf:
                metabarcode_data_loader.load_config(cf)
            with open(TEST_METABARCODE_DATA_FILE) as df, open(TEST_EVENT_DATA_FILE) as ef:
                metabarcode_data_loader.load_data(df, ef)

            physico_chemical_data_loader = PhysicoChemicalCsvLoader()
            with open(TEST_PHYSICO_CHEMICAL_CONFIG_FILE) as cf:
                physico_chemical_data_loader.load_config(cf)
            with open(TEST_PHYSICO_CHEMICAL_DATA_FILE) as df:
                physico_chemical_data_loader.load_data(df)

    def test_load_metadata(self):
        users = list(User.objects.all())
        self.assertIsNotNone(users)
        # Length is the number of owners declared int the metadata +1 (the default AnonymousUser)
        self.assertEqual(len(users), 2)

    def test_load_metabarcode_data(self):
        events = list(Event.objects.all())
        # The test data file contains 37 columns, including the initial 7 non-event related columns.
        # Hence 30 event related, each for two fraction sizes, which gives 15 events.
        self.assertEquals(len(events), 15)


class PhysicoChemicalDataLoaderTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        physico_chemical_data_loader = PhysicoChemicalCsvLoader()
        with open(TEST_PHYSICO_CHEMICAL_CONFIG_FILE) as cf:
            physico_chemical_data_loader.load_config(cf)
        with open(TEST_PHYSICO_CHEMICAL_DATA_FILE) as df:
            physico_chemical_data_loader.load_data(df)

    def test_load_physico_chemical_data(self):
        pcreleves = list(PcReleve.objects.all())
        # The test metabarcode data file contains about 10% of the original data.
        # Hence the number of matching physico chemical samples is also around 10% of the
        # samples in the input file (30 out of 362).
        self.assertEquals(len(pcreleves), 30)


class MorphoDataLoaderTest(TestCase):

    # The setUpTestData class method needs to be user because test methods themselves
    # are ran in transaction which is rolled back.
    @classmethod
    def setUpTestData(cls):
        global REBUILD_DB
        if REBUILD_DB is True:
            REBUILD_DB = False
            management.call_command("flush", "--no-input")
            metadata_loader = MetaDataLoader()
            with open(TEST_MORPHOLOGICAL_METADATA_FILE) as mf:
                metadata_loader.load(mf)
            dataset = metadata_loader.get_dataset()
            taxonomy = metadata_loader.get_taxonomy()

            morphology_data_loader = MorphologyDataLoader(dataset, taxonomy)
            with open(TEST_MORPHOLOGICAL_CONFIG_FILE) as cf:
                morphology_data_loader.load_config(cf)
            with open(TEST_MORPHOLOGICAL_DATA_FILE) as df:
                morphology_data_loader.load_data(df)

    def test_load_metadata(self):
        users = list(User.objects.all())
        self.assertIsNotNone(users)
        # Length is the number of owners declared int the metadata +1 (the default AnonymousUser)
        self.assertEqual(len(users), 2)

    def test_load_morphological_data(self):
        events = list(Event.objects.all())
        # The test data file contains 37 columns, including the initial 7 non-event related columns.
        # Hence 30 event related, each for two fraction sizes, which gives 15 events.
        self.assertEquals(len(events), 15)
