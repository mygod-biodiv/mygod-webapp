import datetime
import json
import logging
import os
import re

from django.conf import settings
from django.contrib.auth.models import User
from django.test import override_settings
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

import uuid
import json
from mygodapp.models import DataSet

API_TEST_DATABASES = settings.DATABASES.copy()
API_TEST_DATABASES['default']['TEST']['NAME'] = settings.DATABASES['default']['NAME']


@override_settings(DATABASES=API_TEST_DATABASES)
class MyGodApiTest(APITestCase):

    def setUp(self):
        super().setUp()
        self.datasets = DataSet.objects.all()
        self.logger = logging.getLogger(__name__)


class MyGodTaxonSearchPanelApiTest(MyGodApiTest):

    def test_get_dataset_names(self):
        url = reverse('visuapp:get_dataset_names')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)

        self.assertEqual(len(content), len(self.datasets))

        self.assertEqual(content[0]['text'], self.datasets[0].name)
        self.assertEqual(content[0]['value'], self.datasets[0].name)

    def test_get_taxonomy_ranks(self):
        url = reverse('visuapp:get_taxonomy_ranks', kwargs={'datasetname': self.datasets[0].name})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content), 9)
        self.assertEqual(content[0]['text'], "Kingdom")
        self.assertEqual(content[0]['value'], "kingdom")
        self.assertEqual(content[-2]['text'], "Species")
        self.assertEqual(content[-2]['value'], "species")

    def test_get_taxon_proposals(self):
        url = reverse('visuapp:get_taxon_proposals',
                      kwargs={'rank': 'Genus', 'taxonheader': 'ost', "dataset": "Astan 18S - 2009-2016"})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content), 2)
        self.assertEqual(content[0], {'name': 'Ostreococcus', 'id': '12|Ostreococcus'})

    def test_get_taxonomic_tree_for_dataset(self):
        url = reverse('visuapp:get_taxonomic_tree_for_dataset', kwargs={'datasetname': self.datasets[0].name})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content[1]['children']), 10)

    def test_get_dataset_date_interval(self):
        url = reverse('visuapp:get_dataset_date_interval', kwargs={'datasetname': self.datasets[0].name})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(content['datestart'], "2009-01-07")


class MyGodTaxonDashboardWidgetParameterApiTest(MyGodApiTest):

    def test_get_dataset_datatype(self):
        url = reverse('visuapp:get_dataset_datatype',
                      kwargs={'datasetname': self.datasets[0].name})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(content, "metabar")


    def test_get_physico_chemical_parameters_for_dataset(self):
        url = reverse('visuapp:get_parameters', kwargs={'datasetname': self.datasets[0].name})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content), 10)
        self.assertEqual(content[0]['text'], 'Temperature')
        self.assertEqual(content[0]['value'], 'temperature')
        self.assertEqual(content[-1]['text'], 'Chlorophyll A')
        self.assertEqual(content[-1]['value'], 'chla')

    def test_get_fraction_sizes_for_dataset(self):
        url = reverse('visuapp:get_fraction_sizes', kwargs={'datasetname': self.datasets[0].name})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content), 2)
        self.assertEqual(content[-1]['text'], '>3 µm')
        self.assertEqual(content[-1]['value'], '3')
        self.assertEqual(content[0]['text'], '0.2-3 µm')
        self.assertEqual(content[0]['value'], '0.2')

    def test_get_dataset_metadata(self):
        url = reverse('visuapp:get_dataset_metadata', kwargs={'datasetname': self.datasets[0].name})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content), 11)
        self.assertEqual(content[0], 'Astan 18S - 2009-2016')
        self.assertEqual(content[-1], 'V4')

    def test_get_taxon_subtree(self):
        url = reverse('visuapp:get_taxon_subtree',
                      kwargs={'datasetname': self.datasets[0].name, 'rank': 'Genus', 'taxonname': 'ostreococcus'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content), 2)
        self.assertEquals(content['Genus'][0]['text'], 'Ostreococcus')
        self.assertEquals(content['Genus'][0]['value'], 'Genus/Ostreococcus')
        self.assertEquals(content['Species'][-1]['text'], 'Ostreococcus mediterraneus')
        self.assertEquals(content['Species'][-1]['value'], 'Species/Ostreococcus_mediterraneus')


class MyGodTaxonDashboardWidgetDataApiTest(MyGodApiTest):

    def test_get_fasta_sequences(self):
        url = reverse('visuapp:get_sequences_as_fasta',
                      kwargs={'datasetname': self.datasets[0].name, 'taxonname': '12|ostreococcus'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        print(content)
        self.assertEqual(content[0], '>')

    def test_get_relative_abundances_taxon(self):
        url = reverse('visuapp:get_relative_abundances_taxon',
                      kwargs={'datasetname': self.datasets[0].name, 'rank': 'Genus', 'taxonname': '12|ostreococcus',
                              'date_start': '1901-01-01', 'date_end': '2999-12-31',
                              'parameter': 'temperature', 'fraction': '3', 'representation': 'taxon', 'depth': 'SRF',
                              'split_by': None})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content['data'][0]['values']), 201)
        self.assertEqual(content['data'][0]['values'][0]['paramvalue'], 8.3)
        self.assertEqual(content['data'][0]['values'][0]['taxonname'], '12|ostreococcus')
        self.assertAlmostEqual(content['data'][0]['values'][0]['abundance'], 0.45, places=3)

    def test_get_relative_abundances_asv(self):
        url = reverse('visuapp:get_relative_abundances_asv',
                      kwargs={'datasetname': self.datasets[0].name,
                              'asv_list': json.dumps(['230c7133a34050bab65b78e86e2626079801e63d']),

                              'date_start': '1901-01-01', 'date_end': '2999-12-31',
                              'parameter': 'temperature', 'fraction': '3', 'depth': 'SRF', 'split_by': 'year'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content['data'][0]['values']), 41)
        self.assertEqual(content['data'][0]['values'][0]['paramvalue'], 8.8)
        self.assertAlmostEqual(content['data'][0]['values'][0]['abundance'], 0.32729486984320294, places=3)

    def test_get_dataset_date_interval(self):
        url = reverse('visuapp:get_dataset_date_interval', kwargs={'datasetname': self.datasets[0].name})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(content['datestart'], str(self.datasets[0].first_sample_date))
        self.assertEqual(content['dateend'], str(self.datasets[0].last_sample_date))

    def test_get_axis_label(self):
        url = reverse('visuapp:get_axis_label', kwargs={'datasetname': self.datasets[0].name, 'parameter': 's'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(content['label'], "PSU")

    def test_get_discrete_temporal_distribution_for_taxon(self):
        url = reverse('visuapp:get_discrete_temporal_distribution_taxon', kwargs={'datasetname': self.datasets[0].name,
                                                                                  'rank': 'Genus',
                                                                                  'taxonname': '12|ostreococcus',
                                                                                  'date_start': '1901-01-01',
                                                                                  'date_end': '2999-12-31',
                                                                                  'fraction': '3', 'depth': 'SRF',
                                                                                  'group_by': 'month',
                                                                                  'representation': 'taxon',
                                                                                  'split_by': None})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content['data'][0]['values']), 245)

    def test_get_discrete_temporal_distribution_for_asv(self):
        url = reverse('visuapp:get_discrete_temporal_distribution_asv', kwargs={'datasetname': self.datasets[0].name,
                                                                                'asv_list': json.dumps([
                                                                                    '230c7133a34050bab65b78e86e2626079801e63d']),

                                                                                'date_start': '1901-01-01',
                                                                                'date_end': '2999-12-31',
                                                                                'fraction': '0.2', 'group_by': 'month',
                                                                                'depth': 'SRF', 'split_by': None})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content['data'][0]['values']), 188)

    def test_get_occurrences_by_date_taxon(self):
        url = reverse('visuapp:get_occurrences_by_date_taxon', kwargs={'datasetname': self.datasets[0].name,
                                                                       'rank': 'Genus', 'taxonname': '12|ostreococcus',
                                                                       'date_start': '1901-01-01',
                                                                       'date_end': '2999-12-31',
                                                                       'fraction': '3',
                                                                       "depth": "SRF", 'representation': 'taxon',
                                                                       'split_by': None
                                                                       })
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content['data'][0]['values']), 190)

    def test_get_occurrences_by_date_asv(self):
        url = reverse('visuapp:get_occurrences_by_date_asv', kwargs={'datasetname': self.datasets[0].name,
                                                                     'asv_list': json.dumps(
                                                                         ['230c7133a34050bab65b78e86e2626079801e63d']),
                                                                     'date_start': '1901-01-01',
                                                                     'date_end': '2999-12-31',
                                                                     'fraction': '0.2',
                                                                     "depth": "SRF", 'split_by': None})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content['data'][0]['values']), 188)

    def test_get_abundances_with_coordinates(self):
        url = reverse('visuapp:get_abundances_with_coordinates', kwargs={'datasetname': self.datasets[0].name,
                                                                         'rank': 'Genus',
                                                                         'taxonname': '12|ostreococcus',
                                                                         'depth': json.dumps(
                                                                             [{'depth': 'SRF', 'visible': True}]),
                                                                         'date_start': '1901-01-01',
                                                                         'date_end': '2999-12-31',
                                                                         'fraction': '3'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content), 1)

    def test_get_station_reference_coordinates(self):
        url = reverse('visuapp:get_station_reference_coordinates', kwargs={'datasetname': self.datasets[0].name})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(content[0]['station_name'], 'RA')

    def test_get_grouped_abundances(self):
        url = reverse('visuapp:get_grouped_abundances', kwargs={'datasetname': self.datasets[0].name,
                                                                'rank': 'Genus', 'taxonname': '12|ostreococcus',
                                                                'date_start': '1901-01-01',
                                                                'date_end': '2999-12-31',
                                                                'fraction': '3', 'group_by': 'season',
                                                                'sublevel': 'Species',
                                                                'data_type': 'relative_abundance',
                                                                'number_displayed': 5, 'split_by': 'year'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)

        self.assertEqual(content["columns"][-1], 'Ostreococcus_mediterraneus')
        self.assertEqual(len(content["columns"]), 3)

    def test_get_taxonomy_subranks(self):
        url = reverse('visuapp:get_taxonomy_subranks', kwargs={'datasetname': self.datasets[0].name,
                                                               'rank': 'Genus', 'taxon': '12|ostreococcus'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)

        self.assertEqual(len(content), 2)
        self.assertEqual(content[0], "Genus")

    def test_get_nested_abundances(self):
        url = reverse('visuapp:get_nested_abundance_for_taxon', kwargs={'datasetname': self.datasets[0].name,
                                                                        'rank': 'Genus', 'taxonname': '12|ostreococcus',
                                                                        'date_start': '1901-01-01',
                                                                        'date_end': '2999-12-31',
                                                                        'fraction': '3', 'depth': 'SRF'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)

        self.assertEqual(len(content['children']), 3)
        self.assertEqual(content['children'][0]['value'], 55)

    def test_get_beta_diversity_dissimilarity(self):
        url = reverse('visuapp:get_beta_diversity_dissimilarity', kwargs={'datasetname': self.datasets[0].name,
                                                                          'rank': 'Genus',
                                                                          'taxonname': '12|ostreococcus',
                                                                          'date_start': '1901-01-01',
                                                                          'date_end': '2999-12-31',
                                                                          'fraction': '3',
                                                                          'dissimilarity_type': 'bray_curtis'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)

        self.assertEqual(content['nmds_result'][0]['sample_id'], 'RA090107_3')

    def test_get_physico_chemical_repartition(self):
        url = reverse('visuapp:get_physico_chemical_repartition', kwargs={'datasetname': self.datasets[0].name,
                                                                          'date_start': '1901-01-01',
                                                                          'date_end': '2999-12-31',
                                                                          'pc_parameter': "temperature",
                                                                          "group_by": "month", 'depth': 'SRF',
                                                                          "split_by": 'year'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(content['data'][0]['group'], 2009)


class MyGodSettingsApiTest(MyGodApiTest):

    def setUp(self):
        super().setUp()
        self.dummy_username = "apitest" + str(uuid.uuid4())[0:4]
        self.dummy_password = "s3cr3t"
        self.dummy_user = User.objects.create_user(username=self.dummy_username, password=self.dummy_password)
        self.client.force_login(self.dummy_user)

    def tearDown(self):
        self.client.logout()
        self.dummy_user.delete()

    def test_save_settings_wrong_method(self):
        url = reverse('visuapp:save_settings')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_save_settings_missing_data(self):
        url = reverse('visuapp:save_settings')
        response = self.client.post(url, data={})
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_save_settings_no_id(self):
        url = reverse('visuapp:save_settings')
        string_data = json.dumps({'settings': {'param1': 'value1'}})
        response = self.client.post(url, data={'data': string_data})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        settings_id = content['id']
        self.assertTrue(re.match(r"([a-z0-9]+\-){4}([a-z0-9]+)", settings_id))

    def test_load_settings_wrong_id(self):
        url = reverse('visuapp:load_settings', kwargs={'settings_id': 'bogus'})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_load_settings(self):
        url = reverse('visuapp:save_settings')
        string_data = json.dumps({'settings': {'param1': 'value1'}})
        response = self.client.post(url, data={'data': string_data})
        content = json.loads(response.content)
        settings_id = content['id']
        url = reverse('visuapp:load_settings', kwargs={'settings_id': settings_id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        mygod_settings = content['settings']
        param1 = mygod_settings['param1']
        self.assertEquals(param1, 'value1')

    def test_list_settings(self):
        url = reverse('visuapp:save_settings')
        string_data = json.dumps({'settings': {'param1': 'value1'}})
        response = self.client.post(url, data={'data': string_data})
        string_data = json.dumps({'settings': {'param2': 'value2'},
                                  'name': 'sample settings'})
        response = self.client.post(url, data={'data': string_data})
        string_data = json.dumps({'settings': {'param3': 'value3'},
                                  'name': 'sample settings with creation date',
                                  'creationdate': datetime.datetime(1999, 1, 1, 23, 59, 59).isoformat()})
        response = self.client.post(url, data={'data': string_data})
        url = reverse('visuapp:list_settings', kwargs={})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        content = json.loads(response.content)
        self.assertEqual(len(content), 3)
