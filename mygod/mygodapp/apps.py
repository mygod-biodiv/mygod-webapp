from django.apps import AppConfig



class VisuappConfig(AppConfig):
    name = 'mygodapp'

    def ready(self):
        import mygodapp.signals
