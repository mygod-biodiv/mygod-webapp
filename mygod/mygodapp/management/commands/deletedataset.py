from django.core.management.base import BaseCommand

import logging

from mygodapp.tools.delete_dataset import RemoveDataset


class Command(BaseCommand):

    LOGGER = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument('--dataset_name', required=True)


    def handle(self, *args, **options):
        dataset = options['dataset_name']
        data_eraser = RemoveDataset()
        data_eraser.remove_metabar_data(dataset)
        self.LOGGER.debug(f"Done Remove dataset from database { dataset }")
