from django.core.management.base import BaseCommand

import logging

from mygodapp.tools.morphology import MorphologyDataLoader, MorphoMetaDataLoader


class Command(BaseCommand):
    LOGGER = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument('--biodiversity_table', type=open, required=True)
        parser.add_argument('--config', type=open, required=True)
        parser.add_argument('--metadata', type=open, required=True)

    def handle(self, *args, **options):
        biodiversity_table = options['biodiversity_table']
        config_file = options['config']
        metadata_file = options['metadata']

        self.LOGGER.debug(f"Loading metadata from file {metadata_file}")
        metadata_loader = MorphoMetaDataLoader()
        metadata_loader.load(metadata_file)
        taxonomy = metadata_loader.get_taxonomy()
        dataset = metadata_loader.get_dataset()
        self.LOGGER.debug(f"Done Loading metadata from file {metadata_file}")

        self.LOGGER.debug(
            f"Processing samples from {biodiversity_table}  (with configuration file {config_file})")
        morphology_loader = MorphologyDataLoader(dataset, taxonomy)
        morphology_loader.load_config(config_file)
        morphology_loader.load_data(biodiversity_table)
