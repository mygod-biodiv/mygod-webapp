from django.contrib.auth.models import Group, User
from guardian.shortcuts import assign_perm
from django.core.management.base import BaseCommand

import logging


class Command(BaseCommand):
    LOGGER = logging.getLogger(__name__)

    def handle(self, *args, **options):
        Group.objects.get_or_create(name='importers')
        self.LOGGER.debug(f"")
