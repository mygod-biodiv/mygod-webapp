from django.core.management.base import BaseCommand
import logging
from mygodapp.tools.physico_chemical import PhysicoChemicalCsvLoader


class Command(BaseCommand):
    LOGGER = logging.getLogger(__name__)

    def add_arguments(self, parser):
        parser.add_argument('--csv', type=open, required=True)
        parser.add_argument('--config', type=open, required=True)

    def handle(self, *args, **options):
        csv_file = options['csv']
        config_file = options['config']

        self.LOGGER.debug(f"Processing file {csv_file} (with configuration file {config_file})")
        physico_chemical_data_loader = PhysicoChemicalCsvLoader()
        physico_chemical_data_loader.load_config(config_file)
        physico_chemical_data_loader.load_data(csv_file)
        self.LOGGER.debug(f"Done Processing file {csv_file} (with configuration file {config_file}")
