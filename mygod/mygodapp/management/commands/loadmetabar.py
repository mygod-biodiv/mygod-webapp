from django.core.management.base import BaseCommand

import logging

from mygodapp.tools.metabar import MetabarcodingDataLoader, MetabarMetadataLoader
from mygodapp.tools.commons import DatasetImportException


class Command(BaseCommand):

    LOGGER = logging.getLogger(__name__)


    def add_arguments(self, parser):
        parser.add_argument('--sample_table', required=True)
        parser.add_argument('--event_table', type=open, required=True)
        parser.add_argument('--config', type=open, required=True)
        parser.add_argument('--metadata', type=open, required=True)

    def handle(self, *args, **options):
        sample_file = options['sample_table']
        event_file = options['event_table']
        config_file = options['config']
        metadata_file = options['metadata']

        try:
            self.LOGGER.debug(f"Loading metadata from file {metadata_file}")
            metadata_loader = MetabarMetadataLoader()
            metadata_loader.load(metadata_file)
            taxonomy = metadata_loader.get_taxonomy()
            dataset = metadata_loader.get_dataset()
            self.LOGGER.debug(f"Done Loading metadata from file {metadata_file}")

            self.LOGGER.debug(f"Processing samples from {sample_file} and events from {event_file} (with configuration file {config_file})")
            metabarcode_loader = MetabarcodingDataLoader(dataset, taxonomy)
            metabarcode_loader.load_config(config_file)
            rarefied_sample = metabarcode_loader.rarefy(sample_file)
            metabarcode_loader.load_data(sample_file, event_file, rarefied_sample)
            self.LOGGER.debug(f"Done processing samples from {sample_file} and events from {event_file} (with configuration file {config_file})")
        except DatasetImportException:
            self.LOGGER.error("Error during data import")