'use strict';

$(document).ready(() => {
    $('.appear-on-hover').parent().each((i, parent) => {
        let $parent = $(parent);
        let toRemove = $parent.children('.appear-on-hover');
        let toAdd = toRemove.html();
        toRemove.remove();
        let parentClasses = $parent.attr('class');
        let newElt = $(`<div class="hover-popup ${parentClasses}" style="display: none">
                            ${$parent.html()}
                            <div class="to-appear" style="display: none">${toAdd}</div>
                        </div>`);
        newElt.css({height: $parent.outerHeight(), width: $parent.outerWidth()})
        let toAppear = newElt.children('.to-appear');
        newElt.appendTo($parent);
        $parent.on('mouseenter', () => {
            newElt.css({display: 'block'});
            newElt.animate({height: $parent.outerHeight() * 1.7, width: $parent.outerWidth() * 1.5}, () => {
                toAppear.show('');
            });
        });
        newElt.on('mouseleave', () => {
            toAppear.hide(100, () => {
                newElt.animate({height: $parent.outerHeight(), width: $parent.outerWidth()}, 400, 'swing', () => {
                    newElt.css({display: 'none'});
                });
            });
        })
    });
})



