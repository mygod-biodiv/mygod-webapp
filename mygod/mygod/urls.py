from django.contrib import admin
from django.http import HttpResponseRedirect
from django.urls import include, path, re_path
from django.contrib.auth import views as auth_views
from django.conf import settings
from django.conf.urls.static import static


BACKOFFICE_BASE = settings.APPLICATION_CONTEXT + '/bo/'

urlpatterns = [
    re_path(r'^$', lambda r: HttpResponseRedirect(settings.APPLICATION_CONTEXT+'/')),
    path(settings.APPLICATION_CONTEXT +'/', include('mygodapp.urls', namespace="vdgob_data")),
    path(BACKOFFICE_BASE + 'admin/', admin.site.urls),
    path(BACKOFFICE_BASE + 'password_reset/', auth_views.PasswordResetView.as_view(template_name='password_reset.html'),
         name='password_reset'),
    path(BACKOFFICE_BASE + 'password_reset/done/', auth_views.PasswordResetDoneView.as_view(template_name='password_reset_done.html'),
         name='password_reset_done'),
    path(BACKOFFICE_BASE + 'reset/<uidb64>/<token>/',
         auth_views.PasswordResetConfirmView.as_view(template_name="password_reset_confirm.html"),
         name='password_reset_confirm'),
    path(BACKOFFICE_BASE + 'reset/done/', auth_views.PasswordResetCompleteView.as_view(template_name='password_reset_complete.html'),
         name='password_reset_complete'),
    path(BACKOFFICE_BASE + 'summernote/', include('django_summernote.urls')),

]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
