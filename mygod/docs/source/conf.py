# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information


# Generating the docs :
# - make html when modifiying the source code.
# - sphinx-apidoc -o source .. when adding/removing modules.


project = 'MyGOD'
copyright = '2022, Charlotte ANDRE <charlotte.andre@sb-roscoff.fr>, Mark HOEBEKE <mark.hoebeke@sb-roscoff.fr>'
author = 'Charlotte ANDRE <charlotte.andre@sb-roscoff.fr>, Mark HOEBEKE <mark.hoebeke@sb-roscoff.fr>'
release = '0.0.9'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['myst_parser', 'sphinx.ext.autodoc', 'sphinx.ext.napoleon', 'sphinx_autodoc_typehints',
              'sphinx.ext.autosummary']

templates_path = ['_templates']
exclude_patterns = []

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'nature'
html_static_path = ['_static']
html_theme_options = {'sidebarwidth': '500px'}

import os
import sys
import django

sys.path.insert(0, os.path.abspath('../..'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'mygod.settings'
django.setup()
