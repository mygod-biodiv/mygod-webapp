Views Gallery Module
================

.. automodule:: mygodapp.views.gallery
    :members:
    :undoc-members:
    :inherited-members:
    :show-inheritance:
