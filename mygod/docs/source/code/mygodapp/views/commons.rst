Views Commons Module
================

.. automodule:: mygodapp.views.commons
    :members:
    :undoc-members:
    :inherited-members:
    :show-inheritance:
