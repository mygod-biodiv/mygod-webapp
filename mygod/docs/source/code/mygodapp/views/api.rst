Views API Module
================

.. automodule:: mygodapp.views.api
    :members:
    :undoc-members:
    :inherited-members:
    :show-inheritance:
