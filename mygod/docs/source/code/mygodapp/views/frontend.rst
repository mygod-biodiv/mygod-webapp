Views Frontend Module
================

.. automodule:: mygodapp.views.frontend
    :members:
    :undoc-members:
    :inherited-members:
    :show-inheritance:
